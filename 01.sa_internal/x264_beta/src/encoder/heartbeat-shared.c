#include "heartbeat.h"
#include <stdlib.h>
#include <string.h>

extern char *__progname;

#ifdef MOCO
int global_level_total;
int global_add_info;
#endif

double global_min_target;
double global_max_target;
int64_t global_window_size;
int64_t global_buffer_depth;
char global_log_name[100];

/**
 * Helper function for allocating shared memory
 */
static inline heartbeat_record_t* HB_alloc_log(int pid, int64_t buffer_size) {

    heartbeat_record_t* p = NULL;
#if 1
    int shmid;

    printf("Allocating log for %d, %d\n", pid, pid << 1);

    if ((shmid = shmget(pid << 1, buffer_size*sizeof(heartbeat_record_t), IPC_CREAT | 0666)) < 0) {
        //perror("cannot allocate shared memory for heartbeat records");
        p = NULL;
    }

    /*
     * Now we attach the segment to our data space.
     */
    if ((p = (heartbeat_record_t*) shmat(shmid, NULL, 0)) == (heartbeat_record_t *) -1) {
        //perror("cannot attach shared memory to heartbeat enabled process");
        p = NULL;
    }

#endif


    return p;

}

/**
 * 
 * @param pid integer 
 */
static inline HB_global_state_t* HB_alloc_state(int pid) {

    HB_global_state_t* p = NULL;
    int shmid;

    if ((shmid = 
                shmget((pid << 1) | 1, 
                    1*sizeof(HB_global_state_t), 
                    IPC_CREAT | 0666)) < 0) {
        p = NULL;
    }

    /*
     * Now we attach the segment to our data space.
     */
    if ((p = (HB_global_state_t*) shmat(shmid, NULL, 0)) == (HB_global_state_t *) -1) {
        p = NULL;
    }

    return p;

}

/**
 * @Description	: read sa.cfg and set the values into heartbeat.
 * @Author		: Young-Joo Kim
 * @Date		: 2015. 07. 14
 */
 /*
char *getApp(int _pid, 
		double *min_target, 
		double *max_target, 
		int64_t *window_size,
		int64_t *buffer_depth,
		char *log_name) {
*/
char *getApp(int _pid) {

#ifdef MOCO
    long longValue[7];
#else
    long longValue[5];
#endif
    double doubleValue[3];
	char logName[100];
    char app_Info[200];

    FILE *file = fopen("./sa.cfg","r");
    if (file == NULL) {
        printf("Error: fopen in getApp() \n");
        return "Error";
    }   

    do {
        char line[100];
        char value[10];

        if(fgets(line, 100, file) == NULL) break;
	
		/*
        if(line[0] == " ") continue;
        else if(line[0] == "#") continue;   
		*/

        //if(line[0] == "#" || line[4] == "#") continue;   
        if(strchr(line, '#') != NULL) continue; 

        //if(strncmp(line, "<SA_TYPE>", 9) == 0){ 
        if(strstr(line, "<SA_TYPE>") != NULL){ 
            fscanf(file, "%ld", &longValue[0]);
            printf("SA_TYPE = %ld\n", longValue[0]);
        }
        //else if(strncmp(line, "<SA_TARGET_QOS>", 15) == 0){
        else if(strstr(line, "<SA_TARGET_QOS>") != NULL){
            fscanf(file, "%lf", &doubleValue[0]);
            printf("SA_TARGET_QOS = %lf\n", doubleValue[0]);
        }
#ifdef MOCO	           
	else if(strstr(line, "<SA_ALG_NUMBER>") != NULL){
            fscanf(file, "%ld", &longValue[5]);
            printf("SA_ALG_NUMBER = %ld\n", longValue[5]);
		//* total number of algorithm
		global_level_total = longValue[5];
	}
//FIXME : ask for adding variable
	else if(strstr(line, "<SA_ADD_INFO>") != NULL){
            fscanf(file, "%ld", &longValue[6]);
            printf("SA_ADD_INFO = %ld\n", longValue[6]);
		//* total number of algorithm
		global_add_info = longValue[6];
	}
#endif
        //else if(strncmp(line, "<SA_MAX_QOS>", 12) == 0){ 
        else if(strstr(line, "<SA_MAX_QOS>") != NULL){ 
            fscanf(file, "%lf", &doubleValue[1]);
            printf("SA_MAX_QOS = %lf\n", doubleValue[1]);
			//*max_target = doubleValue[1];
			global_max_target = doubleValue[1];
        }
        //else if(strncmp(line, "<SA_MIN_QOS>", 12) == 0){
        else if(strstr(line, "<SA_MIN_QOS>") != NULL){
            fscanf(file, "%lf", &doubleValue[2]);
            printf("SA_MIN_QOS = %lf\n", doubleValue[2]);
			//*min_target = doubleValue[2];
			global_min_target = doubleValue[2];
        }
        //else if(strncmp(line, "<SA_WINDOW_SIZE>", 16) == 0){
        else if(strstr(line, "<SA_WINDOW_SIZE>") != NULL){
            fscanf(file, "%ld", &longValue[1]);
            printf("SA_WINDOW_SIZE = %ld\n", longValue[1]);
			//*window_size = longValue[1];
			global_window_size = longValue[1];
        }
        //else if(strncmp(line, "<SA_BUFFER_DEPTH>", 17) == 0){
        else if(strstr(line, "<SA_BUFFER_DEPTH>") != NULL){
            fscanf(file, "%ld", &longValue[2]);
            printf("SA_BUFFER_DEPTH = %ld\n", longValue[2]);
			//*buffer_depth = longValue[2];
			global_buffer_depth = longValue[2];
        }
        //else if(strncmp(line, "<SA_SAMPLING_TIME>", 18) == 0){
        else if(strstr(line, "<SA_SAMPLING_TIME>") != NULL){
            fscanf(file, "%ld", &longValue[3]);
            printf("SA_SAMPLING_TIME = %ld\n", longValue[3]);
        }
        //else if(strncmp(line, "<SA_HB_NUMBER>", 14) == 0){
        else if(strstr(line, "<SA_HB_NUMBER>") != NULL){
            fscanf(file, "%ld", &longValue[4]);
            printf("SA_HB_NUMBER = %ld\n", longValue[4]);
		}
		//else if(strncmp(line, "<SA_LOGNAME>", 12) == 0){
		else if(strstr(line, "<SA_LOGNAME>") != NULL){
			fscanf(file, "%s", logName);
			sprintf(global_log_name, "%s", logName);
			printf("SA_LOGNAME = %s %s\n", logName, global_log_name);
		}

		//memset(line, 0x00, 100);

    } while(1);
#ifdef MOCO
    sprintf(app_Info,"%d|%s|%ld|%lf|%lf|%lf|%ld|%ld|%ld|%ld|%ld|%ld|%s", 
			 _pid, __progname, 
			 longValue[0], doubleValue[0], 
			 doubleValue[1], doubleValue[2], longValue[1], 
			 longValue[2], longValue[3], longValue[4], longValue[5], longValue[6], global_log_name);

#else
    sprintf(app_Info,"%d|%s|%ld|%lf|%lf|%lf|%ld|%ld|%ld|%ld|%s", 
			 _pid, __progname, 
			 longValue[0], doubleValue[0], 
			 doubleValue[1], doubleValue[2], longValue[1], 
			 longValue[2], longValue[3], longValue[4], global_log_name);

#endif
	printf("app_Info: %s \n", app_Info);
    fclose(file);
    return app_Info;
}






//This is YJ Code.
#ifdef MOCO
hbmq_t *connectSKB(int *app_Info) {
#else
int connectSKB(int *app_Info) {
#endif
    struct sockaddr_un addr;
    struct sockaddr_in addr_in;
    char buf[512];
    char temp[512];
    int sfd;
#ifdef MOCO
  //  int mq_id_server = 0;
    int mq_ID[2] = { 0, };

    hbmq_t * ret = (hbmq_t*) malloc(sizeof(hbmq_t));    // added by henry

#else
    int mq_ID = 0;
#endif
    int index = 0;
    //char type = 0x10;
    unsigned int type = 0x000;
    unsigned int len = sizeof(unsigned int)*2  + strlen(app_Info);
    int app_id;

    memcpy(buf, &len, sizeof(unsigned int));
    memcpy(buf+4, &type, sizeof(unsigned int));
    memcpy(buf+8, app_Info, strlen(app_Info));

    sfd = socket(AF_INET, SOCK_STREAM, 0); 
    if(sfd == -1) 
    {   
        printf("Error: socket in connectSKB() inet \n");
        return -1; 
    }   

    memset(&addr_in, 0x00, sizeof(struct sockaddr_in));
    addr_in.sin_family = AF_INET;
    addr_in.sin_addr.s_addr = inet_addr(CONNECT_IP);
    addr_in.sin_port = htons(CONNECT_PORT);

    if(connect(sfd, (struct sockaddr *)&addr_in, sizeof(struct sockaddr_in)) == -1)
    {   
        printf("Error: connect in connectSKB() inet \n");
        return -1; 
    }   


    //if (write(sfd, buf, sizeof(buf)) < 0) {
    if (write(sfd, buf, len) < 0) {
        printf("Error: write in connectSKB() \n");
        return -1; 
    }
    else {
        memset(buf, 0, sizeof(buf));    
        read(sfd, buf, sizeof(buf));

        memcpy(&len, &buf[0], sizeof(int));
        memcpy(&type, &buf[4], sizeof(unsigned int));
        memcpy(temp, &buf[8], sizeof(temp));

#ifdef MOCO

        sscanf(temp, "%d|%d|%d", &app_id, &mq_ID[0], &mq_ID[1]); //corrected by henry

	ret->mq_ID[0] = mq_ID[0];
	ret->mq_ID[1] = mq_ID[1];
#else
        sscanf(temp, "%d|%d", &app_id, &mq_ID);
#endif

        printf(" type = %d\n", type);
        printf(" app_id = %d\n", app_id);

#ifdef MOCO
        printf(" mq_ID[1] = %d\n", mq_ID[0]);
        printf(" mq_ID[2] = %d\n", mq_ID[1]); //added by henry


#else
        printf(" mq_ID = %d\n", mq_ID);
#endif
    }   

    shutdown(sfd, 2);
#ifdef MOCO
    return ret;
#else
    return mq_ID;
#endif
}


//Added by jsseok
void disconnectSKB(int pid) {
    struct sockaddr_in addr_in;
    char buf[512];
    int sfd;
    //char type = 0x11;
    unsigned int type = 0x001;
    unsigned int len = sizeof(unsigned int)*2;

    sprintf(buf+8, "%d", pid);
    memcpy(buf+4, &type, sizeof(unsigned int));
    len += strlen(buf+5);
    memcpy(buf, &len, sizeof(unsigned int));

    sfd = socket(AF_INET, SOCK_STREAM, 0); 
    if(sfd == -1) 
    {   
        printf("Error: socket in disconnectSKB() inet \n");
        return -1; 
    }   

    memset(&addr_in, 0x00, sizeof(struct sockaddr_in));
    addr_in.sin_family = AF_INET;
    addr_in.sin_addr.s_addr = inet_addr(CONNECT_IP);
    addr_in.sin_port = htons(CONNECT_PORT);

    if(connect(sfd, (struct sockaddr *)&addr_in, sizeof(struct sockaddr_in)) == -1)
    {   
        printf("Error: connect in disconnectSKB() inet \n");
        return -1; 
    }   


    //if (write(sfd, buf, sizeof(buf)) < 0) {
    if (write(sfd, buf, len) < 0) {
        printf("Error: write in disconnectSKB() \n");
        return -1; 
    }
    
    shutdown(sfd, 2);
}



/**
 * Initialization function for process that
 * wants to register heartbeats
 * @param hb pointer to heartbeat_t
 * @param min_target double
 * @param max_target double
 * @param window_size int64_t
 * @param buffer_depth int64_t
 * @param log_name pointer to char
 */
int heartbeat_init(heartbeat_t* hb, 
        double min_target, 
        double max_target, 
        int64_t window_size,
        int64_t buffer_depth,
        char* log_name) {
    // FILE* file;
	//char logName[100];
    int rc = 0;
    int pid = 0;
    pid_t test_id = 0;

    pid = getpid();
    test_id = getpid();

    //printf("==== YJ Code ====\n");
    //connect to SKB using clientsocket() and get a mq_ID created by server.
    char *app_Info;
#ifdef MOCO
    hbmq_t * mq_info; //added by henry :hbmq_t 
#else
    	int mq_ID;
	int ret;
#endif
    //app_Info = getApp(pid, &min_target, &max_target, &window_size, &buffer_depth, logName);
    app_Info = getApp(pid);
#ifdef MOCO
    mq_info = connectSKB(app_Info);
    if ((mq_info->mq_ID[0]) == -1) {
	    printf("Error: connectSKB in heartbeat_init() \n");
	    return -1;
    }
    printf("mq_ID(ch1) OF HB is : %d\n",mq_info->mq_ID[0]);
    printf("mq_ID(ch2) OF HB is : %d\n",mq_info->mq_ID[1]);

    hb->mq_ID[0] = mq_info->mq_ID[0];
    hb->mq_ID[1] = mq_info->mq_ID[1]; // added by henry
#else
    mq_ID = connectSKB(app_Info);
    if (mq_ID == -1) {
        printf("Error: connectSKB in heartbeat_init() \n");
        return -1;
    }
    hb->mq_ID = mq_ID;

#endif


    hb->state = HB_alloc_state(pid);
    hb->state->pid = pid;

    //if(log_name != NULL) {
    if(strcmp(global_log_name, "NULL") == -1) {
        //hb->text_file = fopen(log_name, "w");
        hb->text_file = fopen(global_log_name, "w");
		printf("log_name = %s %s\n", global_log_name, hb->text_file);
        fprintf(hb->text_file, "Beat    Tag    Timestamp    Global Rate    Window Rate    Instant Rate\n" );
    }
    else 
        hb->text_file = NULL;

    if(getenv("HEARTBEAT_ENABLED_DIR") == NULL)
	{
	printf("HEARTBEAT_ENABLED_DIR not set\n");
        return 1;
	}

    sprintf(hb->filename, "%s/%d", getenv("HEARTBEAT_ENABLED_DIR"), hb->state->pid);  


    //hb->log = HB_alloc_log(hb->state->pid, buffer_depth);
    hb->log = HB_alloc_log(hb->state->pid, global_buffer_depth);

    if(hb->log == NULL)
        rc = 2;

    hb->first_timestamp = hb->last_timestamp = -1;
    //hb->state->window_size = window_size;
    hb->state->window_size = global_window_size;
    //hb->window = (int64_t*) malloc(window_size*sizeof(int64_t));
    hb->window = (int64_t*) malloc(global_window_size*sizeof(int64_t));
    hb->current_index = 0;
#ifdef MOCO
    hb->state->total_alg_num = global_level_total;
    hb->state->add_data_info = global_add_info;
#endif
    //hb->state->min_heartrate = min_target;
    hb->state->min_heartrate = global_min_target;
    //hb->state->max_heartrate = max_target;
    hb->state->max_heartrate = global_max_target;
    hb->state->counter = 0;
    hb->state->buffer_index = 0;
    hb->state->read_index = 0;
    //hb->state->buffer_depth = buffer_depth;
    hb->state->buffer_depth = global_buffer_depth;
    pthread_mutex_init(&hb->mutex, NULL);
    hb->steady_state = 0;
    hb->state->valid = 0;


    hb->binary_file = fopen(hb->filename, "w");
    if ( hb->binary_file == NULL ) {
        return 1;
    }
    fclose(hb->binary_file);


    return rc;
    //return mq_ID;
}

/**
 * Cleanup function for process that
 * wants to register heartbeats
 * @param hb pointer to heartbeat_t
 */
void heartbeat_finish(heartbeat_t* hb) {
    int pid = getpid();
    free(hb->window);
    if(hb->text_file != NULL)
        fclose(hb->text_file);
    remove(hb->filename);

    disconnectSKB(pid);

    /*TODO : need to deallocate log */
}

/**
 * Returns the record for the current heartbeat
 * currently may read old data
 * @param hb pointer to heartbeat_t
 * @see
 * @return 
 */
void hb_get_current(heartbeat_t volatile * hb, 
        heartbeat_record_t volatile * record) {
    // uint64_t read_index =  (hb->state->buffer_index-1) % hb->state->buffer_depth;
    //memcpy(record, &hb->log[hb->state->read_index], sizeof(heartbeat_record_t));
    record->beat         = hb->log[hb->state->read_index].beat;
    record->tag          = hb->log[hb->state->read_index].tag;
    record->timestamp    = hb->log[hb->state->read_index].timestamp;
    record->global_rate  = hb->log[hb->state->read_index].global_rate;
    record->window_rate  = hb->log[hb->state->read_index].window_rate;
    record->instant_rate = hb->log[hb->state->read_index].instant_rate;
}

/**
 * Returns all heartbeat information for the last n heartbeats
 * @param hb pointer to heartbeat_t
 * @param record pointer to heartbeat_record_t
 * @param n integer
 */
int hb_get_history(heartbeat_t volatile * hb,
        heartbeat_record_t volatile * record,
        int n) {
    if(hb->state->counter > hb->state->buffer_index) {
        memcpy(record, 
                &hb->log[hb->state->buffer_index], 
                (hb->state->buffer_index*hb->state->buffer_depth)*sizeof(heartbeat_record_t));
        memcpy(record + (hb->state->buffer_index*hb->state->buffer_depth), 
                &hb->log[0], 
                (hb->state->buffer_index)*sizeof(heartbeat_record_t));
        return hb->state->buffer_depth;
    }
    else {
        memcpy(record, 
                &hb->log[0], 
                hb->state->buffer_index*sizeof(heartbeat_record_t));
        return hb->state->buffer_index;
    }
}

/**
 * Returns the heart rate over the life 
 * of the entire application
 * @param hb pointer to heartbeat_t
 * @return the heart rate (double) over the entire life of the application
 */
double hb_get_global_rate(heartbeat_t volatile * hb) {
    //uint64_t read_index =  (hb->state->buffer_index-1) % hb->state->buffer_depth;
    //printf("Reading from %lld\n", (long long int) read_index);
    return hb->log[hb->state->read_index].global_rate;
}

/**
 * Returns the heart rate over the last 
 * window (as specified to init) heartbeats
 * @param hb pointer to heartbeat_t
 * @return the heart rate (double) over the last window 
 */
double hb_get_windowed_rate(heartbeat_t volatile * hb) {
    //uint64_t read_index =  (hb->state->buffer_index-1) % hb->state->buffer_depth;
    //printf("Reading from %lld\n", (long long int) read_index);
    return hb->log[hb->state->read_index].window_rate;
}

/**
 * Returns the minimum desired heart rate
 * @param hb pointer to heartbeat_t
 * @return the minimum desired heart rate (double)
 */
double hb_get_min_rate(heartbeat_t volatile * hb) {
    return hb->state->min_heartrate;
}

/**
 * Returns the maximum desired heart rate
 * @param hb pointer to heartbeat_t
 * @return the maximum desired heart rate (double)
 */
double hb_get_max_rate(heartbeat_t volatile * hb) {
    return hb->state->max_heartrate;
}

/**
 * Returns the size of the sliding window 
 * used to compute the current heart rate
 * @param hb pointer to heartbeat_t 
 * @return the size of the sliding window (int64_t)
 */
int64_t hb_get_window_size(heartbeat_t volatile * hb) {
    return hb->state->window_size;
}

/**
 * Helper function to compute windowed heart rate
 * @param hb pointer to heartbeat_t
 * @param time int64_t
 */
static inline float hb_window_average(heartbeat_t volatile * hb, 
        int64_t time) {
    int i;
    double average_time = 0;
    double fps;



    if(!hb->steady_state) {
        hb->window[hb->current_index] = time;

        for(i = 0; i < hb->current_index+1; i++) {
            average_time += (double) hb->window[i];
        }
        average_time = average_time / ((double) hb->current_index+1);
        hb->last_average_time = average_time;
        hb->current_index++;
        if( hb->current_index == hb->state->window_size) {
            hb->current_index = 0;
            hb->steady_state = 1;
        }
    }
    else {
        average_time = 
            hb->last_average_time - 
            ((double) hb->window[hb->current_index]/ (double) hb->state->window_size);
        average_time += (double) time /  (double) hb->state->window_size;

        hb->last_average_time = average_time;

        hb->window[hb->current_index] = time;
        hb->current_index++;

        if( hb->current_index == hb->state->window_size)
            hb->current_index = 0;
    }
    fps = (1.0 / (float) average_time)*1000000000;

    return fps;
}

/**
 * 
 * @param hb pointer to heartbeat_t
 */
static void hb_flush_buffer(heartbeat_t volatile * hb) {
    int64_t i;
    int64_t nrecords = hb->state->buffer_depth;

    //printf("Flushing buffer - %lld records\n", 
    //	 (long long int) nrecords);

    if(hb->text_file != NULL) {
        for(i = 0; i < nrecords; i++) {
            fprintf(hb->text_file, 
                    "%lld    %d    %lld    %f    %f    %f\n", 
                    (long long int) hb->log[i].beat,
                    hb->log[i].tag,
                    (long long int) hb->log[i].timestamp,
                    hb->log[i].global_rate,
                    hb->log[i].window_rate,
                    hb->log[i].instant_rate);
        }

        fflush(hb->text_file);
    }
}


#ifdef MOCO
int mq_receive(heartbeat_t * inf)
{
  int ret, wt_level;
  msgbuf mb;
  
//  ret = msgrcv((inf->mq_ID[1]), &mb, sizeof(mb.mtext), 1, IPC_NOWAIT);
  ret = msgrcv((inf->mq_ID[1]), &mb, sizeof(mb.mtext), 1, 0);
  if(ret == -1){
	printf("Error: msgrcv in mq_receive(int)\n");
	wt_level = -1;
  }
  else
  sscanf(mb.mtext, "%d", &wt_level);

  return wt_level;

}

/* Added by Henry : process(decision making) received message from Server */

// 0 : fail to make a decision  1 : success to make a decision
int res_mode = 1;
int hb_decision( heartbeat_t * hb )
{

	int level; // decision level(algorithm)
	int flag = 0;
	int i;

	level = mq_receive(hb);
//	printf("\nMQ received : %d\n", level);
	//getchar();
	if( level <= 0 )
		level = res_mode;

	else
		res_mode = level; // FIXME preparation for mq_receive error

	

	return level;
	
}

#endif

void mq_snd(int pid, int mq_ID, double window, double instant, double global, int hb_id, int64_t ts) {
    int ret;
    msgbuf mb;

    mb.mtype = 1;

    sprintf(mb.mtext, "%d|%lld|%d|%lf|%lf|%lf", pid, ts, hb_id, window, instant, global);
    ret = msgsnd(mq_ID, &mb, sizeof(mb.mtext), 0);
    if (ret == -1) {
        printf("Error: msgsnd in mq_snd() \n");
    }

    /*	
        ret = msgctl(mq_ID, IPC_SET, &mb);
        if (ret < 0)
        printf("Error: msgctl in mq_snd() \n");
     */	
}


#ifdef MOCO
//FIXME : message type use
//FIXME : combine with heartbeat() or not
void mq_snd2( int pid, int mq_ID, double window, double instant, double global, int hb_id, int64_t ts, double arg_1, double arg_2, double arg_3, double arg_4)
{
	msgbuf mb;
	mb.mtype = 1;
	int ret;

    sprintf(mb.mtext, "%d|%lld|%d|%lf|%lf|%lf|%lf|%lf|%lf|%lf", pid, ts, hb_id, window, instant, global,arg_1,arg_2,arg_3,arg_4);


	ret = msgsnd(mq_ID, &mb, sizeof(mb.mtext), 0);
	if (ret == -1) {
        	printf("Error: msgsnd in msg_snd() \n");
    	}

}


#endif

/**
 * Registers a heartbeat
 * @param hb pointer to heartbeat_t
 * @param tag integer
 */
//int64_t heartbeat( heartbeat_t* hb, int tag )
//This is YJ Code.
int64_t heartbeat( heartbeat_t* hb, int tag )
{

    struct timespec time_info;
    int64_t time;
    int64_t old_last_time = hb->last_timestamp;

    //printf("Registering Heartbeat\n");
    clock_gettime( CLOCK_REALTIME, &time_info );

    time = ( (int64_t) time_info.tv_sec * 1000000000 + (int64_t) time_info.tv_nsec );
    pthread_mutex_lock(&hb->mutex);
    hb->last_timestamp = time;

    if(hb->first_timestamp == -1) {
        //printf("In heartbeat - first time stamp\n");
        hb->first_timestamp = time;
        hb->last_timestamp  = time;
        hb->window[0] = 0;

        //printf("             - accessing state and log\n");
        hb->log[0].beat = hb->state->counter;
        hb->log[0].tag = tag;
        hb->log[0].timestamp = time;
        hb->log[0].window_rate = 0;
        hb->log[0].instant_rate = 0;
        hb->log[0].global_rate = 0;
        hb->state->counter++;
        hb->state->buffer_index++;
        hb->state->valid = 1;
    }
    else {
        //printf("In heartbeat - NOT first time stamp - read index = %d\n",hb->state->read_index );
        int index =  hb->state->buffer_index;
        hb->last_timestamp = time;
        double window_heartrate = hb_window_average(hb, time-old_last_time);
        double global_heartrate = 
            (((double) hb->state->counter+1) / 
             ((double) (time - hb->first_timestamp)))*1000000000.0;
        double instant_heartrate = 1.0 /(((double) (time - old_last_time))) * 
            1000000000.0;

        hb->log[index].beat = hb->state->counter;
        hb->log[index].tag = tag;
        hb->log[index].timestamp = time;
        hb->log[index].window_rate = window_heartrate;
        hb->log[index].instant_rate = instant_heartrate;
        hb->log[index].global_rate = global_heartrate;
        hb->state->buffer_index++;
        hb->state->counter++;
        hb->state->read_index++;

        //This is YJ Code.
#ifdef MOCO
        if ((hb->mq_ID[0]) > 0)
#else
        if (hb->mq_ID > 0)
#endif
        {
#ifdef MOCO//FIXME : expansion mode 

if(  hb->state->add_data_info == 0 ){
            mq_snd(hb->state->pid, hb->mq_ID[0], window_heartrate, instant_heartrate, global_heartrate, hb->log[index].beat, hb->last_timestamp); 
}

else if( hb->state->add_data_info == 1 ){
            mq_snd2(hb->state->pid, hb->mq_ID[0], window_heartrate, instant_heartrate, global_heartrate, hb->log[index].beat, hb->last_timestamp, hb->subdata.arg_1, hb->subdata.arg_2, hb->subdata.arg_3, hb->subdata.arg_4); 


}


#else
            mq_snd(hb->state->pid, hb->mq_ID, window_heartrate, instant_heartrate, global_heartrate, hb->log[index].beat, hb->last_timestamp); 
#endif
        }

        if(hb->state->buffer_index%hb->state->buffer_depth == 0) {
            if(hb->text_file != NULL)
                hb_flush_buffer(hb);
            hb->state->buffer_index = 0;
        }
        if(hb->state->read_index%hb->state->buffer_depth == 0) {
            hb->state->read_index = 0;
        }
    }
    pthread_mutex_unlock(&hb->mutex);
    return time;

}




#if 0

/**
 * Initializes a heartbeat monitor
 * @param hb pointer to heartbeat_t
 * @param pid integer
 * @return rc
 */
int heart_monitor_init(heartbeat_t volatile * hb, int pid) {
    int shmid;
    key_t key;
    int rc = 0;

    key = pid;

    /* Need to find a way to incorporate window size into this */
    if((shmid = shmget(key, 1*sizeof(heartbeat_record_t), 0666)) < 0) {
        rc = 1;
    }

    if ((hb->monitor_log = (HB_record_t*) shmat(shmid, NULL, 0)) == (HB_record_t*) -1) {
        rc = 1;
    }


    return rc;

}


/**
 * Finalizes the heartbeat monitor
 * @param hb pointer to heartbeat_t
 */
void heart_monitor_finish(heartbeat_t* heart) {

    // TODO: put something here.

}

#endif

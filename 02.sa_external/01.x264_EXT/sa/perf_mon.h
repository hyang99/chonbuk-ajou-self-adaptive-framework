#ifndef __PERF_MON_H_
#define __PERF_MON_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <inttypes.h>

#include <unistd.h>
#include <pthread.h>
#include <fcntl.h>

#include <sys/ioctl.h>
#include <sys/types.h>
#include <asm/unistd.h>
#include <linux/perf_event.h>
#include <linux/hw_breakpoint.h>

#include "timer.h"

#define MAX_NUM_CPU         8

typedef struct {
  int epoch_cnt;
  uint64_t inst_cnt_total;
  int num_cpu;
  int cpu_is_active[MAX_NUM_CPU];

  // epoch instruction count
  uint64_t seq_inst_cnt;

  // execution time profile
  uint64_t seq_start_time;
  uint64_t seq_end_time;

  double exec_time_total;
  double exec_time_par;
} perf_mon_epoch_t;

typedef struct {
  struct perf_event_attr pe_inst_cnt;
  int fd_inst_cnt;
  uint64_t inst_cnt_epoch;
} perf_mon_cpu_t;

extern perf_mon_epoch_t pm_epoch;
extern perf_mon_cpu_t pm_all;
extern perf_mon_cpu_t pm_cpu[MAX_NUM_CPU];
 
void sa_perf_mon_init();
void perf_epoch_start();
void perf_start_seq_timer();
void perf_stop_seq_timer();

void perf_epoch_stop();
void perf_epoch_close();
void perf_epoch_get_stat();
uint64_t perf_epoch_get_inst();
double perf_epoch_get_time();

#endif


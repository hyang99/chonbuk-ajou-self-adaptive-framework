#ifndef __SA_H
#define __SA_H

#include "perf_mon.h"

#define ENABLE_SA       1

void sa_ext_adapt_simple(int epoch_cnt, perf_mon_epoch_t *pm, int core);

#endif

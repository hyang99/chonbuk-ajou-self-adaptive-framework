#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <ctime>
#include <math.h>
#include <sys/time.h>
#define ROUNDING(x, dig) (floor((x)*pow(float(10), dig) + 0.5f) /pow(float(10), dig))
#define particle_num 1024 // The number of Particles.

using namespace cv;
using namespace std;

struct Particle
{
	CvRect rectangle;
	float likelihood, weight, fitness;
};

struct Face
{
	CvRect face_rect;
	Mat r_Histo, g_Histo, b_Histo;
};

vector<Face> face_pos;
vector<Particle> particles;

int findIndex(double *CDF, int lengthCDF, double value);
Mat ParticleFilter(Mat, int, int);

bool do_draw = false, finish = false;
void onMouse(int, int, int, int, void*);
double normal_random(double mean, double sigma);
Rect box;
#define RED Scalar(0,0,255)

long long get_time(){
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return (tv.tv_sec *1000000)+tv.tv_usec;
}

double elapsed_time(long long start_time, long long end_time){
  return (double)(end_time - start_time)/(1000*1000);
}

double particle_init = 0;
double calc_likeli_weight = 0;
double sum_weight = 0;
double normalize_weight = 0;
double calc_CDF = 0;
double calc_randomnum = 0;
double resampling = 0;
double new_rect = 0;
double print = 0;
double calc_opencv = 0;
int particle_sum = 0;

int main()
{
	//video read(using opencv)
  long long opencv_start = get_time();
	//VideoCapture video("A_ball.avi");
	//assert(video.isOpened());
  //namedWindow("videoPlay");
	Mat frame;
	int frame_num = 0;
	bool first_face = false;
	
  double click_end = 0;
  double obj_end = 0;
  calc_opencv += elapsed_time(opencv_start, get_time());
  int num = 1;
  long long init_start = get_time();
  while(1) {
    ostringstream file_num;
    file_num<<setw(4)<<setfill('0')<<num;
    string number = file_num.str();
    string file_name = "image/Basketball/img/"+number+".jpg";
		Mat part_frame;
		frame = imread(file_name);
		if(frame.empty()) break;
  
/*
    ////////////////////////////////////////////////////////////
    //////////// Mouse Click ///////////////////////////////////
    ////////////////////////////////////////////////////////////

    long long click_start = get_time();
    setMouseCallback("videoPlay", onMouse, (void*) &frame);
    
    while(finish==false){
      Mat tmp = frame.clone();
      if(do_draw) rectangle(tmp, box, RED, 2);
      //imshow("videoPlay", tmp);
      if(waitKey(20)==27) break;
    }

    click_end += elapsed_time(click_start, get_time());

 */   
    ////////////////////////////////////////////////////////////
    //////// Calc Histogram of object which is clicked /////////
    ////////////////////////////////////////////////////////////
    
    box.x = 198; box.y = 214; box.width=34; box.height=81;
    long long calc_obj_start = 0;
    
    if(first_face==false){
      calc_obj_start = get_time();
     
      float range[] = {0,256};
	    const float *ranges = {range};
	    int histSize = 256;
	    int i;
      vector<Mat> channel;
      Mat roi_face = frame(box).clone();
      
      split(roi_face, channel);
      Face cur_face;
		  cur_face.face_rect = box;
		  calcHist(&channel[0], 1, 0, Mat(), cur_face.b_Histo, 1, &histSize, &ranges);
		  calcHist(&channel[1], 1, 0, Mat(), cur_face.g_Histo, 1, &histSize, &ranges);
		  calcHist(&channel[2], 1, 0, Mat(), cur_face.r_Histo, 1, &histSize, &ranges);
		  face_pos.push_back(cur_face);
      first_face = true;
    }
    obj_end += elapsed_time(calc_obj_start, get_time());
    
    /////////////////////////////////////////////////////////////
    ////////////////// Particle Filter///////////////////////////
    /////////////////////////////////////////////////////////////

    frame = ParticleFilter(frame, particle_num, frame_num);
    
    num++;
    
    frame_num++;
    
    ////////////////////////////////////////////////////////////
    ///////// Calc FPS and PPS /////////////////////////////////
    ////////////////////////////////////////////////////////////

    particle_sum += particle_num;
    
    double fps_time = calc_likeli_weight + particle_init + sum_weight + calc_opencv + normalize_weight + calc_CDF + calc_randomnum + resampling + new_rect + print;
    float fps = (frame_num * 1.0)/fps_time;
    fps = ROUNDING(fps,2);
    int pps = (int)particle_sum/frame_num;

    stringstream stream;
    stream << fixed <<setprecision(2)<<fps;
    string fps_s = stream.str();
    string info = fps_s+" F/S , "+to_string(pps)+" P/F";
    putText(frame, info, cvPoint(10,(frame.size().height*5/6)), 2, 0.8, Scalar(255, 255, 255));


    /////////////////////////////////////////////////////////////
    ///////////// show result image /////////////////////////////
    /////////////////////////////////////////////////////////////
    
    opencv_start = get_time();
		//imshow("videoPlay",frame);
		waitKey(33);
    calc_opencv += elapsed_time(opencv_start, get_time());
	}
  long long init_end = elapsed_time(init_start, get_time());
  cout<<"total time : "<<init_end-click_end<<endl;
  cout<<"opencv time : "<<calc_opencv<<endl;
  cout<<"click time : "<<click_end<<endl;
  cout<<"obj calc time : "<<obj_end<<endl;
  cout<<"particle init time : "<<particle_init<<endl;
  cout<<"calc likelihood & weight time : "<<calc_likeli_weight<<endl;
  cout<<"sum_weight time : "<<sum_weight<<endl;
  cout<<"noramlize time : "<<normalize_weight<<endl;
  cout<<"calc_CDF time : "<<calc_CDF<<endl;
  cout<<"calc_randomnum time : "<<calc_randomnum<<endl;
  cout<<"resampling time : "<<resampling<<endl;
  cout<<"new position time : "<<new_rect<<endl;
  cout<<"print particle time : "<<print<<endl;
  cout<<"frame num : "<<frame_num<<endl;
	return 0;
}


Mat ParticleFilter(Mat img, int Part_num, int frame_no)
{	
	srand(time(NULL)*rand()%100000);
	int i;
  ofstream outFile;
  outFile.open("cpu.txt", ios_base::out | ios_base::app);
  
	if(frame_no == 0)//if frame nunber is 0, it is first frame. so randomly put particles into images.
	{
    long long particle_init_start = get_time();
		for(i = 0; i < Part_num; i++)
		{
			Particle new_Part;
			float rand_x, rand_y, rand_w, rand_h;
      rand_w = box.width; rand_h = box.height;
      do{
        rand_x = normal_random(img.size().width/2, 6);
        rand_y = normal_random(img.size().height/2, 6);
      } while((rand_x<=0||rand_x>=img.size().width-rand_w)||(rand_y<=0||rand_y>=img.size().height-rand_h));
      new_Part.rectangle = cvRect(rand_x, rand_y, rand_w, rand_h);
      particles.push_back(new_Part);
      
		}
    particle_init += elapsed_time(particle_init_start, get_time());
	}
	

  //likelihood
	long long calc_likeli_start = get_time();
  float range[] = {0,256};
	const float *ranges = {range};
	int histSize = 256;
  
	for(i = 0; i < particles.size(); i++)
	{
		float rCorr, gCorr, bCorr;
		Mat roi_img = img(particles[i].rectangle).clone();
    vector<Mat> channels;
		split(roi_img,channels);
		Mat rHisto, gHisto, bHisto;
		calcHist(&channels[0], 1, 0, Mat(), bHisto, 1, &histSize, &ranges);
		calcHist(&channels[1], 1, 0, Mat(), gHisto, 1, &histSize, &ranges);
		calcHist(&channels[2], 1, 0, Mat(), rHisto, 1, &histSize, &ranges);
    rCorr = compareHist(rHisto, face_pos[0].r_Histo, CV_COMP_CORREL);
		gCorr = compareHist(gHisto, face_pos[0].g_Histo, CV_COMP_CORREL);
		bCorr = compareHist(bHisto, face_pos[0].b_Histo, CV_COMP_CORREL);
    particles[i].likelihood = (rCorr*0.4 + gCorr*0.3 + bCorr*0.3);
		particles[i].weight = pow(2.718281828, -16.0 * (1-particles[i].likelihood));
  }
  calc_likeli_weight += elapsed_time(calc_likeli_start, get_time());
	
  
  //sum_weight
  long long sum_start = get_time();
  double sumWeight = 0;
	for(i = 0; i < particles.size(); i++)
	{
		sumWeight += particles[i].weight;
	}
  sum_weight += elapsed_time(sum_start, get_time());


	//normalize weight
  long long normalize_start = get_time();
  vector<Particle> new_part;
  for(i = 0; i <particles.size(); i++)
  {
    particles[i].fitness = particles[i].weight/sumWeight;
  }
  normalize_weight += elapsed_time(normalize_start, get_time());
 
 
  Particle best_part;


  //calc_CDF
  long long calc_CDF_start = get_time();
  double CDF[particles.size()];
  for(i = 0; i < particles.size(); i++)
  {
    if(i==0){
      CDF[i] = particles[i].fitness;
      best_part = particles[i];
    }
    if(best_part.fitness < particles[i].fitness)best_part = particles[i];
    CDF[i] = particles[i].fitness + CDF[i-1];
  }
  calc_CDF += elapsed_time(calc_CDF_start, get_time());

  outFile<<frame_no<<" "<<best_part.likelihood<<endl;

/*
  //calc randomnum
  long long calc_random_start = get_time();
  //vector<double> randnum;
  randnum.resize(particles.size());
  double randnum[particles.size()];
  for(i = 0; i < particles.size(); i++)
  {
    randnum[i] = (double)rand()/RAND_MAX;
  }
  calc_randomnum += elapsed_time(calc_random_start, get_time());
*/
  
  //resampling
  long long resampling_start = get_time();
  new_part.resize(particles.size());
  for(i = 0; i < particles.size(); i++)
  {
    double randnum = (double)rand()/RAND_MAX;
    int index = findIndex(CDF, particles.size(), randnum);
    new_part[i] = particles.at(index);
    int rand_w, rand_h, new_x, new_y;
    rand_w = new_part[i].rectangle.width;
    rand_h = new_part[i].rectangle.height;
    do{
      new_x = normal_random(new_part[i].rectangle.x, 6);
      new_y = normal_random(new_part[i].rectangle.y, 6);
    }
    while((new_x<0||new_x>=img.size().width-rand_w)||(new_y<0||new_y>=img.size().height-rand_h));
			
    new_part[i].rectangle = cvRect(new_x, new_y, rand_w, rand_h);

    //new_part.push_back(particles.at(index));
  }
  resampling += elapsed_time(resampling_start, get_time());

/*
  //new_position
  long long position_start = get_time();
  for(i = 0; i < new_part.size(); i++)
  {
    int rand_w, rand_h, new_x, new_y;
    rand_w = new_part[i].rectangle.width;
    rand_h = new_part[i].rectangle.height;
    do{
      new_x = normal_random(new_part[i].rectangle.x, 10);
      new_y = normal_random(new_part[i].rectangle.y, 10);
    }
    while((new_x<0||new_x>=img.size().width-rand_w)||(new_y<0||new_y>=img.size().height-rand_h));
			
    new_part[i].rectangle = cvRect(new_x, new_y, rand_w, rand_h);
  }
  new_rect += elapsed_time(position_start,get_time());
*/
  particles.clear();
  particles.resize(new_part.size());
  copy(new_part.begin(), new_part.end(), particles.begin());
  new_part.clear();
  
  //print
  long long print_start = get_time();
	for (i = 0; i < particles.size(); i++)
	{
		Point center(particles[i].rectangle.x+particles[i].rectangle.width/2, particles[i].rectangle.y+particles[i].rectangle.height/2);
		circle(img, center, 2, Scalar(0, 255, 0), 2);
    
    Point pt1, pt2;

    pt2.x = particles[i].rectangle.x;
    pt1.x = particles[i].rectangle.x+particles[i].rectangle.width;
    pt1.y = particles[i].rectangle.y;
    pt2.y = particles[i].rectangle.y+particles[i].rectangle.height;
  
    Point best1, best2;
    best2.x = best_part.rectangle.x;
    best1.x = best_part.rectangle.x + best_part.rectangle.width;
    best1.y = best_part.rectangle.y;
    best2.y = best_part.rectangle.y + best_part.rectangle.height;

    rectangle(img,best1, best2, Scalar(255,0,0), 1, 8, 0); 
    //rectangle(img,pt1, pt2, Scalar(255, 0, 0), 1, 8, 0);
  }
  print += elapsed_time(print_start, get_time());
	outFile.close();
  return img;
}

int findIndex(double *CDF, int lengthCDF, double value)
{
  int index = -1;
  int x;
  for(x = 0; x < lengthCDF; x++){
    if(CDF[x] >= value){
      index = x;
      break;
    }
  }
  if(index == -1){
    return lengthCDF-1;
  }
  return index;
}

void onMouse(int event, int x, int y, int, void*)
{
  if(event == CV_EVENT_MOUSEMOVE && !finish){
    box.width = x - box.x;
    box.height = y - box.y;
  }
  else if(event==CV_EVENT_LBUTTONDOWN && !finish){
    do_draw = true;
    box = Rect(x,y,0,0);
  }
  else if(event == CV_EVENT_LBUTTONUP && !finish){
    do_draw= false;
    if(box.width<0) {box.x += box.width; box.width*=-1;}
    if(box.height<0) {box.y += box.height; box.height*=-1;}
    finish = true;
  }
}

double normal_random(double mean, double sigma)
{
  static double V1, V2, S;
  static int phase = 0;
  double X;

  if(phase == 0){
    do
    {
      double U1 = (double)rand()/RAND_MAX;
      double U2 = (double)rand()/RAND_MAX;
      V1 = 2*U1 -1;
      V2 = 2*U2 -1;
      S = V1 * V1 + V2 * V2;
    }while(S >= 1 || S == 0);
    X = V1 * sqrt(-2 * log(S)/S);
  }
  else
    X = V2 * sqrt(-2 * log(S)/S);
  phase = 1-phase;
  return mean + sigma * X;

}
      

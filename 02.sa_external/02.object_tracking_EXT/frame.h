#ifndef FRAME_H_
#define FRAME_H_

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/perf_event.h>
#include <asm/unistd.h>
#include <inttypes.h>
#include <time.h>
#include <math.h>
#include <omp.h>

#define POWER 0.084
#define POWER_core 40
#define CLOCK_NUM 10
#define THRESHOLD 1
#define CORE_NUM 4
#define ISSUE_WIDTH 3

int core=2;
int clock_count=0;

double heartbeat_list[4]={2.5, 1, 2, 1.5};
int Frequency_table[30]={400000, 500000, 600000, 700000, 800000, 900000, 1000000, 1100000, 1200000, 1300000, 1400000, 1500000, 1600000, 1700000, 18000000, 1900000, 2000000};

#endif

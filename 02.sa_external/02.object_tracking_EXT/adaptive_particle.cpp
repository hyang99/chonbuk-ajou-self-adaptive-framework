#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <vector>
#include <ctime>
#include <math.h>
#include <sys/time.h>
#include <omp.h>

#include "./sa/sa.h"
//#include "./perf_h.h"
#include "./frame.h"

/********************************************************
 * heartbeat API header 
 * NOTE: user defined section I
 ********************************************************/
#include "heartbeat.h"

#define TOTAL_ITERATION 10
#define LITTLE 0
#define BIG 1

#define degree_bound 10
#define particle_bound 32
#define speed_bound 5
#define stop_bound 5
#define R_hb 3.0

#define particle_num 1024
#define ROUNDING(x, dig) (floor((x)*pow(float(10), dig) + 0.5f) /pow(float(10),dig))

using namespace cv;
using namespace std;

//extern "C" int heartbeat_init(heartbeat_t* hb, double min_target, double max_target, int64_t window_size, int64_t buffer_depth, char* log_name);
//extern "C" void heartbeat_finish(heartbeat_t* hb);
//extern "C" int64_t heartbeat( heartbeat_t* hb, int tag );
//extern "C" double hb_get_global_rate(heartbeat_t volatile * hb);
//extern "C" double hb_get_windowed_rate(heartbeat_t volatile * hb);
//extern "C" void hb_get_current(heartbeat_t volatile * hb, heartbeat_record_t volatile * record);

extern "C" void sa_ext_adapt_simple(int epoch_cnt, perf_mon_epoch_t *pm, int core);
extern "C" void sa_perf_mon_init();
extern "C" void perf_epoch_start();
extern "C" void perf_start_seq_timer();
extern "C" void perf_stop_seq_timer();
extern "C" void perf_epoch_stop();
extern "C" void perf_epoch_close();
extern "C" void perf_epoch_get_stat();
//extern "C" uint64_t perf_epoch_get_inst();
extern "C" uint64_t perf_epoch_get_l_inst();
extern "C" uint64_t perf_epoch_get_b_inst();
extern "C" double perf_epoch_get_time();


struct Particle
{
	CvRect rectangle;
	float weight, fitness;
};

struct Dir_vector
{
  Point start;
  Point end;
};

struct Face
{
	CvRect face_rect;
	Mat r_Histo, g_Histo, b_Histo;
};

vector<Face> face_pos;
vector<Particle> particles;
Dir_vector exist_vec, new_vec;
unsigned int new_num_particle = 0;

int findIndex(double *CDF, int lengthCDF, double value);
Mat ParticleFilter(Mat, unsigned int, int);

bool do_draw = false, finish = false;
void onMouse(int, int, int, int, void*);
double normal_random(double mean, double sigma);
Rect box;
#define RED Scalar(0,0,255)

long long int get_time();
double elapsed_time(long long int, long long int);

long long int get_time(){
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return (tv.tv_sec *1000000)+tv.tv_usec;
}

double elapsed_time(long long int start_time, long long int end_time){
	return (double)(end_time - start_time)/(1000*1000);
}

double particle_init = 0;
double calc_likeli_weight = 0;
double sum_weight = 0;
double normalize_weight = 0;
double calc_CDF = 0;
double calc_randomnum = 0;
double resampling = 0;
double new_rect = 0;
double calc_opencv = 0;
double print2 = 0;
int particle_sum = 0;




/********************************************************
 * heartbeat API header 
 * NOTE: user defined section II
 ********************************************************/
heartbeat_t hb;

int fd;
char buff[1024];


int main(int argc, char **argv)
{
	int flag=-1, cnt2;


	FILE   *coref, *freqf;
	FILE *heart_beat, *workload;

	coref=fopen("core.txt","w");
  freqf=fopen("freq.txt","w");
	heart_beat=fopen("heart_beat.txt", "w");
  workload=fopen("workload.txt", "w");

	double hb_val=0, time_p;

	//video read
	long long opencv_start = get_time();
	//	VideoCapture video(argv[1]);

	//VideoCapture video("./A_ball.avi");
	//assert(video.isOpened());
	//namedWindow("videoPlay");
	//namedWindow("roi");
	Mat frame;
	int frame_num = 0;
	bool first_face = false;

	int num = 1;
	double obj_end = 0;
	calc_opencv += elapsed_time(opencv_start, get_time());
	
  new_num_particle = particle_num;

  //        long long init_start = get_time();
	int max_frame, ret;
	uint64_t l_inst, b_inst;

	max_frame=atoi(argv[1]);
	core=atoi(argv[2]);
	cnt2=0;




	/********************************************************
	 * heartbeat API initialization 
	 * NOTE: user defined section III
	 ********************************************************/
	ret = heartbeat_init(&hb, 0, 0, 0, 0, NULL);
	if(ret <= 0)
		printf("Error: mQ_ID is abnormally allocated\n");
	heartbeat(&hb, cnt2++);	//FIXME heartbeat tagging for avoiding ignorance of value at 1st iteration




fprintf(heart_beat, "target_QoS, current_hb\n");


#if ENABLE_SA
	sa_perf_mon_init();


#endif       
	while(1) {

#if ENABLE_SA
		sa_ext_adapt_simple(frame_num, &pm_epoch, core);
		perf_start_seq_timer();
		perf_epoch_start();
#endif

		printf("\n\t\t[frame_num = %d]\n\n\n", frame_num);
		printf("frame = %d\n", frame_num);


		ostringstream file_num;
		file_num<<setw(4)<<setfill('0')<<num;
		string number = file_num.str();
		string file_name = "image/Basketball/img/"+number+".jpg";
		Mat part_frame;
		frame = imread(file_name);
		if(frame.empty()) break;

		box.x = 198; box.y=214; box.width=44;box.height=81;

		long long calc_obj_start = 0;
		if(first_face==false){
			calc_obj_start = get_time();
			float range[] = {0,256};
			const float *ranges = {range};
			int histSize = 256;
			vector<Mat> channel;
			Mat roi_face = frame(box).clone();
			//imshow("roi", roi_face);
			split(roi_face, channel);
			Face cur_face;
			cur_face.face_rect = box;
			calcHist(&channel[0], 1, 0, Mat(), cur_face.b_Histo, 1, &histSize, &ranges);
			calcHist(&channel[1], 1, 0, Mat(), cur_face.g_Histo, 1, &histSize, &ranges);
			calcHist(&channel[2], 1, 0, Mat(), cur_face.r_Histo, 1, &histSize, &ranges);
			face_pos.push_back(cur_face);
			first_face = true;
		}
		obj_end += elapsed_time(calc_obj_start, get_time());

		frame = ParticleFilter(frame, new_num_particle, frame_num);

		frame_num++;
		num++;

		particle_sum += new_num_particle;
		double fps_time = calc_likeli_weight + particle_init + sum_weight + calc_opencv + normalize_weight + calc_CDF + calc_randomnum + resampling + new_rect + print2;
		float fps = (frame_num*1.0)/fps_time;
		fps = ROUNDING(fps,2);
		int pps = (int)particle_sum/frame_num;

		stringstream stream;
		stream <<fixed<<setprecision(2)<<fps;
		string fps_s = stream.str();
		string info = fps_s + " F/S , " + to_string(pps) +" P/F";
		putText(frame, info, cvPoint(10, (frame.size().height*5/6)), 2, 0.8, Scalar(255, 255, 255));

    cout<<"pps"<<pps<<endl;
    opencv_start = get_time();
		//imshow("videoPlay",frame);
		waitKey(33);
		calc_opencv += elapsed_time(opencv_start, get_time());

		///////////////////////////////////////////



fd=open("/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq", O_RDONLY);
read(fd, buff, 1024);
fprintf(freqf,"%s", buff);
printf("frequency = %s", buff);


#if ENABLE_SA

    l_inst=perf_epoch_get_l_inst();
    b_inst=perf_epoch_get_b_inst();
    time_p=perf_epoch_get_time();

    cout << "l_inst = " << l_inst << endl;
    cout << "b_inst = " << b_inst << endl;
    cout << "time_p = " << time_p << endl;
    perf_stop_seq_timer();
    perf_epoch_stop();
    perf_epoch_get_stat();
    perf_epoch_close();

#endif

#ifdef MOCO

		/********************************************************
		 * heartbeat API additional data to send
		 * NOTE: user defined section V
		 ********************************************************/
		hb.subdata.arg_1 = time_p;
		hb.subdata.arg_2 = l_inst;
		hb.subdata.arg_3 = b_inst;
		hb.subdata.arg_4 = frame_num-1;

#endif 

		/********************************************************
		 * heartbeat API tagging & read value 
		 * NOTE: user defined section VI
		 ********************************************************/
		heartbeat(&hb, cnt2++);
		heartbeat_record_t temp_hb_rec;
		hb_get_current(&hb, &temp_hb_rec);
		hb_val = temp_hb_rec.instant_rate;

		fprintf(coref,"%d\n",core);
    fprintf(workload,"%d\n", new_num_particle); 

		flag=0;

    fprintf(heart_beat,"%f %f\n", R_hb,(double)hb_val);


#ifdef MOCO

		/********************************************************
		 * heartbeat API receive Mode from SKB-server
		 * NOTE: user defined section VI
		 ********************************************************/

		int lev;
		lev = hb_decision( &hb );
		core = lev;	


		printf("\n       Current hb rate:[   %f   ] \n", hb_val);

		printf("       [ %d Cores] will work on this system !\n",core);
		printf("========================================================\n\n\n");

#endif


		if(frame_num>max_frame) break;
	}



	/********************************************************
	 * heartbeat API finalization
	 * NOTE: user defined section VII
	 ********************************************************/
	heartbeat_finish( &hb );





	fclose(coref );
	fclose( heart_beat );

	return 0;
}

Mat ParticleFilter(Mat img, unsigned int Part_num, int frame_no)
{	
	srand(time(NULL)*rand()%100000);
	unsigned int i;

	omp_set_num_threads(CORE_NUM);
	if(frame_no == 0)//ùframe
	{
		particles.resize(Part_num);
		Particle *ptr_particle = &particles[0];
		long long particle_init_start = get_time();

#pragma parallel for shared(img.size()) private(i)
		for(i = 0; i < Part_num; i++)
		{
			Particle new_Part;
			float rand_x, rand_y, rand_w, rand_h;
			rand_w = box.width; rand_h = box.height;
			do{
				rand_x = normal_random(img.size().width/2, 10);
				rand_y = normal_random(img.size().height/2, 10);
			} while((rand_x<=0||rand_x>=img.size().width-rand_w)||(rand_y<=0||rand_y>=img.size().height-rand_h));
			new_Part.rectangle = cvRect(rand_x, rand_y, rand_w, rand_h);
			//#pragma omp critical
			//particles.push_back(new_Part);
			ptr_particle[i] = new_Part;
		}
		particle_init += elapsed_time(particle_init_start, get_time());
	}

	//likelihood
	long long calc_likeli_start = get_time();
	/*float range[] = {0,256};
	  const float *ranges = {range};
	  int histSize = 256;*/

#pragma omp parallel for shared(particles, face_pos) private(i)
	for(i = 0; i < particles.size(); i++)
	{
		float range[] = {0,256};
		const float *ranges = {range};
		int histSize = 256;
		float likelihood, rCorr, gCorr, bCorr;
		Mat roi_img = img(particles[i].rectangle).clone();
		vector<Mat> channels;
		split(roi_img,channels);
		Mat rHisto, gHisto, bHisto;
		calcHist(&channels[0], 1, 0, Mat(), bHisto, 1, &histSize, &ranges);
		calcHist(&channels[1], 1, 0, Mat(), gHisto, 1, &histSize, &ranges);
		calcHist(&channels[2], 1, 0, Mat(), rHisto, 1, &histSize, &ranges);
		rCorr = compareHist(rHisto, face_pos[0].r_Histo, CV_COMP_CORREL);
		gCorr = compareHist(gHisto, face_pos[0].g_Histo, CV_COMP_CORREL);
		bCorr = compareHist(bHisto, face_pos[0].b_Histo, CV_COMP_CORREL);
		likelihood = (rCorr*0.4 + gCorr*0.3 + bCorr*0.3);
		particles[i].weight = pow(2.718281828, -16.0 * (1-likelihood));
	}
	calc_likeli_weight += elapsed_time(calc_likeli_start, get_time());


	//sum_weight
	long long sum_start = get_time();
	double sumWeight = 0;
#pragma omp parallel for private(i) reduction(+:sumWeight)
	for(i = 0; i < particles.size(); i++)
	{
		sumWeight += particles[i].weight;
	}
	sum_weight += elapsed_time(sum_start, get_time());


	//normalize weight
	long long normalize_start = get_time();
	vector<Particle> new_part;
#pragma omp parallel for shared(sumWeight, particles) private(i)
	for(i = 0; i <particles.size(); i++)
	{
		particles[i].fitness = particles[i].weight/sumWeight;
	}

	//	ioctl(ic_c, PERF_EVENT_IOC_DISABLE, 0);

	//       perf_parallel_stop();

	//     perf_sequential_start();
	//	ioctl(sic_c, PERF_EVENT_IOC_ENABLE, 0);

	normalize_weight += elapsed_time(normalize_start, get_time());

	Particle best_part;

	//calc_CDF
	long long calc_CDF_start = get_time();
	double CDF[particles.size()];

	for(i = 0; i < particles.size(); i++)
	{
		if(i==0) {
			CDF[i] = particles[i].fitness;
			best_part = particles[i];
		}
		else if(best_part.fitness < particles[i].fitness) best_part = particles[i];
		else if(i!=0) CDF[i] = particles[i].fitness + CDF[i-1];
	}
	calc_CDF += elapsed_time(calc_CDF_start, get_time());
	

  //calc direction vector
  if(frame_no == 0){
    Point init_center(best_part.rectangle.x+best_part.rectangle.width/2, best_part.rectangle.y+best_part.rectangle.height/2);
    exist_vec.start = init_center;
  }
  else if(frame_no == 1){
    Point next_center(best_part.rectangle.x+best_part.rectangle.width/2, best_part.rectangle.y+best_part.rectangle.height/2);
    exist_vec.end = next_center;
    new_vec.start = next_center;
  }
  else{
    Point last_center(best_part.rectangle.x+best_part.rectangle.width/2, best_part.rectangle.y+best_part.rectangle.height/2);
    new_vec.end = last_center;
  
    //calc vector angle
    float v1, v2, speed;
    v1 = sqrt((exist_vec.end.x-exist_vec.start.x)*(exist_vec.end.x-exist_vec.start.x) + (exist_vec.end.y-exist_vec.start.y)*(exist_vec.end.y-exist_vec.start.y));
    v2 = sqrt((new_vec.end.x-new_vec.start.x)*(new_vec.end.x-new_vec.start.x) + (new_vec.end.y - new_vec.start.y)*(new_vec.end.y - new_vec.start.y));
    
    speed = abs(v1-v2);

    //if v1 == 0, v2 == 0
    if(new_num_particle >= particle_bound && new_num_particle <= particle_num){
      if(abs(exist_vec.start.x-new_vec.end.x)<stop_bound && abs(exist_vec.start.y - new_vec.end.y)<stop_bound&& new_num_particle/2 >= particle_bound) {
        new_num_particle/=2;
      }
      
      else{
      
         double theta=1, degree=0;
         theta /= v1*v2;
         theta = acos(theta);
         degree = theta * (180 / 3.141592);
         
         string text = "speed :"+to_string(speed)+" theta : "+to_string(theta);
         putText(img,text,cvPoint(best_part.rectangle.x, best_part.rectangle.y-10),2,0.2,Scalar(255,255,255));
         if(speed <= speed_bound){
           if(degree <= degree_bound)
           {
             if(new_num_particle/2 >= particle_bound) new_num_particle /= 2;
             else new_num_particle = particle_bound;
           }
           else if(degree > degree_bound)
           {
             if(new_num_particle*40 <= particle_num) new_num_particle *= 40;
             else new_num_particle = particle_num;
           }
         }
         else{
           if(degree <= degree_bound)
           {
              if(new_num_particle*30 <= particle_num) new_num_particle *= 30;
              else new_num_particle = particle_num;
           }
           else if(degree > degree_bound)
           {
             if(new_num_particle*20 <= particle_num) new_num_particle *= 20;
             else new_num_particle = particle_num;
           }
         }  
         if(new_num_particle>particle_bound && degree < 90) 
         {
           if(new_num_particle/2 >= particle_bound) new_num_particle /= 2;
           else new_num_particle = particle_bound;
         }
         else if(new_num_particle<=particle_num && degree >= 90)
         {
           if(new_num_particle*35 <= particle_num) new_num_particle *= 35;
           else new_num_particle = particle_num;
         }
     
      }
      exist_vec = new_vec;
      new_vec.start = last_center;
    
    }
  }



/*
	//calc randomnum
	long long calc_random_start = get_time();
	double randnum[particles.size()];
#pragma omp parallel for shared(randnum) private(i)
for(i = 0; i < particles.size(); i++)
{
randnum[i] = (double)rand()/RAND_MAX;
}
calc_randomnum += elapsed_time(calc_random_start, get_time());
	 */
	//resampling
	long long resampling_start = get_time();
	new_part.resize(new_num_particle);


#pragma omp parallel for shared(CDF, particles) private(i)
	for(i = 0; i < new_num_particle; i++)
{
	double randnum = (double)rand()/RAND_MAX;
	int index = findIndex(CDF, particles.size(), randnum);
	//#pragma omp critical
	//new_part.push_back(particles.at(index));
	new_part[i] = particles.at(index);

	int rand_w, rand_h, new_x, new_y;
	rand_w = new_part[i].rectangle.width;
	rand_h = new_part[i].rectangle.height;

	do{
		new_x = normal_random(new_part[i].rectangle.x, 10);
		new_y = normal_random(new_part[i].rectangle.y, 10);
	}

	while((new_x<0||new_x>=img.size().width-rand_w)||(new_y<0||new_y>=img.size().height-rand_h));

	new_part[i].rectangle = cvRect(new_x, new_y, rand_w, rand_h);

}

resampling += elapsed_time(resampling_start, get_time());

particles.clear();
particles.resize(new_part.size());
copy(new_part.begin(), new_part.end(), particles.begin());
new_part.clear();

//print
long long print_start = get_time();
#pragma omp parallel for shared(particles) private(i)
for (i = 0; i < particles.size(); i++)
{
	Point center(particles[i].rectangle.x+particles[i].rectangle.width/2, particles[i].rectangle.y+particles[i].rectangle.height/2);
	circle(img, center, 2, Scalar(0, 255, 0), 2);
}

print2 += elapsed_time(print_start, get_time());

Point best1, best2;
best2.x = best_part.rectangle.x;
best1.x = best_part.rectangle.x + best_part.rectangle.width;
best1.y = best_part.rectangle.y;
best2.y = best_part.rectangle.y + best_part.rectangle.height;

rectangle(img, best1, best2, Scalar(255,0,0), 1, 8, 0);

return img;
}

int findIndex(double *CDF, int lengthCDF, double value)
{
	int index = -1;
	int x;
	for(x = 0; x < lengthCDF; x++){
		if(CDF[x] >= value){
			index = x;
			break;
		}
	}
	if(index == -1){
		return lengthCDF-1;
	}
	return index;
}

void onMouse(int event, int x, int y, int, void*)
{
	if(event == CV_EVENT_MOUSEMOVE && !finish){
		box.width = x - box.x;
		box.height = y - box.y;
	}
	else if(event==CV_EVENT_LBUTTONDOWN && !finish){
		do_draw = true;
		box = Rect(x,y,0,0);
	}
	else if(event == CV_EVENT_LBUTTONUP && !finish){
		do_draw= false;
		if(box.width<0) {box.x += box.width; box.width*=-1;}
		if(box.height<0) {box.y += box.height; box.height*=-1;}
		finish = true;
	}
}

double normal_random(double mean, double sigma)
{
	static double V1, V2, S;
	static int phase = 0;
	double X;

	if(phase == 0){
		do
		{
			double U1 = (double)rand()/RAND_MAX;
			double U2 = (double)rand()/RAND_MAX;
			V1 = 2*U1 -1;
			V2 = 2*U2 -1;
			S = V1 * V1 + V2 * V2;
		}while(S >= 1 || S == 0);
		X = V1 * sqrt(-2 * log(S)/S);
	}
	else
		X = V2 * sqrt(-2 * log(S)/S);
	phase = 1-phase;
	return mean + sigma * X;

}

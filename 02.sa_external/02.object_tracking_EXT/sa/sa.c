#include <assert.h>
#include "sa.h"

void sa_ext_adapt_simple(int epoch_cnt, perf_mon_epoch_t *pm, int core)
{

  int i;
  int start_cpu_id = 0;
  int adapt_period = 16;
  int num_cpu = MAX_NUM_CPU < (epoch_cnt/adapt_period) + 1 ?
                  MAX_NUM_CPU : (epoch_cnt/adapt_period) + 1;
/*
  char* cpu_list[][8] = {
    {"0", "0,1", "0,1,2", "0,1,2,3"},
    {"1", "1,2", "1,2,3", "1,2,3,4"},
    {"2", "2,3", "2,3,4", "2,3,4,5"},
    {"3", "3,4", "3,4,5", "3,4,5,6"}
  };
*/
/*
  char taskset_cmd[1024];
  
  for (i=0; i<num_cpu; i++) pm_epoch.cpu_is_active[i] = 0;
  for (i=0; i<num_cpu; i++) pm_epoch.cpu_is_active[start_cpu_id + i] = 1;

  int pid = getpid();
  sprintf(taskset_cmd, "taskset -pc %s %d > /dev/null 2>&1",
            cpu_list[start_cpu_id][num_cpu-1], pid);
  printf("Executing %s\n", taskset_cmd);
  assert(system(taskset_cmd) != -1);

  pm->num_cpu = num_cpu;
*/  

  char taskset_cmd[1024];
  int pid = getpid();
  sprintf(taskset_cmd, "taskset -pc 0-%d %d > /dev/null 2>&1", core-1, pid);

  for(i=0; i<core; i++)
  {
          pm_epoch.cpu_is_active[i]=1;
  }

  printf("Executing %s\n", taskset_cmd);
  assert(system(taskset_cmd) != -1);

}


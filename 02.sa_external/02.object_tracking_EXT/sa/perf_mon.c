#include "perf_mon.h"

perf_mon_epoch_t pm_epoch;
perf_mon_cpu_t pm_all;
perf_mon_cpu_t pm_cpu[MAX_NUM_CPU];
  
//
// power measurement
//
pthread_t pw_sampling_th;
uint64_t pw_sampling_cnt;
double a15_power_sum, a7_power_sum;

void perf_start_seq_timer()
{
  pm_epoch.seq_start_time = get_time();
}

void perf_stop_seq_timer()
{
  pm_epoch.seq_end_time = get_time();
}

static long perf_event_open(struct perf_event_attr *hw_event, pid_t pid, int cpu, int group_fd, unsigned long flags)
{
	int ret;

	ret = syscall(__NR_perf_event_open, hw_event, pid, cpu,
			group_fd, flags);
	return ret;
}

// power monitor
void power_global_init()
{
	a15_power_sum = 0.0;
	a7_power_sum = 0.0;
	pw_sampling_cnt = 0;
}

void sa_perf_mon_init()
{
  int i;
  pm_epoch.epoch_cnt = 0;
  pm_epoch.inst_cnt_total = 0;
  pm_epoch.num_cpu = 1;
  for (i=0; i<MAX_NUM_CPU; i++) pm_epoch.cpu_is_active[i] = 0;
}

int perf_event_init(struct perf_event_attr *pe, int cpu_id)
{
  int fd;

	memset(pe, 0, sizeof(struct perf_event_attr));
	pe->type = PERF_TYPE_HARDWARE;
	pe->size = sizeof(struct perf_event_attr);
	pe->config = PERF_COUNT_HW_INSTRUCTIONS;
	pe->disabled = 1;
	pe->exclude_kernel = 1;
	pe->exclude_hv = 1;

	fd = perf_event_open(pe, 0, cpu_id, -1, 0);
	if (fd == -1) {
		fprintf(stderr, "Error opening leader %llx\n", pe->config);
		exit(EXIT_FAILURE);
	}

	ioctl(fd, PERF_EVENT_IOC_ENABLE, 0);
  return fd;
}

void perf_epoch_start()
{
  int i;
  pm_all.fd_inst_cnt = perf_event_init(&pm_all.pe_inst_cnt, -1);
  for (i=0; i<MAX_NUM_CPU; i++) {
    if (pm_epoch.cpu_is_active[i]) {
      pm_cpu[i].fd_inst_cnt = perf_event_init(&pm_cpu[i].pe_inst_cnt, i);
    }
  }
}

void perf_epoch_stop()
{
  int i;
	ioctl(pm_all.fd_inst_cnt, PERF_EVENT_IOC_DISABLE, 0);
  for (i=0; i<MAX_NUM_CPU; i++) {
    if (pm_epoch.cpu_is_active[i]) {
	    ioctl(pm_cpu[i].fd_inst_cnt, PERF_EVENT_IOC_DISABLE, 0);
    }
  }
}

void perf_epoch_close()
{
  int i;
	close(pm_all.fd_inst_cnt);
  for (i=0; i<MAX_NUM_CPU; i++) {
    if (pm_epoch.cpu_is_active[i]) {
	    close(pm_cpu[i].fd_inst_cnt);
    }
  }
  pm_epoch.epoch_cnt++;
}

uint64_t perf_get_pm_val(int fd)
{
  uint64_t val;
  if (read(fd, &val, sizeof(uint64_t))==0) {
    printf("[SA_PERF] Error\n");
    exit(1);
  }
  return val;
}

uint64_t perf_epoch_get_l_inst()
{
        uint64_t l_inst=0, i;
        for (i=0; i<4; i++) {
                if (pm_epoch.cpu_is_active[i]) {
                        l_inst+=perf_get_pm_val(pm_cpu[i].fd_inst_cnt);
                        //return perf_get_pm_val(pm_all.fd_inst_cnt);
                }
        }

        return l_inst;
}

uint64_t perf_epoch_get_b_inst()
{
        uint64_t b_inst=0, i;
        for (i=4; i<8; i++) {
                if (pm_epoch.cpu_is_active[i]) {
                        b_inst+=perf_get_pm_val(pm_cpu[i].fd_inst_cnt);
                }
        }

        return b_inst;
}

double perf_epoch_get_time()
{
        return pm_epoch.exec_time_total;
}

void perf_epoch_get_stat()
{
  int i;
  uint64_t seq_inst_cnt;

  seq_inst_cnt = perf_get_pm_val(pm_all.fd_inst_cnt);
  pm_epoch.exec_time_total = (double)(pm_epoch.seq_end_time - pm_epoch.seq_start_time) / 1e6;

  pm_epoch.inst_cnt_total += seq_inst_cnt;

  printf("[Epoch%8d] ", pm_epoch.epoch_cnt);
  printf(" exec time(s) %f ", pm_epoch.exec_time_total);
  printf(" inst_cnt: epoch %"PRIu64" ", seq_inst_cnt);
  for (i=0; i<MAX_NUM_CPU; i++) {
    if (pm_epoch.cpu_is_active[i]) {
      uint64_t inst_cnt = perf_get_pm_val(pm_cpu[i].fd_inst_cnt);
      printf("cpu_%d %"PRIu64", ", i, inst_cnt);
    }
  }
  printf(" inst_cnt_total %"PRIu64"\n", pm_epoch.inst_cnt_total);
}

void pw_epoch_reset()
{
	a15_power_sum=0;
	a7_power_sum=0;
	pw_sampling_cnt=0;
}

void* pw_sampling_main(void *arg) // add
{
	int fd;
	char buff[1024];
	
	while(1)
	{
		if(0 < (fd=open("/sys/bus/i2c/drivers/INA231/2-0040/sensor_W", O_RDONLY)))
		{
      if ( read(fd, buff, 1024)==0 ) {
        printf("[SA_PERF] Error\n");
        exit(1);
      }
			a15_power_sum+=atof(buff);
//			a15_power_sum=atof(buff);
			close(fd);
		}

		if(0 < (fd=open("/sys/bus/i2c/drivers/INA231/2-0045/sensor_W", O_RDONLY)))
		{
      if ( read(fd, buff, 1024)==0 ) {
        printf("[SA_PERF] Error\n");
        exit(1);
      }
			a7_power_sum+=atof(buff);
			close(fd);
		}

//		printf("a15_power_sum = %f\n", a15_power_sum);

		pw_sampling_cnt++;
		usleep(200000);
	}

	pthread_exit((void *) 0);
}


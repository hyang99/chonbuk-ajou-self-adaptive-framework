/*************************************************************************
 *                                                                       * 
 *        N  A  S     P A R A L L E L     B E N C H M A R K S  3.3       *
 *                                                                       * 
 *                                  I S                                  * 
 *		    Pthread version
 *                                                                       * 
 ************************************************************************* 
 * Author: Hoeseok Yang and H. W. Kim					 *
 *************************************************************************/



#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h> // pthread header
#include <sys/time.h>

#include "../inc/heartbeat.h"

/************************************
 *	Heartbeat initialization ****
 ************************************/
heartbeat_t hb;

/**************************************
 * COLOR PRINT INITIALIZATION 
 **************************************/
#define KRED "\x1B[31m"
#define KGRN "\x1B[32m"
#define KYEL "\x1B[33m"
#define KWHT "\x1B[37m"

/**************************************
 * TIME INITIALIZATION 
 **************************************/
struct timeval start;
struct timeval end;

struct timeval tstart;
struct timeval tend;

struct timeval start_1;
struct timeval end_1;

struct timeval start_2;
struct timeval end_2;

double exec_time;
double exec_time_a;
double exec_time_b;


/***************************************
 * dynamic parameters
 * by default, problem size is set to W
 ***************************************/
char CLASS='W';
int num_of_threads=2;
int total_keys_log_2;
int total_keys; /* total num of keys */
int num_keys; /* number of keys per each thread */
int max_key_log_2; 
int max_key; 
int num_buckets_log_2;
int num_buckets;
int min_procs=1;
int max_procs=1;
int size_of_buffers;


/*********************/
/* Pthread properties*/
/*********************/
pthread_barrier_t barrier;

#define  MAX_ITERATIONS      10
#define  TEST_ARRAY_SIZE     5


/*************************************/
/* Typedef: if necessary, change the */
/* size of int here by changing the  */
/* int type to, say, long            */
/*************************************/
typedef  int  INT_TYPE;
typedef  long INT_TYPE2;
//#define MP_KEY_TYPE MPI_INT



/********************/
/* Some global info */
/********************/

INT_TYPE ** key_buff_ptr_global;
INT_TYPE * total_local_keys;
INT_TYPE * total_lesser_keys;


int*      passed_verification;



/************************************/
/* These are the three main arrays. */
/* See SIZE_OF_BUFFERS def above    */
/************************************/
INT_TYPE * bucket_size_totals; /* part. ver. vals */

INT_TYPE **key_array;
INT_TYPE **key_buff1;
INT_TYPE **key_buff2;
INT_TYPE **bucket_size;

int	**send_count;
int	**recv_count;
int	**send_displ;
int	**recv_displ;



/**********************/
/* Partial verif info */
/**********************/
INT_TYPE2 test_index_array[TEST_ARRAY_SIZE],
          test_rank_array[TEST_ARRAY_SIZE],

          S_test_index_array[TEST_ARRAY_SIZE] = 
{48427,17148,23627,62548,4431},
    S_test_rank_array[TEST_ARRAY_SIZE] = 
{0,18,346,64917,65463},

    W_test_index_array[TEST_ARRAY_SIZE] = 
{357773,934767,875723,898999,404505},
    W_test_rank_array[TEST_ARRAY_SIZE] = 
{1249,11698,1039987,1043896,1048018},

    A_test_index_array[TEST_ARRAY_SIZE] = 
{2112377,662041,5336171,3642833,4250760},
    A_test_rank_array[TEST_ARRAY_SIZE] = 
{104,17523,123928,8288932,8388264},

    B_test_index_array[TEST_ARRAY_SIZE] = 
{41869,812306,5102857,18232239,26860214},
    B_test_rank_array[TEST_ARRAY_SIZE] = 
{33422937,10244,59149,33135281,99}, 

    C_test_index_array[TEST_ARRAY_SIZE] = 
{44172927,72999161,74326391,129606274,21736814},
    C_test_rank_array[TEST_ARRAY_SIZE] = 
{61147,882988,266290,133997595,133525895},

    D_test_index_array[TEST_ARRAY_SIZE] = 
{1317351170,995930646,1157283250,1503301535,1453734525},
    D_test_rank_array[TEST_ARRAY_SIZE] = 
{1,36538729,1978098519,2145192618,2147425337};



/***********************/
/* function prototypes */
/***********************/
double	randlc( double *X, double *A );

void full_verify( int my_rank );

/*
 *    FUNCTION RANDLC (X, A)
 *
 *  This routine returns a uniform pseudorandom double precision number in the
 *  range (0, 1) by using the linear congruential generator
 *
 *  x_{k+1} = a x_k  (mod 2^46)
 *
 *  where 0 < x_k < 2^46 and 0 < a < 2^46.  This scheme generates 2^44 numbers
 *  before repeating.  The argument A is the same as 'a' in the above formula,
 *  and X is the same as x_0.  A and X must be odd double precision integers
 *  in the range (1, 2^46).  The returned value RANDLC is normalized to be
 *  between 0 and 1, i.e. RANDLC = 2^(-46) * x_1.  X is updated to contain
 *  the new seed x_1, so that subsequent calls to RANDLC using the same
 *  arguments will generate a continuous sequence.
 *
 *  This routine should produce the same results on any computer with at least
 *  48 mantissa bits in double precision floating point data.  On Cray systems,
 *  double precision should be disabled.
 *
 *  David H. Bailey     October 26, 1990
 *
 *     IMPLICIT DOUBLE PRECISION (A-H, O-Z)
 *     SAVE KS, R23, R46, T23, T46
 *     DATA KS/0/
 *
 *  If this is the first call to RANDLC, compute R23 = 2 ^ -23, R46 = 2 ^ -46,
 *  T23 = 2 ^ 23, and T46 = 2 ^ 46.  These are computed in loops, rather than
 *  by merely using the ** operator, in order to insure that the results are
 *  exact on all systems.  This code assumes that 0.5D0 is represented exactly.
 */


/*****************************************************************/
/*************           R  A  N  D  L  C             ************/
/*************                                        ************/
/*************    portable random number generator    ************/
/*****************************************************************/

double	randlc( double *X, double *A )
{
    static int        KS=0;
    static double	R23, R46, T23, T46;
    double		T1, T2, T3, T4;
    double		A1;
    double		A2;
    double		X1;
    double		X2;
    double		Z;
    int     		i, j;

    if (KS == 0) 
    {
        R23 = 1.0;
        R46 = 1.0;
        T23 = 1.0;
        T46 = 1.0;

        for (i=1; i<=23; i++)
        {
            R23 = 0.50 * R23;
            T23 = 2.0 * T23;
        }
        for (i=1; i<=46; i++)
        {
            R46 = 0.50 * R46;
            T46 = 2.0 * T46;
        }
        KS = 1;
    }

    /*  Break A into two parts such that A = 2^23 * A1 + A2 and set X = N.  */

    T1 = R23 * *A;
    j  = T1;
    A1 = j;
    A2 = *A - T23 * A1;

    /*  Break X into two parts such that X = 2^23 * X1 + X2, compute
        Z = A1 * X2 + A2 * X1  (mod 2^23), and then
        X = 2^23 * Z + A2 * X2  (mod 2^46).                            */

    T1 = R23 * *X;
    j  = T1;
    X1 = j;
    X2 = *X - T23 * X1;
    T1 = A1 * X2 + A2 * X1;

    j  = R23 * T1;
    T2 = j;
    Z = T1 - T23 * T2;
    T3 = T23 * Z + A2 * X2;
    j  = R46 * T3;
    T4 = j;
    *X = T3 - T46 * T4;
    return(R46 * *X);
} 



/*****************************************************************/
/************   F  I  N  D  _  M  Y  _  S  E  E  D    ************/
/************                                         ************/
/************ returns parallel random number seq seed ************/
/*****************************************************************/

/*
 * Create a random number sequence of total length nn residing
 * on np number of processors.  Each processor will therefore have a 
 * subsequence of length nn/np.  This routine returns that random 
 * number which is the first random number for the subsequence belonging
 * to processor rank kn, and which is used as seed for proc kn ran # gen.
 */

double   find_my_seed( int  kn,       /* my processor rank, 0<=kn<=num procs */
        int  np,       /* np = num procs                      */
        long nn,       /* total num of ran numbers, all procs */
        double s,      /* Ran num seed, for ex.: 314159265.00 */
        double a )     /* Ran num gen mult, try 1220703125.00 */
{

    long   i;

    double t1,t2,t3,an;
    long   mq,nq,kk,ik;



    nq = nn / np;

    for( mq=0; nq>1; mq++,nq/=2 )
        ;

    t1 = a;

    for( i=1; i<=mq; i++ )
        t2 = randlc( &t1, &t1 );

    an = t1;

    kk = kn;
    t1 = s;
    t2 = an;

    for( i=1; i<=100; i++ )
    {
        ik = kk / 2;
        if( 2 * ik !=  kk ) 
            t3 = randlc( &t1, &t2 );
        if( ik == 0 ) 
            break;
        t3 = randlc( &t2, &t2 );
        kk = ik;
    }

    return( t1 );

}




/*****************************************************************/
/*************      C  R  E  A  T  E  _  S  E  Q      ************/
/*****************************************************************/

void	create_seq(int my_rank, double seed, double a )
{
    double x;
    int    i, k;

    k = max_key/4;
    for (i=0; i<num_keys; i++)
    {
        x = randlc(&seed, &a);
        x += randlc(&seed, &a);
        x += randlc(&seed, &a);
        x += randlc(&seed, &a);  

        key_array[my_rank][i] = k*x;
    }
}




/*****************************************************************/
/*************    F  U  L  L  _  V  E  R  I  F  Y     ************/
/*****************************************************************/

INT_TYPE * last_local_keys;

void full_verify(int my_rank  )
{

    INT_TYPE    i, j;
    INT_TYPE    k, last_local_key;


    // TIMER_START( T_VERIFY );

    /*  Now, finally, sort the keys:  */
    for( i=0; i<total_local_keys[my_rank]; i++ )
        key_array[my_rank][--key_buff_ptr_global[my_rank][key_buff2[my_rank][i]]-
            total_lesser_keys[my_rank]] = key_buff2[my_rank][i];
    last_local_keys[my_rank] = (total_local_keys[my_rank]<1)? 0 : (total_local_keys[my_rank]-1);

    pthread_barrier_wait(&barrier); 

    //printf("last_local_key of thread %d is %d\n",my_rank,last_local_keys[my_rank]);

    /*  Send largest key value to next processor  */
    if(my_rank > 0) k = key_array[my_rank-1][last_local_keys[my_rank-1]];

    /*  Confirm that neighbor's greatest key value 
        is not greater than my least key value       */              
    j = 0;
    if( my_rank > 0 && total_local_keys[my_rank] > 0 )
        if( k > key_array[my_rank][0] )
            j++;


    /*  Confirm keys correctly sorted: count incorrectly sorted keys, if any */
    for( i=1; i<total_local_keys[my_rank]; i++ )
        if( key_array[my_rank][i-1] > key_array[my_rank][i] )
            j++;


    if( j != 0 )
    {
        printf( "Processor %d:  Full_verify: number of keys out of sort: %d\n",
                my_rank, j );
    }
    else
        passed_verification[my_rank]++;

    // TIMER_STOP( T_VERIFY );

}




/*****************************************************************/
/*************             R  A  N  K             ****************/
/*****************************************************************/


void rank( int iteration ,int my_rank )
{

    INT_TYPE    i, k;

    INT_TYPE    shift = max_key_log_2 - num_buckets_log_2;
    INT_TYPE    key;
    INT_TYPE2   bucket_sum_accumulator, j, m;
    INT_TYPE    local_bucket_sum_accumulator;
    INT_TYPE    min_key_val, max_key_val;
    INT_TYPE    *key_buff_ptr;

    INT_TYPE* bucket_ptrs = NULL;
    INT_TYPE* process_bucket_distrib_ptr1 = NULL;
    INT_TYPE* process_bucket_distrib_ptr2 = NULL;

    bucket_ptrs =(int*)malloc(sizeof(int)*num_buckets);
    process_bucket_distrib_ptr1 =(int*)malloc(sizeof(int)*(num_buckets+TEST_ARRAY_SIZE));
    process_bucket_distrib_ptr2 =(int*)malloc(sizeof(int)*(num_buckets+TEST_ARRAY_SIZE));

    /*  Iteration alteration of keys */  
    if(my_rank == 0 )                    
    {
        key_array[my_rank][iteration] = iteration;
        key_array[my_rank][iteration+MAX_ITERATIONS] = max_key - iteration;
    }


    /*  Initialize */
    if(my_rank==0) 
        for( i=0; i<num_buckets+TEST_ARRAY_SIZE; i++ )  
            bucket_size_totals[i]=0;

    for( i=0; i<num_buckets+TEST_ARRAY_SIZE; i++ )  
    {
        bucket_size[my_rank][i] = 0;
        process_bucket_distrib_ptr1[i] = 0;
        process_bucket_distrib_ptr2[i] = 0;
    }

    /*  Determine where the partial verify test keys are, load into  */
    /*  top of array bucket_size 
     */
    for( i=0; i<TEST_ARRAY_SIZE; i++ )
        if( (test_index_array[i]/num_keys) == my_rank ){
            //printf("=======test %d=========\n",i);
            //printf("test index is %ld\n",test_index_array[i]);
            //printf("num_keys is %ld\n",num_keys);
            //printf("corresponding thread is %d\n",test_index_array[i]/num_keys);

            bucket_size[my_rank][num_buckets+i] = 
                key_array[my_rank][test_index_array[i] % num_keys];
            //printf("index is %ld / %ld\n",test_index_array[i],num_keys);
            //printf("A test value is %ld inserted into %d\n",bucket_size[my_rank][num_buckets+i],my_rank);
        }

    /* INITIALIZATION DONE */

    //printf("I'm %d at iteration %d\n",my_rank,iteration);
    pthread_barrier_wait(&barrier); 

    /*  Determine the number of keys in each bucket */
    for( i=0; i<num_keys; i++ ){
        bucket_size[my_rank][key_array[my_rank][i] >> shift]++;
    }
    /*  Accumulative bucket sizes are the bucket pointers */
    bucket_ptrs[0] = 0;
    for( i=1; i< num_buckets; i++ ) 
        bucket_ptrs[i] = bucket_ptrs[i-1] + bucket_size[my_rank][i-1];

    /*  Sort into appropriate bucket */
    for( i=0; i<num_keys; i++ )  
    {
        key = key_array[my_rank][i];
        key_buff1[my_rank][bucket_ptrs[key >> shift]++] = key;
    }
    // TIMER_STOP( T_RANK );
    // TIMER_START( T_RCOMM );

    /*  Get the bucket size totals for the entire problem. These 
        will be used to determine the redistribution of keys      */

    pthread_barrier_wait(&barrier); //wait for each other till each tasks done.  

    if(my_rank==0){
        int jj=0;
        for( i=0; i< num_buckets+TEST_ARRAY_SIZE; i++)
        {
            for(jj=0;jj<num_of_threads;jj++)
                bucket_size_totals[i] += bucket_size[jj][i];
        } 
    }
    pthread_barrier_wait(&barrier);

    // TIMER_STOP( T_RCOMM );
    // TIMER_START( T_RANK );

    /*  Determine Redistibution of keys: accumulate the bucket size totals 
        till this number surpasses NUM_KEYS (which the average number of keys
        per processor).  Then all keys in these buckets go to processor 0.
        Continue accumulating again until supassing 2*NUM_KEYS. All keys
        in these buckets go to processor 1, etc.  This algorithm guarantees
        that all processors have work ranking; no processors are left idle.
        The optimum number of buckets, however, does not result in as high
        a degree of load balancing (as even a distribution of keys as is
        possible) as is obtained from increasing the number of buckets, but
        more buckets results in more computation per processor so that the
        optimum number of buckets turns out to be 1024 for machines tested.
        Note that process_bucket_distrib_ptr1 and ..._ptr2 hold the bucket
        number of first and last bucket which each processor will have after   
        the redistribution is done.                                          */

    bucket_sum_accumulator = 0;
    local_bucket_sum_accumulator = 0;
    send_displ[my_rank][0] = 0;
    process_bucket_distrib_ptr1[0] = 0;
    for( i=0, j=0; i<num_buckets; i++ )  
    {

        bucket_sum_accumulator       += bucket_size_totals[i];
        local_bucket_sum_accumulator += bucket_size[my_rank][i];
        if( bucket_sum_accumulator >= (j+1)*num_keys )  
        {
            send_count[my_rank][j] = local_bucket_sum_accumulator; 
            if( j != 0 )
            {
                send_displ[my_rank][j] = send_displ[my_rank][j-1] + send_count[my_rank][j-1];
                process_bucket_distrib_ptr1[j] = 
                    process_bucket_distrib_ptr2[j-1]+1;
            }


            process_bucket_distrib_ptr2[j++] = i;
            local_bucket_sum_accumulator = 0;
        }
    }

    /*  When NUM_PROCS approaching NUM_BUCKETS, it is highly possible
        that the last few processors don't get any buckets.  So, we
        need to set counts properly in this case to avoid any fallouts.    */
    while( j < num_of_threads )
    {
        send_count[my_rank][j] = 0;
        process_bucket_distrib_ptr1[j] = 1;
        j++;
    }
    pthread_barrier_wait(&barrier);

    /*  This is the redistribution section:  first find out how many keys
        each processor will send to every other processor:                 */

    for( i = 0 ; i <  num_of_threads ; i++)
        recv_count[my_rank][i] = send_count[i][my_rank];

    pthread_barrier_wait(&barrier);


    /*  Determine the receive array displacements for the buckets */    
    recv_displ[my_rank][0] = 0;
    for( i = 1; i < num_of_threads; i++ )
        recv_displ[my_rank][i] = recv_displ[my_rank][i-1] + recv_count[my_rank][i-1];


    pthread_barrier_wait(&barrier);



    /*  Now send the keys to respective processors  */    
    INT_TYPE * dst_ptr;
    INT_TYPE * src_ptr;
    for(i=0;i<num_of_threads;i++) {
        dst_ptr = key_buff2[my_rank] + recv_displ[my_rank][i];
        src_ptr = key_buff1[i] + send_displ[i][my_rank];
        memcpy( (void*)dst_ptr,(void*) src_ptr, sizeof(INT_TYPE)*recv_count[my_rank][i]);
    }
    pthread_barrier_wait(&barrier);



    //   TIMER_STOP( T_RCOMM ); 
    //   TIMER_START( T_RANK );

    /*  The starting and ending bucket numbers on each processor are
        multiplied by the interval size of the buckets to obtain the 
        smallest possible min and greatest possible max value of any 
        key on each processor                                          */
    min_key_val = process_bucket_distrib_ptr1[my_rank] << shift;
    max_key_val = ((process_bucket_distrib_ptr2[my_rank] + 1) << shift)-1;
    //printf("min_key_val of thread %d : %d\n",my_rank,min_key_val);
    //printf("max_key_val of thread %d : %d\n",my_rank,max_key_val);
    /*  Clear the work array */

    for( i=0; i<max_key_val-min_key_val+1; i++ )
        key_buff1[my_rank][i] = 0;


    /*  Determine the total number of keys on all other 
        processors holding keys of lesser value         */


    m = 0;
    for( k=0; k<my_rank; k++ )
        for( i= process_bucket_distrib_ptr1[k];
                i<=process_bucket_distrib_ptr2[k];
                i++ )  
            m += bucket_size_totals[i]; /*  m has total # of lesser keys */

    /*  Determine total number of keys on this processor */
    j = 0;                                 
    for( i= process_bucket_distrib_ptr1[my_rank];
            i<=process_bucket_distrib_ptr2[my_rank];
            i++ )  
        j += bucket_size_totals[i];     /* j has total # of local keys   */



    /*  Ranking of all keys occurs in this section:                 */
    /*  shift it backwards so no subtractions are necessary in loop */
    key_buff_ptr = key_buff1[my_rank] - min_key_val;

    /*  In this section, the keys themselves are used as their 
        own indexes to determine how many of each there are: their
        individual population                                       */
    for( i=0; i<j; i++ )
        key_buff_ptr[key_buff2[my_rank][i]]++;  /* Now they have individual key   */
    /* population                     */


    /*  To obtain ranks of each key, successively add the individual key
        population, not forgetting the total of lesser keys, m.
NOTE: Since the total of lesser keys would be subtracted later 
in verification, it is no longer added to the first key population 
here, but still needed during the partial verify test.  This is to 
ensure that 32-bit key_buff can still be used for class D.           */
    /*    key_buff_ptr[min_key_val] += m;    */
    for( i=min_key_val; i<max_key_val; i++ )   
        key_buff_ptr[i+1] += key_buff_ptr[i];  


    /* This is the partial verify test section */
    /* Observe that test_rank_array vals are   */
    /* shifted differently for different cases */
    for( i=0; i<TEST_ARRAY_SIZE; i++ )
    {                                             
        k = bucket_size_totals[i+num_buckets];    /* Keys were hidden here */
        //printf("Thread %d - TEST %d: range is <%ld,%ld> and test value is %d\n",my_rank,i,min_key_val, max_key_val,k);
        if( min_key_val <= k  &&  k <= max_key_val )
        {
            //printf("Thread %d - TEST %d: test value is %d\n",my_rank,i,k);
            /* Add the total of lesser keys, m, here */
            INT_TYPE2 key_rank = key_buff_ptr[k-1] + m;
            int failed = 0;

            switch(CLASS)
            {
                case 'S':
                    if( i <= 2 )
                    {
                        if( key_rank != test_rank_array[i]+iteration )
                            failed = 1;
                        else
                            passed_verification[my_rank]++;
                    }
                    else
                    {
                        if( key_rank != test_rank_array[i]-iteration )
                            failed = 1;
                        else
                            passed_verification[my_rank]++;
                    }
                    break;
                case 'W':
                    if( i < 2 )
                    {
                        if( key_rank != test_rank_array[i]+(iteration-2) ){
                            failed = 1;
                            //printf("Thread %d: failure detected, i=%d,k=%d\n",my_rank,i,k);
                        }
                        else{
                            //printf("Thread %d: verification successfully done\n",my_rank);
                            passed_verification[my_rank]++;
                        }
                    }
                    else
                    {
                        if( key_rank != test_rank_array[i]-iteration ){
                            failed = 1;
                            //printf("Thread %d: failure detected, i=%d,k=%d\n",my_rank,i,k);
                        }
                        else{
                            //printf("Thread %d: verification successfully done\n",my_rank);
                            passed_verification[my_rank]++;
                        }
                    }
                    break;
                case 'A':
                    if( i <= 2 )
                    {
                        if( key_rank != test_rank_array[i]+(iteration-1) )
                            failed = 1;
                        else
                            passed_verification[my_rank]++;
                    }
                    else
                    {
                        if( key_rank != test_rank_array[i]-(iteration-1) )
                            failed = 1;
                        else
                            passed_verification[my_rank]++;
                    }
                    break;
                case 'B':
                    if( i == 1 || i == 2 || i == 4 )
                    {
                        if( key_rank != test_rank_array[i]+iteration )
                            failed = 1;
                        else
                            passed_verification[my_rank]++;
                    }
                    else
                    {
                        if( key_rank != test_rank_array[i]-iteration )
                            failed = 1;
                        else
                            passed_verification[my_rank]++;
                    }
                    break;
                case 'C':
                    if( i <= 2 )
                    {
                        if( key_rank != test_rank_array[i]+iteration )
                            failed = 1;
                        else
                            passed_verification[my_rank]++;
                    }
                    else
                    {
                        if( key_rank != test_rank_array[i]-iteration )
                            failed = 1;
                        else
                            passed_verification[my_rank]++;
                    }
                    break;
                case 'D':
                    if( i < 2 )
                    {
                        if( key_rank != test_rank_array[i]+iteration )
                            failed = 1;
                        else
                            passed_verification[my_rank]++;
                    }
                    else
                    {
                        if( key_rank != test_rank_array[i]-iteration )
                            failed = 1;
                        else
                            passed_verification[my_rank]++;
                    }
                    break;
            }
            if( failed == 1 )
                printf( "Failed partial verification: "
                        "iteration %d, processor %d, test key %d\n", 
                        iteration, my_rank, (int)i );
        }
    }
    pthread_barrier_wait(&barrier); 


    //    TIMER_STOP( T_RANK ); 


    /*  Make copies of rank info for use by full_verify: these variables
        in rank are local; making them global slows down the code, probably
        since they cannot be made register by compiler                        */

    if( iteration == MAX_ITERATIONS ) 
    {
        key_buff_ptr_global[my_rank] = key_buff_ptr;
        total_local_keys[my_rank]    = j;
        total_lesser_keys[my_rank]   = 0;  /* no longer set to 'm', see note above */
    }

    free(bucket_ptrs);
    free(process_bucket_distrib_ptr1);
    free(process_bucket_distrib_ptr2);

}      



void *thread_routine(void *arg)
{


    int i, iteration, itemp;
    double  timecounter, maxtime;
    int my_rank = (int)arg;

    /*  Initialize the verification arrays if a valid class */
    for( i=0; i<TEST_ARRAY_SIZE; i++ )
        switch(CLASS)//switch( CLASS )
        {
            case 'S':
                test_index_array[i] = S_test_index_array[i];
                test_rank_array[i]  = S_test_rank_array[i];
                break;
            case 'A':
                test_index_array[i] = A_test_index_array[i];
                test_rank_array[i]  = A_test_rank_array[i];
                break;
            case 'W':
                test_index_array[i] = W_test_index_array[i];
                test_rank_array[i]  = W_test_rank_array[i];
                break;
            case 'B':
                test_index_array[i] = B_test_index_array[i];
                test_rank_array[i]  = B_test_rank_array[i];
                break;
            case 'C':
                test_index_array[i] = C_test_index_array[i];
                test_rank_array[i]  = C_test_rank_array[i];
                break;
            case 'D':
                test_index_array[i] = D_test_index_array[i];
                test_rank_array[i]  = D_test_rank_array[i];
                break;
        };

    /*  Printout initial NPB info */
    if( my_rank == 0 )
    {
        //	printf( "\n\n NAS Parallel Benchmarks 3.3 -- IS Benchmark\n\n" );
        //	printf( " Size:  %ld  (class %c)\n", (long)total_keys*min_procs, CLASS );
        //	printf( " Iterations:   %d\n", MAX_ITERATIONS );
        //	printf( " Number of threads:     %d\n", num_of_threads );


    }

    /*  Generate random number sequence and subsequent keys on all procs */
    create_seq( my_rank,find_my_seed( my_rank, 
                num_of_threads, 
                4*(long)total_keys*min_procs,
                314159265.00,      /* Random number gen seed */
                1220703125.00 ),   /* Random number gen mult */
            1220703125.00 );                 /* Random number gen mult */


    /*  Do one interation for free (i.e., untimed) to guarantee initialization of  
        all data and code pages and respective tables */
    rank( 1, my_rank );

    //printf("Thread %d: check partial verify\n",my_rank);
    /*  Start verification counter */
    passed_verification[my_rank] = 0;

    //   if( my_rank == 0 && CLASS != 'S' ) printf( "\n   iteration\n" );


    /*  This is the main iteration */

    for( iteration=1; iteration<=MAX_ITERATIONS; iteration++ )
    {
        //	if( my_rank == 0 && CLASS != 'S' ) printf( "%d\t", iteration );
        rank( iteration,my_rank );
        //printf("Thread %d: returned from rank iteration %d,\n",my_rank, iteration);
        //getchar();

    }
    //    printf("\n");


    /*  This tests that keys are in sequence: sorting of last ranked key seq
        occurs here, but is an untimed operation                             */
    full_verify(my_rank);
    //printf("Thread %d: full verification completed\n",my_rank);
    pthread_barrier_wait(&barrier); 

    if(my_rank==0){
        int total_verification_count=0;
        //printf("********passed_verification***********\n");
        for(i=0;i<num_of_threads;i++){
            //printf("Thread %d: %d\n",i,passed_verification[i]);
            total_verification_count+=passed_verification[i];
        }
        //printf("TOTAL verification count is %d\n",total_verification_count);
        //printf("Expected verification count is %d\n",5*MAX_ITERATIONS + num_of_threads);
    }

    /*  Obtain verification counter sum */

    pthread_exit(NULL);


}

void set_problem_size(char s)
{
    CLASS = s;
    if(s=='S') {
        total_keys_log_2 =  16;
        max_key_log_2 =    11;
        num_buckets_log_2 = 9;
    } else if(s=='W') {
        total_keys_log_2 =  20;
        max_key_log_2 =    16;
        num_buckets_log_2 = 10;
    } else if(s=='A') {
        total_keys_log_2 =  23;
        max_key_log_2 =    19;
        num_buckets_log_2 = 10;
    } else if(s=='B') {
        total_keys_log_2 =  25;
        max_key_log_2 =    21;
        num_buckets_log_2 = 10;
    } else if(s=='C') {
        total_keys_log_2 =  27;
        max_key_log_2 =    23;
        num_buckets_log_2 = 10;
    } else if(s=='D') {
        total_keys_log_2 =  29;
        max_key_log_2 =    27;
        num_buckets_log_2 = 10;
    } else {
        total_keys_log_2 =  16;
        max_key_log_2 =    11;
        num_buckets_log_2 = 9;
    }
    total_keys = (1<<total_keys_log_2);
    max_key = (1<<max_key_log_2); 
    num_buckets = (1<<num_buckets_log_2);
    num_keys = total_keys / num_of_threads;

    /* min procs */
    if(s=='D') min_procs = 4;
    else min_procs = 1;
    /* max procs */
    if(s=='S') max_procs = 128;
    else max_procs = 1024;

    /* size of buffers */
    if(num_of_threads < 256) {
        size_of_buffers = 3*num_keys/2;
    } else if (num_of_threads < 512) {
        size_of_buffers = 5*num_keys/2;
    } else if (num_of_threads < 1024) {
        size_of_buffers = 4*num_keys;
    } else {
        size_of_buffers = 13*num_keys;
    }
}



void is_main(char cl)
{
    int i,j;
    int status;

    /* parameter setting */
    //num_of_threads = 1;
    set_problem_size(cl);

    //printf("TOTAL_KEYS %ld, num_keys %ld\n",TOTAL_KEYS,num_keys);
    //getchar();

    // initialize bucket_size_totals
    bucket_size_totals = (INT_TYPE*) malloc(sizeof(INT_TYPE)*(num_buckets+TEST_ARRAY_SIZE));
    for( i=0; i<num_buckets+TEST_ARRAY_SIZE; i++ )  
        bucket_size_totals[i] = 0;

    // initialization key_buffs
    key_array = (INT_TYPE **) malloc(sizeof(INT_TYPE*)*num_of_threads);
    key_buff1 = (INT_TYPE **) malloc(sizeof(INT_TYPE*)*num_of_threads);
    key_buff2 = (INT_TYPE **) malloc(sizeof(INT_TYPE*)*num_of_threads);
    bucket_size = (INT_TYPE **) malloc(sizeof(INT_TYPE*)*num_of_threads);
    for( i=0;i<num_of_threads;i++){
        key_array[i] = (INT_TYPE*) malloc(sizeof(INT_TYPE)*size_of_buffers);
        key_buff1[i] = (INT_TYPE*) malloc(sizeof(INT_TYPE)*size_of_buffers);
        key_buff2[i] = (INT_TYPE*) malloc(sizeof(INT_TYPE)*size_of_buffers);
        bucket_size[i] = (INT_TYPE*) malloc(sizeof(INT_TYPE)*(num_buckets+TEST_ARRAY_SIZE));
    }
    // initialization send/recv_count
    send_count = (int **) malloc(sizeof(int*)*num_of_threads);
    recv_count = (int **) malloc(sizeof(int*)*num_of_threads);
    send_displ = (int **) malloc(sizeof(int*)*num_of_threads);
    recv_displ = (int **) malloc(sizeof(int*)*num_of_threads);
    for( i=0;i<num_of_threads;i++){
        send_count[i] = (int *) malloc(sizeof(int)*num_of_threads);
        recv_count[i] = (int *) malloc(sizeof(int)*num_of_threads);
        send_displ[i] = (int *) malloc(sizeof(int)*num_of_threads);
        recv_displ[i] = (int *) malloc(sizeof(int)*num_of_threads);
        for( j=0;j<num_of_threads;j++) {
            send_count[i][j]=0;
            recv_count[i][j]=0;
            send_displ[i][j]=0;
            recv_displ[i][j]=0;
        }
    }
    // initialization the values used in full_verify
    key_buff_ptr_global = (INT_TYPE**) malloc(sizeof(INT_TYPE*)*num_of_threads);
    total_local_keys = (INT_TYPE*) malloc(sizeof(INT_TYPE)*num_of_threads);
    total_lesser_keys = (INT_TYPE*) malloc(sizeof(INT_TYPE)*num_of_threads);
    last_local_keys = (INT_TYPE*) malloc(sizeof(INT_TYPE)*num_of_threads);
    passed_verification = (int*) malloc(sizeof(int)*num_of_threads);
    for(i=0;i<num_of_threads;i++){
        key_buff_ptr_global[i]=NULL;
        total_local_keys[i]=0;
        total_lesser_keys[i]=0;
        last_local_keys[i]=0;
        passed_verification[i]=0;
    }

    /*pthread init*/
    pthread_t threads[num_of_threads];

    pthread_barrier_init(&barrier,NULL,num_of_threads);	
    for( i = 0; i < num_of_threads; i ++ )
        pthread_create(&threads[i],NULL, thread_routine, (void*)i);

    for( i = 0; i < num_of_threads; i ++ )
        pthread_join(threads[i],(void**)&status);


    /* free memory */
    for( i=0;i<num_of_threads;i++){
        free(send_count[i]);
        free(recv_count[i]);
        free(send_displ[i]);
        free(recv_displ[i]);
        free(key_array[i]);
        free(key_buff1[i]);
        free(key_buff2[i]);
        free(bucket_size[i]);
    }
    free(send_count);
    free(recv_count);
    free(send_displ);
    free(recv_displ);
    free(bucket_size_totals);
    free(key_array);
    free(key_buff1);
    free(key_buff2);
    free(bucket_size);
    free(key_buff_ptr_global);
    free(total_local_keys);
    free(total_lesser_keys);
    free(last_local_keys);
    free(passed_verification);

    return ;


}


void func1() { num_of_threads=1; printf("func1 is working!\n"); }
void func2() { num_of_threads=2; printf("func2 is working!\n"); }
void func3() { num_of_threads=4; printf("func3 is working!\n"); }
void func4() { num_of_threads=8; printf("func4 is working!\n"); }

int main(int argc, char *argv[])
{

    int i;


    double curr_hb;
    FILE * graph_out;

    int ret,rc;

    num_of_threads = 8;

    graph_out  = fopen("hb_out_sa.txt","wt");

    printf("/*** Heartbeat init ***/\n");

    rc = heartbeat_init(&hb, 0, 0, 0, 0, NULL);
    if( rc <=  0)
        printf("Error: mQ_ID is abnormally allocated :%d\n",rc);
//    heartbeat( &hb, -1 );

    
    void (*func_ptr[4])() = { func1, func2, func3,func4 };


    //	fprintf(graph_out,"min_rate, instant_rate, max_rate\n" );



    
  gettimeofday(&start,NULL);
int lev;
    for(i=0;i<25;i++) {

  gettimeofday(&tstart,NULL);
        /* heartbeat tagging */
        gettimeofday( &start_1, NULL);
        heartbeat(&hb,i);
        gettimeofday( &end_1, NULL);


        heartbeat_record_t temp_hb_rec;
        hb_get_current( &hb, &temp_hb_rec);
        curr_hb = temp_hb_rec.instant_rate;




        printf("-----------------------\n");
        printf("*     %d th Routine   *\n",i);
        printf("-----------------------\n");


        printf("=======================\n");
        printf("instant rate %f\n",curr_hb);
        printf("=======================\n");
        //	fprintf(graph_out,"%d %f %f %f\n",i,min_rate,curr_rec.instant_rate,max_rate);
 //       fprintf(graph_out," %lf, %f, %lf\n", hb.state->min_heartrate, curr_hb,hb.state->max_heartrate);
 //       fflush(graph_out);


        //	ret = mq_receive(&hb);
        /*
           if(i > 2){

           if((int)ret==1){ printf("%s Thread 1 will be Working Next time !\n",KWHT); target_func = &func1;}
           else if((int)ret==2){printf("%s Thread 2 will be Working Next time !\n",KWHT); target_func = &func2;}
           else if((int)ret==4){printf("%s Thread 4 will be Working Next time !\n",KWHT); target_func = &func3;}
           else ;
           (*target_func)();
           }
         */	

        if( i > 0 ) {
        gettimeofday( &start_2, NULL);
           lev =  hb_decision( &hb );	
           lev = 4;
           (*func_ptr[lev-1])();
        gettimeofday( &end_2, NULL);
        }


exec_time_a = (end_1.tv_usec - start_1.tv_usec)*1e-6 + (end_1.tv_sec - start_1.tv_sec);
exec_time_b = (end_2.tv_usec - start_2.tv_usec)*1e-6 + (end_2.tv_sec - start_2.tv_sec);
/*
        if(i<7){
            printf("%s \n====  Medium SIZE WorkLoad ====\n",KGRN);
            is_main('W');
        }else if(i<15) {
            printf("%s \n==== Light SIZE WorkLoad ====\n",KYEL);
            is_main('S');
        } else if(i<23) {
            printf("%s \n====  Medium SIZE WorkLoad ====\n",KGRN);
            is_main('W');
        } else if(i<31){  
            printf("%s \n====  Heavy SIZE WorkLoad ====\n",KRED);
            is_main('S');
        } else if(i<39){
            printf("%s \n====  Medium SIZE WorkLoad ====\n",KGRN);
            is_main('W');
        } else if(i<47){
            printf("%s \n==== Light SIZE WorkLoad ====\n",KYEL);
            is_main('S');
        } else {
            printf("%s \n====  Medium SIZE WorkLoad ====\n",KGRN);
            is_main('W');
        }
*/
        
        if(i<4){
            printf("%s \n====  Light SIZE WorkLoad ====\n",KYEL);
            is_main('W');
        }else if(i<7) {
            printf("%s \n==== Medium SIZE WorkLoad ====\n",KRED);
            is_main('A');
        } else if(i<10) {
            printf("%s \n====  Medium SIZE WorkLoad ====\n",KRED);
            is_main('A');
        } else if(i<14){  
            printf("%s \n====  Light SIZE WorkLoad ====\n",KYEL);
            is_main('W');
        } else if(i<18){
            printf("%s \n====  Medium SIZE WorkLoad ====\n",KRED);
            is_main('A');
        } else if(i<22){
            printf("%s \n==== Medium SIZE WorkLoad ====\n",KRED);
            is_main('A');
        } else {
            printf("%s \n====  Medium SIZE WorkLoad ====\n",KRED);
            is_main('A');
        }

  gettimeofday(&tend,NULL);
exec_time = (tend.tv_usec - tstart.tv_usec)*1e-6 + (tend.tv_sec - tstart.tv_sec);

        fprintf(graph_out," %lf, %f, %lf, %10f, %10f, %10f\n", hb.state->min_heartrate, curr_hb,hb.state->max_heartrate, exec_time_a, exec_time_b, exec_time);
        fflush(graph_out);


    }

  gettimeofday(&end,NULL);
exec_time = (end.tv_usec - start.tv_usec)*1e-6 + (end.tv_sec - start.tv_sec);
  printf("Elapsed_time : %.3f sec\n",exec_time);

    /* heartbeat wrapup */
    fclose(graph_out);
    heartbeat_finish(&hb);

    return 0;

}

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <ctime>
#include <ctype.h>
#include <math.h>
#include <sys/time.h>
#include <unistd.h>
#include <omp.h>
#define ROUNDING(x, dig) (floor((x)*pow(float(10), dig) + 0.5f) /pow(float(10),dig))
using namespace cv;
using namespace std;

struct Particle
{
	CvRect rectangle;
	float weight, fitness;
};

struct Face
{
	CvRect face_rect;
	Mat r_Histo, g_Histo, b_Histo;
};

vector<Face> face_pos;
vector<Particle> particles;

int findIndex(double *CDF, int lengthCDF, double value);
Mat ParticleFilter(Mat, unsigned int, int);

bool do_draw = false, finish = false;
void onMouse(int, int, int, int, void*);
double normal_random(double mean, double sigma);
long long int get_time();
double elapsed_time(long long int, long long int);
void help();
Rect box;
#define RED Scalar(0,0,255)

double particle_init = 0;
double calc_likeli_weight = 0;
double sum_weight = 0;
double normalize_weight = 0;
double calc_CDF = 0;
double calc_randomnum = 0;
double resampling = 0;
double new_rect = 0;
double calc_opencv = 0;
double print_particle = 0;
int particle_sum = 0;

unsigned int particle_num = 1024;
unsigned int CORE_NUM = 1;
int target[4] = {-1, -1, -1, -1};
string video_name_or_directory;
bool is_video = 0;
bool is_mouse = 0;
bool is_show = 0;
bool is_file = 0;

int main(int argc, char* argv[])
{
  string opt_test;
  int opt;
  while((opt = getopt(argc, argv, "hmav:c:p: ")) != -1)
  {
    switch(opt)
    {
      case 'h':
        help();
        exit(0);
      case 'v':
        is_video = 1;
        opt_test = optarg;
        for(string::iterator it = opt_test.begin(); it != opt_test.end(); ++it){
          if(!isdigit(*it)){
            is_file = 1;
            break;
          }
        }
        if(is_file)video_name_or_directory = opt_test;
        else if(!is_file){
          cout<<"Please enter file name"<<endl;
          exit(0);
        }
        break;
      case 'm':
        is_mouse = 1;
        break;
      case 'c':
        CORE_NUM = atoi(optarg);
        break;
      case 'p':
        particle_num = atoi(optarg);
        break;
      case 'a':
        is_show = 1;
        break;

      default:
        break;
    }
  }
  
  int target_id = 0;
  if(is_video == 0 || is_mouse == 0){
    if(is_video == 0){
      opt_test = argv[optind];
      for(string::iterator it = opt_test.begin(); it != opt_test.end(); ++it){
        if(!isdigit(*it)){
          is_file = 1;
          break;
        }
      }
      if(is_file)video_name_or_directory = argv[optind++];
      else if(!is_file){
        cout<<"Please enter directory"<<endl;
        exit(0);
      }
    }
    if(!is_mouse) {
      while(optind < argc && isdigit(argv[optind][0])){
        target[target_id++] = atoi(argv[optind++]);
      }
      for(int i = 0; i < 4; i++){
        if(target[i] == -1){
          cout<<"Please check ground truth input!"<<endl;
          exit(0);
        }
      }
    }
  }
  
  if((is_mouse == 1) && ( is_show == 0)){
    cout<<"ERROR: if you want to use mouse, please use option -a to see window"<<endl;
    exit(0);
  }

  if(is_show) namedWindow("object_tracking");
  Mat frame;
	int frame_num = 0;
	bool first_face = false;
	double click_end = 0;
  double obj_end = 0;
  VideoCapture video;

  long long opencv_start;
  long long init_start = get_time();
  unsigned int file_start_num = 1;
  ifstream file_check;
  while(1) {
    if(is_video){ //video read
      file_check.open(video_name_or_directory, ios::in);
      if(!file_check.is_open()){
        cout<<"ERROR: File doesn't exist"<<endl;
        exit(0);
      }

		  opencv_start = get_time();
      
      if(frame_num == 0){
        video=VideoCapture(video_name_or_directory);
        assert(video.isOpened());
      }
      
      video.read(frame);
      calc_opencv += elapsed_time(opencv_start, get_time());
		  
      if(frame.empty()) {
        frame.release();
        break;
      }
    }

    if(!is_video){//image sequence read    
      ostringstream file_num;
      file_num<<setw(4)<<setfill('0')<<file_start_num;
      string file_number = file_num.str();
      string file_name = video_name_or_directory+file_number+".jpg";
      
      file_check.open(file_name, ios::in);
      if(!file_check.is_open()){
        cout<<"ERROR: File doesn't exist"<<endl;
        exit(0);
      }
      
      frame = imread(file_name);
      
      if(frame.empty()) {
        frame.release();
        break;
      }
    }
    
    if(is_mouse){ //Mouse Click 
      long long click_start = get_time();
      if(is_video) {
        setMouseCallback("object_tracking", onMouse, (void*) &frame);
        while(finish==false){
          Mat tmp = frame.clone();
          if(do_draw) rectangle(tmp, box, RED, 2);
          imshow("object_tracking", tmp);
          tmp.release();
          if(waitKey(20)==27) break;
        }
      }
      click_end += elapsed_time(click_start, get_time());
    }

    if(!is_mouse){
      box.x = target[0]; box.y = target[1]; box.width = target[2] ; box.height = target[3];
    }

    ////////Calc Histogram of object which is selected///////
    long long calc_obj_start = 0;
    
    if(first_face==false){
      calc_obj_start = get_time();
      
      float range[] = {0,256};
	    const float *ranges = {range};
	    int histSize = 256;
      
      vector<Mat> channel;
      Mat roi_face = frame(box).clone();
      
      split(roi_face, channel);
      Face cur_face;
		  cur_face.face_rect = box;
		  calcHist(&channel[0], 1, 0, Mat(), cur_face.b_Histo, 1, &histSize, &ranges);
		  calcHist(&channel[1], 1, 0, Mat(), cur_face.g_Histo, 1, &histSize, &ranges);
		  calcHist(&channel[2], 1, 0, Mat(), cur_face.r_Histo, 1, &histSize, &ranges);
		  face_pos.push_back(cur_face);
      first_face = true;
      roi_face.release();
    }
    obj_end += elapsed_time(calc_obj_start, get_time());
    
    //////Particle Filter//////////////////////
    frame = ParticleFilter(frame, particle_num, frame_num);
    frame_num++;
    if(!is_video)file_start_num++;
    ////////Calc FPS(Frame per Sec) and PPS(Particle per Sec)//////
    particle_sum += particle_num;

    double fps_time = calc_likeli_weight + particle_init + sum_weight + calc_opencv + normalize_weight + calc_CDF + calc_randomnum + resampling + new_rect + print_particle;
    float fps = (frame_num*1.0)/fps_time;
    fps = ROUNDING(fps,2);
    int pps = (int)particle_sum/frame_num;
  
    stringstream stream;
    stream <<fixed<<setprecision(2)<<fps;
    string fps_s = stream.str();
    string info = fps_s + " F/S , " + to_string(pps) +" P/F";
    putText(frame, info, cvPoint(10, (frame.size().height*5/6)), 2, 0.8, Scalar(255, 255, 255));
		

    if(is_show){////////show result image///////////////////
      opencv_start = get_time();
      imshow("object_tracking",frame);
		  waitKey(33);
      calc_opencv += elapsed_time(opencv_start, get_time());
    }
	}
  long long init_end = elapsed_time(init_start, get_time());
  cout<<"total time : "<<init_end-click_end<<endl;
  cout<<"click time : "<<click_end<<endl;
  cout<<"obj calc time : "<<obj_end<<endl;
  cout<<"particle init time : "<<particle_init<<endl;
  cout<<"calc likelihood & weight time : "<<calc_likeli_weight<<endl;
  cout<<"sum_weight time : "<<sum_weight<<endl;
  cout<<"noramlize time : "<<normalize_weight<<endl;
  cout<<"calc_CDF time : "<<calc_CDF<<endl;
  cout<<"calc_randomnum time : "<<calc_randomnum<<endl;
  cout<<"resampling time : "<<resampling<<endl;
  cout<<"new position time : "<<new_rect<<endl;
  cout<<"print particle time : "<<print_particle<<endl;

  frame.release();
	return 0;
}


Mat ParticleFilter(Mat img, unsigned int Part_num, int frame_no)
{	

  omp_set_num_threads(CORE_NUM);
	srand(time(NULL)*rand()%100000);
	unsigned int i;
	
  if(frame_no == 0)//ùframe
  {
    particles.resize(Part_num);
    Particle *ptr_particle = &particles[0];
    long long particle_init_start = get_time();
    #pragma parallel for shared(img.size()) private(i)
    for(i = 0; i < Part_num; i++)
		{
			Particle new_Part;
			float rand_x, rand_y, rand_w, rand_h;
      rand_w = box.width; rand_h = box.height;
      do{
        rand_x = normal_random(img.size().width/2, 10);
        rand_y = normal_random(img.size().height/2, 10);
      } while((rand_x<=0||rand_x>=img.size().width-rand_w)||(rand_y<=0||rand_y>=img.size().height-rand_h));
      new_Part.rectangle = cvRect(rand_x, rand_y, rand_w, rand_h);
      ptr_particle[i] = new_Part;
		}
    particle_init += elapsed_time(particle_init_start, get_time());
  }
	

  //likelihood
	long long calc_likeli_start = get_time();
  
  #pragma omp parallel for shared(particles, face_pos) private(i)
	for(i = 0; i < particles.size(); i++)
	{
    float range[] = {0,256};
    const float *ranges = {range};
    int histSize = 256;
		float likelihood, rCorr, gCorr, bCorr;
		Mat roi_img = img(particles[i].rectangle).clone();
    vector<Mat> channels;
		split(roi_img,channels);
		Mat rHisto, gHisto, bHisto;
		calcHist(&channels[0], 1, 0, Mat(), bHisto, 1, &histSize, &ranges);
		calcHist(&channels[1], 1, 0, Mat(), gHisto, 1, &histSize, &ranges);
		calcHist(&channels[2], 1, 0, Mat(), rHisto, 1, &histSize, &ranges);
    rCorr = compareHist(rHisto, face_pos[0].r_Histo, CV_COMP_CORREL);
		gCorr = compareHist(gHisto, face_pos[0].g_Histo, CV_COMP_CORREL);
		bCorr = compareHist(bHisto, face_pos[0].b_Histo, CV_COMP_CORREL);
    likelihood = (rCorr*0.4 + gCorr*0.3 + bCorr*0.3);
		particles[i].weight = pow(2.718281828, -16.0 * (1-likelihood));
    roi_img.release();
  }
  calc_likeli_weight += elapsed_time(calc_likeli_start, get_time());
	

  //sum_weight
  long long sum_start = get_time();
  double sumWeight = 0;
  #pragma omp parallel for private(i) reduction(+:sumWeight)
	for(i = 0; i < particles.size(); i++)
	{
		sumWeight += particles[i].weight;
	}
  sum_weight += elapsed_time(sum_start, get_time());


  //normalize weight
  long long normalize_start = get_time();
  vector<Particle> new_part;
  #pragma omp parallel for shared(sumWeight, particles) private(i)
  for(i = 0; i <particles.size(); i++)
  {
    particles[i].fitness = particles[i].weight/sumWeight;
  }
  normalize_weight += elapsed_time(normalize_start, get_time());
  
  Particle best_part;

  //calc_CDF
  long long calc_CDF_start = get_time();
  double CDF[particles.size()];

  for(i = 0; i < particles.size(); i++)
  {
    if(i==0) {
      CDF[i] = particles[i].fitness;
      best_part = particles[i];
    }
    else if(best_part.fitness < particles[i].fitness) best_part = particles[i];
    else if(i!=0) CDF[i] = particles[i].fitness + CDF[i-1];
  }
  calc_CDF += elapsed_time(calc_CDF_start, get_time());
  
  //resampling
  long long resampling_start = get_time();
  new_part.resize(particles.size());
  #pragma omp parallel for shared(CDF, particles) private(i)
  for(i = 0; i < particles.size(); i++)
  {
    double randnum = (double)rand()/RAND_MAX;
    int index = findIndex(CDF, particles.size(), randnum);
    new_part[i] = particles.at(index);
    
    int rand_w, rand_h, new_x, new_y;
    rand_w = new_part[i].rectangle.width;
    rand_h = new_part[i].rectangle.height;
    
    do{
      new_x = normal_random(new_part[i].rectangle.x, 10);
      new_y = normal_random(new_part[i].rectangle.y, 10);
    }
    
    while((new_x<0||new_x>=img.size().width-rand_w)||(new_y<0||new_y>=img.size().height-rand_h));
			
    new_part[i].rectangle = cvRect(new_x, new_y, rand_w, rand_h);
 

  }
  resampling += elapsed_time(resampling_start, get_time());


  particles.clear();
  particles.resize(new_part.size());
  copy(new_part.begin(), new_part.end(), particles.begin());
  new_part.clear();
  
  //print
  long long print_start = get_time();
  #pragma omp parallel for shared(particles) private(i)
	for (i = 0; i < particles.size(); i++)
	{
		Point center(particles[i].rectangle.x+particles[i].rectangle.width/2, particles[i].rectangle.y+particles[i].rectangle.height/2);
		circle(img, center, 2, Scalar(0, 255, 0), 2);
  }
  print_particle += elapsed_time(print_start, get_time());

  Point best1, best2;
  best2.x = best_part.rectangle.x;
  best1.x = best_part.rectangle.x + best_part.rectangle.width;
  best1.y = best_part.rectangle.y;
  best2.y = best_part.rectangle.y + best_part.rectangle.height;

  rectangle(img, best1, best2, Scalar(255,0,0), 1, 8, 0);

	return img;
}

int findIndex(double *CDF, int lengthCDF, double value)
{
  int index = -1;
  int x;
  for(x = 0; x < lengthCDF; x++){
    if(CDF[x] >= value){
      index = x;
      break;
    }
  }
  if(index == -1){
    return lengthCDF-1;
  }
  return index;
}

void onMouse(int event, int x, int y, int, void*)
{
  if(event == CV_EVENT_MOUSEMOVE && !finish){
    box.width = x - box.x;
    box.height = y - box.y;
  }
  else if(event==CV_EVENT_LBUTTONDOWN && !finish){
    do_draw = true;
    box = Rect(x,y,0,0);
  }
  else if(event == CV_EVENT_LBUTTONUP && !finish){
    do_draw= false;
    if(box.width<0) {box.x += box.width; box.width*=-1;}
    if(box.height<0) {box.y += box.height; box.height*=-1;}
    finish = true;
  }
}

double normal_random(double mean, double sigma)
{
  static double V1, V2, S;
  static int phase = 0;
  double X;

  if(phase == 0){
    do
    {
      double U1 = (double)rand()/RAND_MAX;
      double U2 = (double)rand()/RAND_MAX;
      V1 = 2*U1 -1;
      V2 = 2*U2 -1;
      S = V1 * V1 + V2 * V2;
    }while(S >= 1 || S == 0);
    X = V1 * sqrt(-2 * log(S)/S);
  }
  else
    X = V2 * sqrt(-2 * log(S)/S);
  phase = 1-phase;
  return mean + sigma * X;

}
long long int get_time(){
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return (tv.tv_sec *1000000)+tv.tv_usec;
}

double elapsed_time(long long int start_time, long long int end_time){
  return (double)(end_time - start_time)/(1000*1000);
}

void help(){
  cout<<endl;
  cout<<"Usage: openmp_particle [options] <image sequence directory> <x y width height(ground truth)>"<<endl;
  cout<<"Default: Use image sequence and ground truth,  #processors = 1, #particles = 1024, don't see window"<<endl;
  cout<<endl;
  cout<<"<directory>    Use this when you don't use -v option"<<endl;
  cout<<"<x y width height>     Use this when you don't use -m option"<<endl;
  cout<<endl;
  cout<<"Options:"<<endl; 
  cout<<"  -v <file>            Use Video file"<<endl;
  cout<<"  -m                   Use mouse to select tracking object(when use this option, you must set -a option with -m option.)"<<endl;
  cout<<"  -c <#processors>      Use #processor when execute this program"<<endl;
  cout<<"  -p <#particles>      Use #particles when execute this program"<<endl;
  cout<<"  -a                   You can see window when execute this program"<<endl;
  cout<<"Examples:"<<endl;
  cout<<"  ./openmp_particle data/Basketball/img/ 198 214 34 81 "<<endl;
  cout<<"     : Use image sequence(data/Basketball/img/) and ground truth(198, 214, 34, 81), #processors = 1, #particles = 1024, don't see window"<<endl;
  cout<<"  ./openmp_particle -v data/A_ball.avi -m -c 4 -p 512 -a"<<endl;
  cout<<"     : Use video file(data/A_ball.avi) and mouse, #processors = 4, #particles = 512, see window"<<endl;
}  

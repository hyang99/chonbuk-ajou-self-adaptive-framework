#ifndef _HEARTBEAT_H_
#define _HEARTBEAT_H_

#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <unistd.h>
#ifdef MOCO
#include <stdint.h>
#endif
#include <time.h>
#include <pthread.h>

#include <sys/un.h>
#include <sys/socket.h>

#define INIT_BUFSIZE 15
#define COMM_BUFSIZE 300
#define SOCK_PATH "/tmp/us_xfr"

//command head
#define INIT "INIT" 
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#define CONNECT_IP  "127.0.0.1" //local
#define CONNECT_PORT    10000   // Hooo.

typedef struct {
	long mtype;
	char mtext[COMM_BUFSIZE];
} msgbuf;

#ifdef MOCO //FIXME
typedef struct { 
	double arg_1;	
	double arg_2;	
	double arg_3;
	double arg_4;
} add_data_t;

#endif

typedef struct {
	int64_t beat;
	int tag;
	int64_t timestamp;
	double global_rate;
	double window_rate;
	double instant_rate;
} heartbeat_record_t;

typedef struct {
	int pid;

#ifdef MOCO
	int total_alg_num; // total number of algorithm
	int add_data_info;//FIXME : add variables to send
#endif
	double min_heartrate;
	double max_heartrate;
	int64_t window_size;

	int64_t counter;
	int64_t buffer_depth;
	int64_t buffer_index;
	int64_t read_index;
	char    valid;


} HB_global_state_t;

#ifdef MOCO
typedef struct {

	int mq_ID[2];	// 0 : mq_ID for App2Server 	1 : mq_ID for Server2App

} hbmq_t;
#endif


typedef struct {
#ifdef MOCO
	int	mq_ID[2];
#else
	int 	  mq_ID;
#endif

	int64_t first_timestamp;
	int64_t last_timestamp;

	int64_t* window;
	//int64_t window_size;
	int64_t current_index;

	int steady_state;
	double last_average_time;

	heartbeat_record_t* log;
#ifdef MOCO
	add_data_t subdata;  
#endif
	FILE* binary_file;
	FILE* text_file;
	char filename[256];
	pthread_mutex_t mutex;

	HB_global_state_t* state;

} heartbeat_t;

#ifdef __cplusplus
extern "C" {
#endif
	int heartbeat_init(heartbeat_t * hb, 
			double min_target, 
			double max_target, 
			int64_t window_size, 
			int64_t buffer_depth,
			char* log_name);

	void heartbeat_finish(heartbeat_t * hb);

	void hb_get_current(heartbeat_t volatile * hb, 
			heartbeat_record_t volatile * record);

	int hb_get_history(heartbeat_t volatile * hb,
			heartbeat_record_t volatile * record,
			int n);

	double hb_get_global_rate(heartbeat_t volatile * hb);

	double hb_get_windowed_rate(heartbeat_t volatile * hb);

	double hb_get_min_rate(heartbeat_t volatile * hb);

	double hb_get_max_rate(heartbeat_t volatile * hb);

	int64_t hb_get_window_size(heartbeat_t volatile * hb);

	int64_t heartbeat(heartbeat_t* hb, int tag);

#ifdef MOCO
	int mq_receive(heartbeat_t *);
	hbmq_t * connectSKB(int *app_Info);
	int hb_decision( heartbeat_t * hb );

	void mq_snd2( int pid, int mq_ID, double window, double instant, double global, int hb_id, int64_t ts, double a, double b, double c, double d);

#endif

#ifdef __cplusplus
}
#endif

#endif 

#ifndef __SA_MONITOR_PROCESS_H
#define __SA_MONITOR_PROCESS_H

#define MAX_THREAD_COUNT    1024

/** process state                                                        */
typedef enum process_state_type
{
    TYPE_PS_NONE = 0x00,                /// None
    TYPE_PS_RUNNING = 0x01,             /// R
    TYPE_PS_INTERRUPT_SLEEP = 0x02,     /// S
    TYPE_PS_DISK_SLEEP = 0x03,          /// D
    TYPE_PS_TRACED = 0x04,              /// T
    TYPE_PS_PAGING = 0x05,              /// W
    TYPE_PS_ZOMBIE = 0x06               /// Z
} PROCESS_STATE_TYPE;

/** This process is the parent, is the child?                            */
typedef enum process_pc_type
{
    TYPE_PS_PARENT = 0x11,
    TYPE_PS_CHILD = 0x12
} PROCESS_PC_TYPE;

/** Memory type                                                          */
typedef enum process_mem_type
{
    TYPE_MEM_PEAK = 0x21,
    TYPE_MEM_VM_SIZE = 0x22,
    TYPE_MEM_VM_RESIDENT = 0x23,
    TYPE_MEM_VM_PEAK_RESIDENT = 0x24
} PROCESS_MEM_TYPE;

/** process information                                                  */
typedef struct _process_info_t
{
    int pid;
    int type;
    int core_id;
    int thread_count;
    int process_state;
    int background;
    double usage;
    int thread_list[MAX_THREAD_COUNT];
} SA_PROCESS_INFO_T;

/** QoS information                                                     */
typedef struct _app_state_t
{
    int id;
    double heartbeat_global;
    double heartbeat_average;
    double heartbeat_instant;
} SA_APP_STATE_T;

typedef struct _mem_info_t
{
    int vm_peak;
    int vm_size;
    int vm_hwm;
    int vm_rss;
    int mem_size;
    double vm_usage_percent;
    double total_usage;
} SA_MEM_INFO_T;

typedef struct _network_info_t
{
    unsigned long total_tx;
    unsigned long total_rx;
    unsigned long delta_tx;
    unsigned long delta_rx;
    unsigned long rx_bandwidth;
    unsigned long tx_bandwidth;
    unsigned long backlogq_time;
    unsigned long preq_time;
    unsigned long ooo_time;
    unsigned long recvq_time;
    unsigned long latency_time;

    double rx_usage;
    double tx_usage;
} SA_NETWORK_INFO_T;


///////////////////////////////////////////////////////////////
typedef struct _static_t
{
        int pid;
        int type;
        long sampling_time;
        long buffer_depth;
        double max_qos;
        double min_qos;
        double target_qos;
        double window_size;
        char app_name[256];
        char app_path[1024];
} SA_APP_STATIC_INFO_T;
///////////////////////////////////////////////////////////////

typedef struct _process_t
{
    int count;
    SA_APP_STATIC_INFO_T static_info;
    SA_PROCESS_INFO_T process;
    SA_APP_STATE_T app;
    SA_MEM_INFO_T mem;
    SA_NETWORK_INFO_T net;
} SA_PROCESS_T;


SA_PROCESS_T *sa_process_t;

int sa_get_process_count(void);
int sa_get_process_info_list(SA_PROCESS_T *);
int sa_get_process_info(SA_PROCESS_T *, int);
double sa_get_process_usage(int);
PROCESS_STATE_TYPE sa_get_process_state(int);
int sa_get_thread_count(int);

int sa_get_mem_state(SA_MEM_INFO_T *, int);
int sa_get_mem_vmpeak(int);
int sa_get_mem_vmsize(int);
int sa_get_mem_vmhwm(int);
int sa_get_mem_vmrss(int);
double sa_get_proc_mem_usage(int);
double sa_get_sys_mem_total_usage(void);

unsigned long sa_get_network_total_rx_bytes(int);
unsigned long sa_get_network_total_tx_bytes(int);
unsigned long sa_get_network_delta_rx_bytes(int);
unsigned long sa_get_network_delta_tx_bytes(int);
unsigned long sa_get_network_rx_bandwinth(int);
unsigned long sa_get_network_tx_bandwinth(int);
unsigned long sa_get_network_backlogq_time(int);
unsigned long sa_get_network_preq_time(int);
unsigned long sa_get_network_ooo_time(int);
unsigned long sa_get_network_recvq_time(int);
unsigned long sa_get_network_latency_time(int);
double sa_get_network_rx_usage(int);
double sa_get_network_tx_usage(int);

double sa_get_core_usage_by_pid(int);


///////////////////////////////////////////////////////////////
int sa_get_process_static_info(SA_APP_STATIC_INFO_T *, int);
int sa_get_process_static_type(int);
long sa_get_process_static_sampling_time(int);
long sa_get_process_static_buffer_depth(int);
double sa_get_process_static_max_qos(int);
double sa_get_process_static_min_qos(int);
double sa_get_process_static_target_qos(int);
double sa_get_process_static_window_size(int);
int sa_get_process_name(int, char *);
int sa_get_process_path(int, char *);
///////////////////////////////////////////////////////////////
int sa_registration_application(int pid);
int sa_unregistration_application(int pid);
///////////////////////////////////////////////////////////////
int sa_get_proc_mem_kbytes(int pid);
int sa_get_sys_mem_kbytes();

#endif  /*  __SA_MONITOR_PROCESS_H  */

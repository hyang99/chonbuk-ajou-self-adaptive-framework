#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <assert.h>

#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include "skb-types.h"
#include "sa_monitor_system.h"
#include "sa_monitor_process.h"
#include "skb-shm.h"

#define CONNECT_IP  "127.0.0.1" //local
#define CONNECT_PORT    10000   // Hooo.

//#define DEBUG   1
#define DEBUG   0


int process_list_size = 50;


/**
 * @breif   Attach shared memory about 'ID'
 *
 * @param   id      id of identifier
 *
 * @return  void *  Memory area about id
 *
 */
void *_shm_attach(int id)
{
    void *mem;

    mem = shmat(id, 0, 0);
    if(mem == (void *)-1)
    {
        printf("Error Attach Shm!\n");
        return (void *)-1;
    }

    return mem;
}


/**
 * @breif   Get shared memory area from kernel
 *
 * @param   key     identifier for shared memory
 * @param   size    size of shared memory
 *
 * @return  int     On Success, return 0.
 *                      On Failure, return id;
 *
 */
int _shm_get(key_t key, int size)
{
    int id;

    if((id = shmget(key, size, IPC_CREAT | 0666)) < 0)
    {
        printf("Error Shared Mem!\n");
        return -1;
    }

    return id;
}

/**
 * @breif   Dttach shared memory from process
 *
 * @param   *shm    shared memory area to detach
 *
 * @return  int     On Success, return 0.
 *                      On Failure, return an integer less than Zero.
 *
 */
int _shm_detach(void *shm)
{
    int ret;

    if(shm != NULL)
    {
        ret = shmdt(shm);

        if(ret < 0)
        {
            printf("Error Shm Detach!\n");
            return -1;
        }
    }

    return 0;
}


/**
 * @breif   Delete shared memory from kernel
 *
 * @param   id      id of identifier
 *
 * @return  int     On Success, return 0.
 *                      On Failure, return an integer less than Zero.
 *
 */
int _shm_delete(int id)
{
    int ret;

    ret = shmctl(id, IPC_RMID, NULL);

    if(ret < 0)
    {
        printf("Error Shm Delete!\n");
        return -1;
    }

    return 0;
}


int skb_shm_exit()
{
    _shm_detach((void *)shm_core);
    _shm_delete(shm_id_core);

    _shm_detach((void *)shm_process);
    _shm_delete(shm_id_process);

    return 0;
}


int skb_shm_init()
{
    char make_file[64];
    unsigned int _spec_t_size = sizeof(SA_CPU_T)*max_core_count;
    unsigned int _process_t_size = sizeof(SA_PROCESS_T)*MAX_PROCESS_COUNT;
    process_list_size = MAX_PROCESS_COUNT;

    sprintf(make_file, "touch -a %s", PATH_CORE);
    system(make_file);
    memset(make_file, 0x00, sizeof(make_file));

    sprintf(make_file, "touch -a %s", PATH_PROCESS);
    system(make_file);
    memset(make_file, 0x00, sizeof(make_file));

    key_core = ftok(PATH_CORE, PROJ_CORE);
    shm_id_core = _shm_get(key_core, _spec_t_size);

    if(shm_id_core > 0)
    {
        shm_core = _shm_attach(shm_id_core);
        if(shm_core < (char *)0)
        {
            perror("Shared Mem Init Error(SA_CPU_T)\n");
            return -1;
        }

        sa_cpu_t = (SA_CPU_T *)shm_core;
    }

    key_process = ftok(PATH_PROCESS, PROJ_PROCESS);
    shm_id_process = _shm_get(key_process, _process_t_size);

    if(shm_id_process > 0)
    {
        shm_process = _shm_attach(shm_id_process);
        if(shm_process < (char *)0)
        {
            perror("Shared Mem Init Error(SA_PROCESS_T)\n");
            return -1;
        }

        sa_process_t = (SA_PROCESS_T *)shm_process;
    }

    return -1;
}
/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////

/**
 * @breif   Delete shared memory from kernel
 *
 * @param   path    path name for shared memory id
 * @param   proj_id project name for shared memory id
 *
 * @return  int     On Success, return shared memory.
 *                      On Failure, return NULL.
 *
 */
void *_get_shm_for_api(char *path, int proj)
{
    int id;
    void *shm;
    key_t key;

    key = ftok(path, proj);
    id = _shm_get(key, 0);

    if(id > 0)
    {
        shm = _shm_attach(id);
        if(shm < (void *)0)
        {
            perror("Error when getting shared memory\n");
            return NULL;
        }

        return shm;

    }

    return NULL;
}


int sa_cpu_spec_set(void)
{
    int i;

    for(i=0 ; i<max_core_count ; i++)
    {
        sa_cpu_t[i].phy_core = max_core_count;
        sa_cpu_t[i].spec.core_id = core_info[i].core_id;
        sa_cpu_t[i].spec.max_freq = core_info[i].core_static.core_max_freq;
        sa_cpu_t[i].spec.min_freq = core_info[i].core_static.core_min_freq;

        sa_cpu_t[i].spec.bigLITTLE = core_info[i].core_static.bigLITTLE;
        sa_cpu_t[i].spec.available_freq_count = core_info[i].core_static.available_freq_count;
        memcpy(sa_cpu_t[i].spec.available_freq, core_info[i].core_static.available_freq,
                sizeof(long)*MAX_FREQ_COUNT);
    }

    return 1;
}

/**
 * @breif   Data update to shared memory of dynamic information
 *
 * @param   NULL
 *
 * @return  NULL
 *
 */
int sa_cpu_state_update(void)
{
    int i;

    for(i=0 ; i<max_core_count ; i++)
    {
        sa_cpu_t[i].state.core_id = core_info[i].core_id;
        sa_cpu_t[i].spec.core_count = current_core_count;
        sa_cpu_t[i].total_usage = core_info[i].cpu_total_usage;
        sa_cpu_t[i].state.curr_freq = core_info[i].core_dynamic.core_cur_freq;
        sa_cpu_t[i].state.usage = core_info[i].core_dynamic.core_usage;
        if(core_info[i].core_dynamic.core_active == 1)
        {
            sa_cpu_t[i].state.enabled = TYPE_OPERATION_ENABLED;
        }
        else
        {
            sa_cpu_t[i].state.enabled = TYPE_OPERATION_DISABLED;
        }
        
        sa_cpu_t[i].net.total_rx = core_info[i].net_info.total_rx_bytes;
        sa_cpu_t[i].net.total_tx = core_info[i].net_info.total_tx_bytes;

        sa_cpu_t[i].net.delta_rx = core_info[i].net_info.delta_rx_bytes;
        sa_cpu_t[i].net.delta_tx = core_info[i].net_info.delta_tx_bytes;
    }

    return 1;
}


int sa_get_cpu_physical_core_count()
{
    int core;
    SA_CPU_T *tmp = (SA_CPU_T *)_get_shm_for_api(PATH_CORE, PROJ_CORE);

    core = tmp[0].phy_core;

#if DEBUG
    printf("[%s-%d] physical : %d\n", __func__, __LINE__, core);
#endif

    _shm_detach(tmp);

    return core;
}


int sa_get_cpu_current_core_count()
{
    int core;
    SA_CPU_T *tmp = (SA_CPU_T *)_get_shm_for_api(PATH_CORE, PROJ_CORE);

    core = tmp[0].spec.core_count;

#if DEBUG
    printf("[%s-%d] current : %d\n", __func__, __LINE__, core);
#endif

    _shm_detach(tmp);

    return core;
}


long sa_get_cpu_curr_freq(int core_id)
{
    long freq;
    SA_CPU_T *tmp = (SA_CPU_T *)_get_shm_for_api(PATH_CORE, PROJ_CORE);

    freq = tmp[core_id].state.curr_freq;

#if DEBUG
    printf("[%s-%d] freq : %ld\n", __func__, __LINE__, freq);
#endif

    _shm_detach(tmp);

    return freq;
}

double sa_get_core_usage(int core_id)
{
    double usage;
    SA_CPU_T *tmp = (SA_CPU_T *)_get_shm_for_api(PATH_CORE, PROJ_CORE);

    usage = tmp[core_id].state.usage;

#if DEBUG
    printf("[%s-%d] usage : %lf\n", __func__, __LINE__, usage);
#endif

    _shm_detach(tmp);

    return usage;
}

double sa_get_cpu_usage(void)
{
    double total;
    SA_CPU_T *tmp = (SA_CPU_T *)_get_shm_for_api(PATH_CORE, PROJ_CORE);

    total = tmp[0].total_usage;

#if DEBUG
    printf("[%s-%d] total : %lf\n", __func__, __LINE__, total);
#endif

    _shm_detach(tmp);

    return total;
}

int sa_is_cpu_enabled(int core_id)
{
    int enabled;
    SA_CPU_T *tmp = (SA_CPU_T *)_get_shm_for_api(PATH_CORE, PROJ_CORE);

    enabled = tmp[core_id].state.enabled;

#if DEBUG
    printf("[%s-%d] enabled : %d\n", __func__, __LINE__, enabled);
#endif

    _shm_detach(tmp);

    return enabled;
}

double sa_get_cpu_linear_curr_freq(int core_id)
{
    double linear_freq;
    long curr, min, max;

    SA_CPU_T *tmp = (SA_CPU_T *)_get_shm_for_api(PATH_CORE, PROJ_CORE);

    curr = tmp[core_id].state.curr_freq;
    min = tmp[core_id].spec.min_freq;
    max = tmp[core_id].spec.max_freq;

    linear_freq = ((double)(curr - min)/(double)(max - min));

#if DEBUG
    printf("[%s-%d] linear_freq : %lf\n", __func__, __LINE__, linear_freq);
#endif

    _shm_detach(tmp);

    return linear_freq;
}

int sa_get_cpu_spec_info(SA_CPU_SPEC_T *spec, int core_id)
{
    int ret = -1;
    SA_CPU_T *tmp = (SA_CPU_T *)_get_shm_for_api(PATH_CORE, PROJ_CORE);

    if(spec)
    {
        memcpy(spec, &(tmp[core_id].spec), sizeof(SA_CPU_SPEC_T));
        ret = 1;
    }

#if DEBUG
    printf("[%s-%d] core_id : %d\n", __func__, __LINE__, tmp[core_id].spec.core_id);
    printf("[%s-%d] core_count : %d\n", __func__, __LINE__, tmp[core_id].spec.core_count);
    printf("[%s-%d] max_freq : %ld\n", __func__, __LINE__, tmp[core_id].spec.max_freq);
    printf("[%s-%d] min_freq : %ld\n", __func__, __LINE__, tmp[core_id].spec.min_freq);
    printf("[%s-%d] bigLITTLE : %d\n", __func__, __LINE__, tmp[core_id].spec.bigLITTLE);
    printf("[%s-%d] available_freq_count : %d\n", __func__, __LINE__, tmp[core_id].spec.available_freq_count);
    int i;
    for(i=0 ; i<tmp[core_id].spec.available_freq_count ; i++)
    {
        printf("[%s-%d] available_freq : %ld\n", __func__, __LINE__, tmp[core_id].spec.available_freq[i]);
    }
#endif

    _shm_detach(tmp);

    return ret;
}

int sa_get_cpu_state(SA_CPU_STATE_T *state, int core_id)
{
    int ret = -1;
    SA_CPU_T *tmp = (SA_CPU_T *)_get_shm_for_api(PATH_CORE, PROJ_CORE);

    if(state)
    {
        memcpy(state, &(tmp[core_id].state), sizeof(SA_CPU_STATE_T));
        ret = 1;
    }

#if DEBUG
    printf("[%s-%d] core_id : %d\n", __func__, __LINE__, tmp[core_id].state.core_id);
    printf("[%s-%d] curr_freq : %ld\n", __func__, __LINE__, tmp[core_id].state.curr_freq);
    printf("[%s-%d] enabled : %d\n", __func__, __LINE__, tmp[core_id].state.enabled);
    printf("[%s-%d] usage : %lf\n", __func__, __LINE__, tmp[core_id].state.usage);
#endif

    _shm_detach(tmp);

    return ret;
}

/////////////////////////////////////////////////////////////////////////
int sa_get_cpu_bigLITTLE_type(int core_id)
{
    int bigLITTLE = -1;

    SA_CPU_T *tmp = (SA_CPU_T *)_get_shm_for_api(PATH_CORE, PROJ_CORE);

    bigLITTLE = tmp[core_id].spec.bigLITTLE;

#if DEBUG
    printf("[%s-%d] bigLITTLE : %d\n", __func__, __LINE__, bigLITTLE);
#endif

    _shm_detach(tmp);

    return bigLITTLE;
}

int sa_get_cpu_available_freq(long *list, int core_id)
{
    int count = -1;

    SA_CPU_T *tmp = (SA_CPU_T *)_get_shm_for_api(PATH_CORE, PROJ_CORE);

    if(list)
    {
        memcpy(list, &(tmp[core_id].spec.available_freq), sizeof(long)*MAX_FREQ_COUNT);
        count = tmp[core_id].spec.available_freq_count;
    }

    _shm_detach(tmp);

    return count;
}
/////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////

int sa_process_update(void)
{
    int i, j, k, pid;
    SA_PROCESS_T *shared;
    PROCESS_INFO *local_proc;
    APP_INFO *local_app;

    if(app_count > 0)
    {
        for(i=0 ; i<MAX_PROCESS_COUNT ; i++)
        {
            memset(&sa_process_t[i], 0x00, sizeof(SA_PROCESS_T));
            shared = &sa_process_t[i];
            local_proc = &process_info[i];
            pid = local_proc->pid;

            memset(&sa_process_t[i], 0x00, sizeof(SA_PROCESS_T));
            shared = &sa_process_t[i];
            local_proc = &process_info[i];

            shared->count = app_count;
            shared->process.pid = local_proc->pid;
            shared->process.type = -1;      // Do not defined;
            shared->process.core_id = local_proc->core_id;
            shared->process.thread_count = local_proc->thread_on_proc;
            memcpy(shared->process.thread_list, local_proc->thread_list, shared->process.thread_count*sizeof(int));

            shared->process.background = -1;    // Do not defined;
            shared->process.usage = local_proc->usage;

            switch(local_proc->proc_state)
            {
                case PROCESS_RUNNING:
                    shared->process.process_state = TYPE_PS_RUNNING;
                    break;
                case PROCESS_INTERRUPT_SLEEP:
                    shared->process.process_state = TYPE_PS_INTERRUPT_SLEEP;
                    break;
                case PROCESS_DISK_SLEEP:
                    shared->process.process_state = TYPE_PS_DISK_SLEEP;
                    break;
                case PROCESS_TRACED:
                    shared->process.process_state = TYPE_PS_TRACED;
                    break;
                case PROCESS_PAGING:
                    shared->process.process_state = TYPE_PS_PAGING;
                    break;
                case PROCESS_ZOMBIE:
                    shared->process.process_state = TYPE_PS_ZOMBIE;
                    break;
                defalut:
                    shared->process.process_state = TYPE_PS_NONE;
                    break;
            }

            shared->mem.vm_peak = local_proc->mem.vmpeak;
            shared->mem.vm_size = local_proc->mem.vmsize;
            shared->mem.vm_hwm = local_proc->mem.vmhwm;
            shared->mem.vm_rss = local_proc->mem.vmrss;
            shared->mem.vm_usage_percent = local_proc->mem.usage;
            //shared->mem.total_usage = process_info[0].mem.total_usage;
            shared->mem.total_usage = local_proc->mem.total_usage;
            shared->mem.mem_size = core_info[0].mem_size;

            shared->net.total_tx = local_proc->net.total_tx_bytes;
            shared->net.total_rx = local_proc->net.total_rx_bytes;
            shared->net.delta_tx = local_proc->net.delta_tx_bytes;
            shared->net.delta_rx = local_proc->net.delta_rx_bytes;

            shared->net.tx_bandwidth = local_proc->net.tx_bandwidth;
            shared->net.rx_bandwidth = local_proc->net.rx_bandwidth;

            shared->net.backlogq_time = local_proc->net.backlogq_time;
            shared->net.preq_time = local_proc->net.preq_time;
            shared->net.ooo_time = local_proc->net.ooo_time;
            shared->net.recvq_time = local_proc->net.recvq_time;
            shared->net.latency_time = local_proc->net.latency_time;

            shared->net.tx_usage = local_proc->net.tx_usage;
            shared->net.rx_usage = local_proc->net.rx_usage;


            for(j=0 ; j<MAX_APP_COUNT ; j++)
            {
                if(app_info[j].pid == shared->process.pid)
                //if(app_info[j].pid == shared->process.pid && (app_info[j].pid > 0))
                {
                    shared->app.id = app_info[j].app_dynamic.app_hb_id;
                    shared->app.heartbeat_global = app_info[j].app_dynamic.app_global_rate;
                    shared->app.heartbeat_average = app_info[j].app_dynamic.app_window_rate;
                    shared->app.heartbeat_instant = app_info[j].app_dynamic.app_instant_rate;

                    shared->static_info.pid = app_info[j].pid;
                    shared->static_info.type = app_info[j].app_static.app_type;
                    shared->static_info.sampling_time = app_info[j].app_static.app_sampling_time;
                    shared->static_info.buffer_depth = app_info[j].app_static.app_buffer_depth;
                    shared->static_info.max_qos = app_info[j].app_static.app_max_qos;
                    shared->static_info.min_qos = app_info[j].app_static.app_min_qos;
                    shared->static_info.target_qos = app_info[j].app_static.app_target_qos;
                    shared->static_info.window_size = app_info[j].app_static.app_window_size;

                    memcpy(shared->static_info.app_name, app_info[j].app_name, 256);
                    memcpy(shared->static_info.app_path, app_info[j].app_path, 1024);

                    break;
                }
            }
        }
    }
    else
    {
        for(i=0 ; i<process_list_size ; i++)
        {
            memset(&sa_process_t[i], 0x00, sizeof(SA_PROCESS_T));
        }
    }

    return 1;
}

//SA_PROCESS_T *_get_process_shm_by_pid(int pid)
int _get_process_shm_by_pid(int pid, SA_PROCESS_T *data)
{
    int i;

    SA_PROCESS_T *tmp = (SA_PROCESS_T *)_get_shm_for_api(PATH_PROCESS, PROJ_PROCESS);

    if(!tmp) return -1;

    for(i=0 ; i<MAX_PROCESS_COUNT ; i++)
    {
        if(tmp[i].process.pid == pid)
        {

#if DEBUG
            printf("[LIB - %s/%d] pid : %d\n", __func__, __LINE__, pid);
            printf(" count : %d\n", tmp[i].count);
            printf(" pid : %d\n", tmp[i].static_info.pid);
            printf(" type : %d\n", tmp[i].static_info.type);
            printf(" sampling_time : %ld\n", tmp[i].static_info.sampling_time);
            printf(" buffer_depth : %ld\n", tmp[i].static_info.buffer_depth);
            printf(" max_qos : %lf\n", tmp[i].static_info.max_qos);
            printf(" min_qos : %lf\n", tmp[i].static_info.min_qos);
            printf(" target_qos : %lf\n", tmp[i].static_info.target_qos);
            printf(" window_size : %lf\n", tmp[i].static_info.window_size);
            printf(" app_name : %s\n", tmp[i].static_info.app_name);
            printf(" app_path : %s\n", tmp[i].static_info.app_path);

            printf(" type : %d\n", tmp[i].process.type);
            printf(" core_id : %d\n", tmp[i].process.core_id);
            printf(" thread_count : %d\n", tmp[i].process.thread_count);
            printf(" process_state : %d\n", tmp[i].process.process_state);
            printf(" background : %d\n", tmp[i].process.background);
            printf(" usage : %lf\n", tmp[i].process.usage);

            printf(" id : %d\n", tmp[i].app.id);
            printf(" heartbeat_global : %lf\n", tmp[i].app.heartbeat_global);
            printf(" heartbeat_average : %lf\n", tmp[i].app.heartbeat_average);
            printf(" heartbeat_instant : %lf\n", tmp[i].app.heartbeat_instant);

            printf(" vm_peak : %d\n", tmp[i].mem.vm_peak);
            printf(" vm_size : %d\n", tmp[i].mem.vm_size);
            printf(" vm_hwm : %d\n", tmp[i].mem.vm_hwm);
            printf(" vm_rss : %d\n", tmp[i].mem.vm_rss);
            printf(" vm_usage_percent : %lf\n", tmp[i].mem.vm_usage_percent);
            printf(" total_usage : %lf\n", tmp[i].mem.total_usage);
#endif
            memcpy(data, &tmp[i], sizeof(SA_PROCESS_T));

            _shm_detach(tmp);
            return 0;
        }
    }
    
    _shm_detach(tmp);
    return -1;
}


int sa_get_process_count(void)
{
    int count;
    SA_PROCESS_T *tmp = (SA_PROCESS_T *)_get_shm_for_api(PATH_PROCESS, PROJ_PROCESS);

    count = tmp[0].count;

#if DEBUG
    printf("[%s-%d] count : %d\n", __func__, __LINE__, count);
#endif

    _shm_detach(tmp);

    return count;
}

int sa_get_process_info_list(SA_PROCESS_T *info)
{
    int i;
    int count;
    int index = 0;
    SA_PROCESS_T *tmp = (SA_PROCESS_T *)_get_shm_for_api(PATH_PROCESS, PROJ_PROCESS);

    count = tmp[0].count;

    for(i=0 ; i<process_list_size ; i++)
    {
        if(tmp[i].static_info.pid > 0)
        {
            memcpy(&info[index], &tmp[i], sizeof(SA_PROCESS_T));
            index++;

#if DEBUG
    printf("[%s-%d/%d] count : %d\n", __func__, __LINE__, i, tmp[i].count);
    printf("[%s-%d/%d] pid : %d\n", __func__, __LINE__, i, tmp[i].static_info.pid);
#endif

        }
    }

    _shm_detach(tmp);

    return count;
}

int sa_get_process_info(SA_PROCESS_T *info, int pid)
{
    int ret = -1;
    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    memcpy(info, &tmp, sizeof(SA_PROCESS_T));
    ret = 1;

    return ret;
}

double sa_get_process_usage(int pid)
{
    double ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.process.usage;

#if DEBUG
    printf("[%s-%d] p_usage : %lf\n", __func__, __LINE__, ret);
#endif

    return ret;
}

PROCESS_STATE_TYPE sa_get_process_state(int pid)
{
    int ret = TYPE_PS_NONE;
    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.process.process_state;

#if DEBUG
    printf("[%s-%d] p_state : %d\n", __func__, __LINE__, ret);
#endif

    return ret;
}

int sa_get_thread_count(int pid)
{
    int ret = -1;
    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.process.thread_count;

#if DEBUG
    printf("[%s-%d] thread_count : %d\n", __func__, __LINE__, ret);
#endif

    return ret;
}

int sa_get_mem_state(SA_MEM_INFO_T *mem, int pid)
{
    int ret = -1;
    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    memcpy(mem, &(tmp.mem), sizeof(SA_MEM_INFO_T));
    ret = 1;

#if DEBUG
    printf("[%s-%d] vm_peak : %d\n", __func__, __LINE__, tmp.mem.vm_peak);
    printf("[%s-%d] vm_size : %d\n", __func__, __LINE__, tmp.mem.vm_size);
    printf("[%s-%d] vm_hwm : %d\n", __func__, __LINE__, tmp.mem.vm_hwm);
    printf("[%s-%d] vm_rss : %d\n", __func__, __LINE__, tmp.mem.vm_rss);
    printf("[%s-%d] vm_usage_percent : %lf\n", __func__, __LINE__, tmp.mem.vm_usage_percent);
    printf("[%s-%d] total_usage : %lf\n", __func__, __LINE__, tmp.mem.total_usage);
#endif

    return ret;
}

int sa_get_mem_vmpeak(int pid)
{
    int ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.mem.vm_peak;

#if DEBUG
    printf("[%s-%d] vm_peak : %d\n", __func__, __LINE__, ret);
#endif

    return ret;
}

int sa_get_mem_vmsize(int pid)
{
    int ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.mem.vm_size;

#if DEBUG
    printf("[%s-%d] vm_size : %d\n", __func__, __LINE__, ret);
#endif

    return ret;
}

int sa_get_mem_vmhwm(int pid)
{
    int ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.mem.vm_hwm;

#if DEBUG
    printf("[%s-%d] vm_hwm : %d\n", __func__, __LINE__, ret);
#endif

    return ret;
}

int sa_get_mem_vmrss(int pid)
{
    int ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.mem.vm_rss;

#if DEBUG
    printf("[%s-%d] vm_rss : %d\n", __func__, __LINE__, ret);
#endif

    return ret;
}

double sa_get_proc_mem_usage(int pid)
{
    double ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.mem.vm_usage_percent;

#if DEBUG
    printf("[%s-%d] vm_usage_percent : %lf\n", __func__, __LINE__, ret);
#endif

    return ret;
}

double sa_get_sys_mem_total_usage()
{
    double ret = -1;
    SA_PROCESS_T *tmp = (SA_PROCESS_T *)_get_shm_for_api(PATH_PROCESS, PROJ_PROCESS);

    if(!tmp)
        return ret;

    ret = tmp[0].mem.total_usage;

#if DEBUG
    printf("[%s-%d] total_usage : %lf\n", __func__, __LINE__, ret);
#endif

    _shm_detach(tmp);

    return ret;
}

unsigned long sa_get_network_total_rx_bytes(int pid)
{
    unsigned long ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.net.total_rx;

#if DEBUG
    printf("[%s-%d] proc_total_rx : %ld\n", __func__, __LINE__, ret);
#endif

    return ret;
}

unsigned long sa_get_network_total_tx_bytes(int pid)
{
    unsigned long ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.net.total_tx;

#if DEBUG
    printf("[%s-%d] proc_total_tx : %ld\n", __func__, __LINE__, ret);
#endif

    return ret;
}

unsigned long sa_get_network_delta_rx_bytes(int pid)
{
    unsigned long ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.net.delta_rx;

#if DEBUG
    printf("[%s-%d] proc_delta_rx : %ld\n", __func__, __LINE__, ret);
#endif

    return ret;
}

unsigned long sa_get_network_delta_tx_bytes(int pid)
{
    unsigned long ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.net.delta_tx;

#if DEBUG
    printf("[%s-%d] proc_delta_tx : %ld\n", __func__, __LINE__, ret);
#endif

    return ret;
}


unsigned long sa_get_network_rx_bandwinth(int pid)
{
    unsigned long ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.net.rx_bandwidth;

#if DEBUG
    printf("[%s-%d] rx_bandwidth : %ld\n", __func__, __LINE__, ret);
#endif

    return ret;
}

unsigned long sa_get_network_tx_bandwinth(int pid)
{
    unsigned long ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.net.tx_bandwidth;

#if DEBUG
    printf("[%s-%d] tx_bandwidth : %ld\n", __func__, __LINE__, ret);
#endif

    return ret;
}

unsigned long sa_get_network_backlogq_time(int pid)
{
    unsigned long ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.net.backlogq_time;

#if DEBUG
    printf("[%s-%d] backlogq_time : %ld\n", __func__, __LINE__, ret);
#endif

    return ret;
}

unsigned long sa_get_network_preq_time(int pid)
{
    unsigned long ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.net.preq_time;

#if DEBUG
    printf("[%s-%d] preq_time : %ld\n", __func__, __LINE__, ret);
#endif

    return ret;
}

unsigned long sa_get_network_ooo_time(int pid)
{
    unsigned long ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.net.ooo_time;

#if DEBUG
    printf("[%s-%d] ooo_time : %ld\n", __func__, __LINE__, ret);
#endif

    return ret;
}

unsigned long sa_get_network_recvq_time(int pid)
{
    unsigned long ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.net.recvq_time;

#if DEBUG
    printf("[%s-%d] recvq_time : %ld\n", __func__, __LINE__, ret);
#endif

    return ret;
}

unsigned long sa_get_network_latency_time(int pid)
{
    unsigned long ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.net.latency_time;

#if DEBUG
    printf("[%s-%d] latency_time : %ld\n", __func__, __LINE__, ret);
#endif

    return ret;
}

double sa_get_network_rx_usage(int pid)
{
    double ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.net.rx_usage;

#if DEBUG
    printf("[%s-%d] rx_usage : %lf\n", __func__, __LINE__, ret);
#endif

    return ret;
}

double sa_get_network_tx_usage(int pid)
{
    double ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.net.tx_usage;

#if DEBUG
    printf("[%s-%d] tx_usage : %lf\n", __func__, __LINE__, ret);
#endif

    return ret;
}


/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////

int sa_get_core_by_pid(int pid)
{
    int core = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    core = tmp.process.core_id;

    return core;
}


double sa_get_core_usage_by_pid(int pid)
{
    int core = -1;
    double usage = 0;

    while((core = sa_get_core_by_pid(pid)) < 0);

    //printf(" >> %d\n", core);

    usage = sa_get_core_usage(core);

    return usage;
}



/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////



int sa_get_process_static_info(SA_APP_STATIC_INFO_T *info, int pid)
{
    int ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    memcpy(info, &(tmp.static_info), sizeof(SA_APP_STATIC_INFO_T));
    ret = 1;

#if DEBUG
    printf(" count : %d\n", tmp.count);
    printf(" pid : %d\n", tmp.static_info.pid);
    printf(" type : %d\n", tmp.static_info.type);
    printf(" sampling_time : %ld\n", tmp.static_info.sampling_time);
    printf(" buffer_depth : %ld\n", tmp.static_info.buffer_depth);
    printf(" max_qos : %lf\n", tmp.static_info.max_qos);
    printf(" min_qos : %lf\n", tmp.static_info.min_qos);
    printf(" target_qos : %lf\n", tmp.static_info.target_qos);
    printf(" window_size : %lf\n", tmp.static_info.window_size);
    printf(" app_name : %s\n", tmp.static_info.app_name);
    printf(" app_path : %s\n", tmp.static_info.app_path);
#endif

    return ret;
}

int sa_get_process_static_type(int pid)
{
    int ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.static_info.type;

#if DEBUG
    printf("[%s-%d] static_info.type : %d\n", __func__, __LINE__, ret);
#endif

    return ret;
}

long sa_get_process_static_sampling_time(int pid)
{
    long ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.static_info.sampling_time;

#if DEBUG
    printf("[%s-%d] static_info.sampling_time : %ld\n", __func__, __LINE__, ret);
#endif

    return ret;
}

long sa_get_process_static_buffer_depth(int pid)
{
    long ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.static_info.buffer_depth;

#if DEBUG
    printf("[%s-%d] static_info.buffer_depth : %ld\n", __func__, __LINE__, ret);
#endif

    return ret;
}

double sa_get_process_static_max_qos(int pid)
{
    double ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.static_info.max_qos;

#if DEBUG
    printf("[%s-%d] static_info.max_qos : %lf\n", __func__, __LINE__, ret);
#endif

    return ret;
}

double sa_get_process_static_min_qos(int pid)
{
    double ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.static_info.min_qos;

#if DEBUG
    printf("[%s-%d] static_info.min_qos : %lf\n", __func__, __LINE__, ret);
#endif

    return ret;
}


double sa_get_process_static_target_qos(int pid)
{
    double ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.static_info.target_qos;

#if DEBUG
    printf("[%s-%d] static_info.target_qos : %lf\n", __func__, __LINE__, ret);
#endif

    return ret;
}

double sa_get_process_static_window_size(int pid)
{
    double ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.static_info.window_size;

#if DEBUG
    printf("[%s-%d] static_info.window_size : %lf\n", __func__, __LINE__, ret);
#endif

    return ret;
}

int sa_get_process_name(int pid, char *name)
{
    int ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    memcpy(name, tmp.static_info.app_name, sizeof(tmp.static_info.app_name));
    ret = 1;

#if DEBUG
    printf("[%s-%d] static_info.app_name : %s\n", __func__, __LINE__, tmp.static_info.app_name);
#endif

    return ret;
}

int sa_get_process_path(int pid, char *path)
{
    int ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    memcpy(path, tmp.static_info.app_path, sizeof(tmp.static_info.app_path));
    ret = 1;

#if DEBUG
    printf("[%s-%d] static_info.app_path : %s\n", __func__, __LINE__, tmp.static_info.app_path);
#endif

    return ret;
}

double sa_get_process_global_rate(int pid)
{
    double ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.app.heartbeat_global;

#if DEBUG
    printf("[%s-%d] app.heartbeat_global : %lf\n", __func__, __LINE__, ret);
#endif

    return ret;
}

double sa_get_process_window_rate(int pid)
{
    double ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.app.heartbeat_average;

#if DEBUG
    printf("[%s-%d] app.heartbeat_average : %lf\n", __func__, __LINE__, ret);
#endif

    return ret;
}

double sa_get_process_instant_rate(int pid)
{
    double ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.app.heartbeat_instant;

#if DEBUG
    printf("[%s-%d] app.heartbeat_instant : %lf\n", __func__, __LINE__, ret);
#endif

    return ret;
}

int sa_registration_application(int pid)
{
    struct sockaddr_in addr_in;
    int sfd;
    char buf[64] = {0};
    char tmp[4] = {0};
    unsigned int type = 0x080;
    unsigned int len;

    sprintf(tmp, "%d", pid);
    len = sizeof(unsigned int)*2 + strlen(tmp);

    memcpy(buf, &len, sizeof(unsigned int));
    memcpy(buf+4, &type, sizeof(unsigned int));
    memcpy(buf+8, tmp, strlen(tmp));

    sfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sfd == -1)
    {
        printf("Error: socket in sa_registration_application\n");
        return -1;
    }

    memset(&addr_in, 0x00, sizeof(struct sockaddr_in));
    addr_in.sin_family = AF_INET;
    addr_in.sin_addr.s_addr = inet_addr(CONNECT_IP);
    addr_in.sin_port = htons(CONNECT_PORT);

    if(connect(sfd, (struct sockaddr *)&addr_in, sizeof(struct sockaddr_in)) == -1)
    {
        printf("Error: connect in sa_registration_application\n");
        return -1;
    }

    if(write(sfd, buf, len) < 0)
    {
        printf("Error: write in sa_registration_application\n");
        shutdown(sfd, 2);
        return -1;
    }

    memset(buf, 0x00, sizeof(buf));

    if(read(sfd, buf, sizeof(buf)) < 0)
    {
        printf("Error: read in sa_registration_application\n");
        shutdown(sfd, 2);
        return -1;
    }

    memcpy(&type, &buf[4], sizeof(unsigned int));

    if(type != 0x800)
    {
        printf("Error: type mismatch in sa_registration_application\n");
        shutdown(sfd, 2);
        return -1;
    }

    shutdown(sfd, 2);
    return 1;
}

int sa_unregistration_application(int pid)
{
    struct sockaddr_in addr_in;
    int sfd;
    char buf[64] = {0};
    char tmp[4] = {0};
    unsigned int type = 0x081;
    unsigned int len;

    sprintf(tmp, "%d", pid);
    len = sizeof(unsigned int)*2 + strlen(tmp);

    memcpy(buf, &len, sizeof(unsigned int));
    memcpy(buf+4, &type, sizeof(unsigned int));
    memcpy(buf+8, tmp, strlen(tmp));

    sfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sfd == -1)
    {
        printf("Error: socket in sa_unregistration_application\n");
        return -1;
    }

    memset(&addr_in, 0x00, sizeof(struct sockaddr_in));
    addr_in.sin_family = AF_INET;
    addr_in.sin_addr.s_addr = inet_addr(CONNECT_IP);
    addr_in.sin_port = htons(CONNECT_PORT);

    if(connect(sfd, (struct sockaddr *)&addr_in, sizeof(struct sockaddr_in)) == -1)
    {
        printf("Error: connect in sa_unregistration_application\n");
        return -1;
    }

    if(write(sfd, buf, len) < 0)
    {
        printf("Error: write in sa_unregistration_application\n");
        shutdown(sfd, 2);
        return -1;
    }

    memset(buf, 0x00, sizeof(buf));

    if(read(sfd, buf, sizeof(buf)) < 0)
    {
        printf("Error: read in sa_unregistration_application\n");
        shutdown(sfd, 2);
        return -1;
    }

    memcpy(&type, &buf[4], sizeof(unsigned int));

    if(type != 0x801)
    {
        printf("Error: type mismatch in sa_registration_application\n");
        shutdown(sfd, 2);
        return -1;
    }

    shutdown(sfd, 2);
    return 1;
}

unsigned long sa_get_sys_network_total_rx_bytes()
{
    unsigned long bytes;
    SA_CPU_T *tmp = (SA_CPU_T *)_get_shm_for_api(PATH_CORE, PROJ_CORE);

    bytes = tmp[0].net.total_rx;

#if DEBUG
    printf("[%s-%d] net.total_rx : %ld\n", __func__, __LINE__, bytes);
#endif

    _shm_detach(tmp);

    return bytes;
}

unsigned long sa_get_sys_network_total_tx_bytes()
{
    unsigned long bytes;
    SA_CPU_T *tmp = (SA_CPU_T *)_get_shm_for_api(PATH_CORE, PROJ_CORE);

    bytes = tmp[0].net.total_tx;

#if DEBUG
    printf("[%s-%d] net.total_tx : %ld\n", __func__, __LINE__, bytes);
#endif

    _shm_detach(tmp);

    return bytes;
}

unsigned long sa_get_sys_network_delta_rx_bytes()
{
    unsigned long bytes;
    SA_CPU_T *tmp = (SA_CPU_T *)_get_shm_for_api(PATH_CORE, PROJ_CORE);

    bytes = tmp[0].net.delta_rx;

#if DEBUG
    printf("[%s-%d] net.delta_rx : %ld\n", __func__, __LINE__, bytes);
#endif

    _shm_detach(tmp);

    return bytes;
}

unsigned long sa_get_sys_network_delta_tx_bytes()
{
    unsigned long bytes;
    SA_CPU_T *tmp = (SA_CPU_T *)_get_shm_for_api(PATH_CORE, PROJ_CORE);

    bytes = tmp[0].net.delta_tx;

#if DEBUG
    printf("[%s-%d] net.delta_tx : %ld\n", __func__, __LINE__, bytes);
#endif

    _shm_detach(tmp);

    return bytes;
}

int sa_get_proc_mem_kbytes(int pid)
{
    int ret = -1;

    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.mem.vm_size;

#if DEBUG
    printf("[%s-%d] proc mem kbytes : %ld\n", __func__, __LINE__, ret);
#endif

    return ret;
}

int sa_get_sys_mem_kbytes()
{
    int ret = -1;
    SA_PROCESS_T *tmp = (SA_PROCESS_T *)_get_shm_for_api(PATH_PROCESS, PROJ_PROCESS);

    if(!tmp)
        return ret;

    ret = tmp->mem.mem_size;

    _shm_detach(tmp);

#if DEBUG
    printf("[%s-%d] proc mem kbytes : %ld\n", __func__, __LINE__, ret);
#endif

    return ret;
}


/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////

/////////////////////////  control section  /////////////////////////////

/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////


int _get_thread_list_by_pid(int pid, int *list, int size)
{
    int ret = -1;
    SA_PROCESS_T tmp;
    _get_process_shm_by_pid(pid, &tmp);

    ret = tmp.process.thread_count;
    memcpy(list, tmp.process.thread_list, ret*sizeof(int));

#if DEBUG
    int i;

    for(i=0 ; i<ret ; i++)
    {
        printf("[%s-%d] ret: %d / thread list : %d\n", __func__, __LINE__, ret, list[i]);
    }
#endif

    return ret;
}


int sa_set_affinity_threads(int pid, int start, int end, cpu_set_t mask)
{
    int list[MAX_THREAD_COUNT];
    int thread_count, ret = -1;
    int c_count;
    int i;

    thread_count = _get_thread_list_by_pid(pid, list, sizeof(list)/sizeof(int));

    if(thread_count > 0)
    {
        for(i=0 ; i<thread_count ; i++)
        {
            if(i>=start && i<end)
            {
                ret = sched_setaffinity(list[i], sizeof(mask), &mask);

                if(ret < 0)
                {
                    c_count = CPU_COUNT(&mask);
                }
                else
                {
                    ret++;
                }
            }
        }
    }

    return ret;
}


int sa_enable_core(int core)
{
    int ret;
    char location[64];
    FILE *fp;
    static char enable = '1';

    if(core == 0)
    {
        printf(" CPU [%d] can NOT control\n", core);
        return -1;
    }

    memset(location, 0x00, sizeof(location));
    sprintf(location, "/sys/devices/system/cpu/cpu%d/online", core);

    fp = fopen(location, "w");

    if(fp != NULL)
    {
        ret = fwrite(&enable, sizeof(enable), 1, fp);
        fclose(fp);

        if(ret > 0)
            return 1;
        else
            return -1;
    }

    return -1;
}

int sa_disable_core(int core)
{
    int ret;
    char location[64];
    FILE *fp;
    static char enable = '0';

    if(core == 0)
    {
        printf(" CPU [%d] can NOT control\n", core);
        return -1;
    }

    memset(location, 0x00, sizeof(location));
    sprintf(location, "/sys/devices/system/cpu/cpu%d/online", core);

    fp = fopen(location, "w");

    if(fp != NULL)
    {
        ret = fwrite(&enable, sizeof(enable), 1, fp);
        fclose(fp);

        if(ret > 0)
            return 1;
        else
            return -1;
    }

    return -1;
}

int sa_set_current_freq(int core, int freq)
{
    int ret;
    char location[64];
    FILE *fp;
    char buff[16];
    char tmp[16];

    memset(location, 0x00, sizeof(location));
    memset(buff, 0x00, sizeof(buff));
    sprintf(buff, "%d", freq);

    sprintf(location, "/sys/devices/system/cpu/cpu%d/cpufreq/scaling_max_freq", core);

    fp = fopen(location, "w");

    if(fp != NULL)
    {
        ret = fwrite(buff, sizeof(buff), 1, fp);
        fclose(fp);

    }

    memset(location, 0x00, sizeof(location));
    sprintf(location, "/sys/devices/system/cpu/cpu%d/cpufreq/scaling_min_freq", core);

    fp = fopen(location, "w");

    if(fp != NULL)
    {
        ret = fwrite(buff, sizeof(buff), 1, fp);
        fclose(fp);

    }

    memset(location, 0x00, sizeof(location));
    memset(tmp, 0x00, sizeof(tmp));
    sprintf(location, "/sys/devices/system/cpu/cpu%d/cpufreq/scaling_cur_freq", core);

    fp = fopen(location, "r");

    if(fp != NULL)
    {
        ret = fread(tmp, sizeof(tmp), 1, fp);
        fclose(fp);
        printf(" -> [%d] core current freq. : %d\n", core, atoi(tmp));
    }

    return 1;
}

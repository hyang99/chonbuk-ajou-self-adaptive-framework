#!/bin/bash

#gcc -o test test.c -I../include -I./ -L./ -lskb
gcc -c test.c -I../include
gcc -Wl,-E -o test -Wl,-rpath,. test.o -lskb -L.

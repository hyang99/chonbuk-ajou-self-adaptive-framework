#ifndef __SA_MONITOR_SYSTEM_H
#define __SA_MONITOR_SYSTEM_H

//#include "skb-types.h"

///-------------------------------------------------------------------
typedef struct system_net
{
    unsigned long total_rx;
    unsigned long total_tx;

    double rx_usage;
    double tx_usage;
} SA_NET_SYS_T;
///-------------------------------------------------------------------

/** Enum type for Core cluster type                                    */
typedef enum core_cluster_type
{
    TYPE_CLUSTER_MASTER = 0x01,
    TYPE_CLUSTER_SLAVE = 0x02
} CORE_CLUSTER_TYPE;

/** Enum type for Core Active state                                    */
typedef enum core_operation_state
{
    TYPE_OPERATION_DISABLED = 0x00,
    TYPE_OPERATION_ENABLED = 0x01
} CORE_OPERATION_STATE;

/** Core Spec                                                          */
typedef struct _spec_t
{
    int core_id;
    int core_count;
    long max_freq;
    long min_freq;
    int bigLITTLE;      /// big : 1, LITTLE : 0

    long available_freq[MAX_FREQ_COUNT];  /// available freq. list of core
    int available_freq_count;             /// available freq. count
} SA_CPU_SPEC_T;

/** Core State                                                         */
typedef struct _state_t
{
    int core_id;
    long curr_freq;
    int enabled;
    double usage;
} SA_CPU_STATE_T;

/** Net State                                                          */
typedef struct _net_t
{
    unsigned long total_tx;
    unsigned long total_rx;
    unsigned long delta_tx;
    unsigned long delta_rx;

    double rx_usage;
    double tx_usage;
} SA_CPU_NET_T;

/** Core structure for using shared memory                             */
typedef struct _cpu_t
{
    int phy_core;
    double total_usage;
    SA_CPU_SPEC_T spec;
    SA_CPU_STATE_T state;
    SA_CPU_NET_T net;
} SA_CPU_T;


SA_CPU_T *sa_cpu_t;

int sa_get_cpu_spec_info(SA_CPU_SPEC_T *, int);
int sa_get_cpu_state(SA_CPU_STATE_T *, int);
int sa_get_cpu_physical_core_count(void);
int sa_get_cpu_current_core_count(void);
long sa_get_cpu_curr_freq(int);
double sa_get_cpu_linear_curr_freq(int);
int sa_is_cpu_enabled(int);
double sa_get_core_usage(int);
double sa_get_cpu_usage(void);


int sa_get_cpu_bigLITTLE_type(int);
int sa_get_cpu_available_freq(long *, int);

unsigned long sa_get_sys_network_total_rx_bytes();
unsigned long sa_get_sys_network_total_tx_bytes();
unsigned long sa_get_sys_network_delta_rx_bytes();
unsigned long sa_get_sys_network_delta_tx_bytes();


#endif  /*  __SA_MONITOR_SYSTEM_H   */

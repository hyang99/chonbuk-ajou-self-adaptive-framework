#ifndef __SKB_SERVER_H
#define __SKB_SERVER_H

#include <pthread.h>

/** socket PORT                                                         */
#define VISUALIZER_PORT 10000
#define MAX_CONNECTION  30

int create_socket(int);
int read_message(int, char *, int);
int send_message(int, char *, int);
int skb_server_start(pthread_t *);


#endif  /*  __SKB_SERVER_H  */

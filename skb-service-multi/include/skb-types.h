#ifndef __SKB_TYPES_H
#define __SKB_TYPES_H

/** Size of Array(available_freq)                                        */
#define MAX_FREQ_COUNT  20
/** Size of char buffer                                                  */
#define MAX_MSGSIZE 1024


/** Size of Array(process_info)                                          */
#define MAX_PROCESS_COUNT   50
/** Size of Array(app_info)                                              */
#define MAX_APP_COUNT   128
/** SKB will collect data that pid bigger than PROCESS_BOUND             */
#define PROCESS_BOUND   3400
/** Maximum count of Network Interface                                   */
#define MAX_NET_INTERFACE   10
/** Maximu count of Thtread count                                        */
#define MAX_THREAD_COUNT 1024

/** For MQ Interval time                                                 */
#define DEFAULT_MQ_INTERVAL 10000//500000


///--------------------------------------------------------
#define INA231_IOCGREG          _IOR('i', 1, ina231_iocreg_t *)
#define INA231_IOCSSTATUS       _IOW('i', 2, ina231_iocreg_t *)
#define INA231_IOCGSTATUS       _IOR('i', 3, ina231_iocreg_t *)

#define DEV_SENSOR_ARM  "/dev/sensor_arm"
#define DEV_SENSOR_MEM  "/dev/sensor_mem"
#define DEV_SENSOR_KFC  "/dev/sensor_kfc"
#define DEV_SENSOR_G3D  "/dev/sensor_g3d"


enum {
	SENSOR_ARM = 0,
	SENSOR_MEM,
	SENSOR_KFC,
	SENSOR_G3D,
	SENSOR_MAX
};

typedef struct {
	unsigned char name[20];
	unsigned int enable;
	unsigned int cur_uV;
	unsigned int cur_uA;
	unsigned int cur_uW;
} ina231_iocreg_t;

typedef struct {
	int             fd;
	ina231_iocreg_t data;
} sensor_t;
///--------------------------------------------------------

/** process state                                                        */
typedef enum process_state
{
	PROCESS_NONE = 0,               /// None
	PROCESS_RUNNING,                /// R
	PROCESS_INTERRUPT_SLEEP,        /// S
	PROCESS_DISK_SLEEP,             /// D
	PROCESS_TRACED,                 /// T
	PROCESS_PAGING,                 /// W
	PROCESS_ZOMBIE                  /// Z
} PROCESS_STATE;

/** memory info by PID                                                   */
typedef struct _memory_info
{
	int vmpeak;                 /// peak virtual memory
	int vmsize;                 /// current virtual memory
	int vmhwm;                  /// peak resident set size
	int vmrss;                  /// resident set size

	double usage;               /// memory usage byself
	double total_usage;         /// total memory usage
} MEMORY_INFO;


/** network info by PID                                                  */
typedef struct _network_info
{
	char netID[10];
	int nic_count;
	unsigned long total_rx_bytes;
	unsigned long total_tx_bytes;

	unsigned long delta_rx_bytes;
	unsigned long delta_tx_bytes;

	unsigned long rx_bandwidth;
	unsigned long tx_bandwidth;

	unsigned long backlogq_time;
	unsigned long preq_time;
	unsigned long ooo_time;
	unsigned long recvq_time;
	unsigned long latency_time;

	double rx_usage;
	double tx_usage;
	double total_usage;
} NETWORK_INFO;


/** process info from /proc/[pid]/stat                                   */
typedef struct _process_info
{
	int pid;                        /// process id
	int core_id;                    /// execute core
	unsigned int thread_on_proc;    /// thread count on process
	int proc_state;                 /// process state
	int foreground;                 /// foreground
	int parent_process;             /// parent pid
	unsigned int priority;          /// priority of process

	unsigned long u_time;           /// u_time of process
	unsigned long s_time;           /// s_time of process
	unsigned int cu_time;           /// cu_time of process
	unsigned int cs_time;           /// cs_time of process
	unsigned long long start_time; /// start_time of process
	double usage;

	unsigned long user_mode;     /// user_mode time of core (with core_id)
	unsigned long nice_mode;     /// nice_mode time of core (with core_id)
	unsigned long system_mode;   /// system_mode time of core (with core_id)
	unsigned long idle_mode;     /// idle_mode time of core (with core_id)

	long long int timestamp;
	int thread_list[MAX_THREAD_COUNT];

	MEMORY_INFO mem;
	NETWORK_INFO net;
} PROCESS_INFO;



/** core dynamic info from /proc/stat, /sys/devices/                     */
typedef struct _core_dynamic_info
{
	int core_active;                /// active state of core
	long core_cur_freq;             /// current frequence of core
	float normalized_mips;      /// normailized core mips value

	int thread_on_core;             /// thread count on core
	int process_on_core;            /// process count on core

	double core_usage;

	unsigned long user_mode;         /// user_mode time of core
	unsigned long nice_mode;         /// nice_mode time of core
	unsigned long system_mode;       /// system_mode time of core
	unsigned long idle_mode;         /// idle_mode time of core

	unsigned long total_use_tick;    /// total use tick (user + nice + system)
	unsigned long total_idle_tick;   /// total idle tick (idle)

	long long int timestamp;
} CORE_DYNAMIC_INFO;

/** core static info from /proc/stat, /sys/devices/                      */
typedef struct _core_static_info
{
	long core_max_freq;             /// maximum frequence of core
	long core_min_freq;             /// minimum frequence of core

	int core_mips;                  /// performance of core (mips)
	int bigLITTLE;          /// big : 1, LITTLE : 0
	int cluster_master;             /// main of core cluster

	long available_freq[MAX_FREQ_COUNT];  /// available freq. list of core
	int available_freq_count;             /// available freq. count
} CORE_STATIC_INFO;


typedef struct _core_info
{
	int core_id;
	double cpu_total_usage;
	double mem_total_usage;

	int mem_size;
	unsigned long total_jiffies;
	unsigned long work_jiffies;

	unsigned long delta_total;
	unsigned long delta_work;

	sensor_t power_sensor[SENSOR_MAX];
	CORE_STATIC_INFO core_static;
	CORE_DYNAMIC_INFO core_dynamic;
	NETWORK_INFO net_info;
} CORE_INFO;


/** app static info                                                      */
typedef struct _app_static_info
{
	int app_type;                   /// app_type [internal|external]
	double app_max_qos;             /// maximum QOS
	double app_target_qos;          /// target QOS
	double app_min_qos;             /// minimum QOS
	double app_window_size;         /// windown size
	long app_sampling_time;         /// sampling time
	int app_hb_count;               /// heartbeat count
	char app_log_file[MAX_MSGSIZE]; /// logfile name for heartbeat
	long app_buffer_depth;          /// buffer depth
#ifdef MOCO
	int app_alg_total;		    /// total number of algorithm
	int app_add_var;		    /// add var 
#endif

} APP_STATIC_INFO;

/** app dynamic info from MQ's data                                      */
typedef struct _app_dynamic_info
{
	int app_hb_id;                  /// heartbeat id (default = 0)
	long long int app_timestamp;    /// heartbeat timestamp of app
	double app_window_rate;         /// heartbeat window rate
	double app_instant_rate;        /// heartbeat instant rate
	double app_global_rate;         /// heartbeat global rate
} APP_DYNAMIC_INFO;


#ifdef MOCO //FIXME 
typedef struct _app_dynamic_info2
{
	double arg_1;		/// instruction cycle
	double arg_2;
	double arg_3;
	double arg_4;

} APP_DYNAMIC_INFO2;
#endif



typedef struct _app_info
{
	int pid;                        /// pid of application
#ifdef MOCO
	int mq_id[2];
#else
	int mq_id;                      /// message queue id of app
#endif
	char app_name[256];             /// application name
	char app_path[1024];            /// application file path (full)
	long long int timestamp;        /// timestamp on SKB

	APP_STATIC_INFO app_static;
	APP_DYNAMIC_INFO app_dynamic;

#ifdef MOCO // FIXME : data structure for suplementary data
	APP_DYNAMIC_INFO2 app_dynamic2;
#endif

} APP_INFO;


char msq_file[512];
#ifdef MOCO
char msq_file_ext[512];
#endif

CORE_INFO *core_info;
APP_INFO app_info[MAX_APP_COUNT];


PROCESS_INFO process_info[MAX_PROCESS_COUNT];



int max_core_count;         /// [global] system physical core count
int current_core_count;     /// [global] system current core count
int app_count;              /// [global] current application count

int mq_interval;

#endif  /*  __SKB_TYPES_H   */


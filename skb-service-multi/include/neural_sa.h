#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define NeuralPATH 	"./src/hb_1.vgn"
#define maxLayerCount 6

int inputCount;
int outputCount;
int hiddenLayerCount;
int nodeCountInHiddenlayers[maxLayerCount + 1];
double nodeThreshold[maxLayerCount + 1][200];
double nodeValue[maxLayerCount + 1][200];
double nodeTotAct[maxLayerCount + 1][200];
double connectWeightFactor[maxLayerCount + 1][200][200];

char neural_path[512];	// neural_input path 

typedef struct 
{
	
	double hb;
	double size;	

} pred_t;



//pred_t* predict( double _me, double _sub_me, double h_hb, double me, double sub, double size );
double predict( double _me, double _sub_me, double h_hb , double me, double sub);
void openTrainedNF(char file_name[]);
void propagateNetwork();
double computeTransferFunction(double inX);
//pred_t* test( int me, int sub_me, double h_hb, int _me, int _subme, int size );
double test( int me, int sub_me, double h_hb, int _me, int _sub );

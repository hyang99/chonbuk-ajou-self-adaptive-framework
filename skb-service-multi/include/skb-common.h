#ifndef __SKB_COMMON_H
#define __SKB_COMMON_H

#include <getopt.h>
#include <linux/stddef.h>


/** data of SKB - using for logging or analysis                         */
#define HEARTBEAT_FILE  "./log/heartbeat.txt"
#define PROCESS_FILE    "./log/process.txt"
#define CPU_FILE    "./log/cpu.txt"



/**
 * @breif   get timestamp(ns)
 *
 * @return  int64_t timestamp
 *
 */
int64_t get_nano_timestamp();

/**
 * @breif   skb data logging
 *
 * @param   file    log file
 * @param   buf     message
 *
 */
void skb_logging(char *, char *);

/**
 * @breif   getopt for skb
 *
 * @param   argc
 * @param   argv
 *
 */
void skb_getopt(int, char **);

/**
 * @breif   Display app info
 *
 * @return  NULL
 *
 */
void hb_info_print();

/**
 * @breif   Display core info
 *
 * @return  NULL
 *
 */
void core_info_print();

/**
 * @breif   Display process info (Utilization >0)
 *
 * @return  NULL
 *
 */
void process_info_print();




int log_flag;           /// using getopt for log enable
int hb_print_flag;      /// using getopt for heartbeat info display
int core_print_flag;        /// using getopt for core info display
int process_print_flag;     /// using getopt for process info display

#endif  /*  __SKB_COMMON_H    */

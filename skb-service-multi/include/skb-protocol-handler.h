#ifndef __SKB_PROTOCOL_HANDLER_H
#define __SKB_PROTOCOL_HANDLER_H


/** LENGTH INDEX of receive data from socket                             */
#define INDEX_LENG  0
/** TYPE INDEX of receive data from socket                               */
#define INDEX_TYPE  INDEX_LENG+sizeof(int)
/** PAYLOAD INDEX of receive data from socket                            */
#define INDEX_LOAD  INDEX_TYPE+sizeof(unsigned int)
/** count for process_usage list (REQ_PROCESS_STATUS_START)              */
#define USAGE_LIST  5



typedef enum socket_request
{
    REQ_APP_REGISTER = 0x000,
    REQ_APP_CLEAR = 0x001,

    REQ_SESSION_REGISTER = 0x010,
    REQ_SESSION_START = 0x011,      // Create Thread 1
    REQ_SESSION_STOP = 0x012,
    REQ_SESSION_DELETE = 0x013,

    REQ_APPINFO_GET = 0x020,
    REQ_APPINFO_START = 0x021,      // Create Thread 2
    REQ_APPINFO_STOP = 0x022,
    REQ_APPINFO_KILL = 0x023,
    //REQ_APPINFO_RESTART = 0x024,

    REQ_CPUINFO_GET = 0x030,
    REQ_MEMINFO_GET = 0x031,
    REQ_NETINFO_GET = 0x032,
    REQ_COREINFO_GET = 0x033,

    REQ_APP_STATUS_GET = 0x040,

    REQ_APP_FREQ_GET = 0x050,
    REQ_APP_MEM_GET = 0x051,
    REQ_APP_NET_GET = 0x052,
    REQ_APP_POWER_GET = 0x053,
    
    REQ_APP_FORCE_REG = 0x080,
    REQ_APP_FORCE_UNREG = 0x081,
} SOCKET_REQUEST;

typedef enum socket_response
{
    RES_SESSION_APP_DATA = 0x1001,
    RES_SESSION_SYS_DATA = 0x1002,

    RES_PID_APP_DATA = 0x2001,
    RES_PID_SYS_DATA = 0x2002,

    RES_SESSION_REGISTER = 0x100,
    RES_SESSION_START = 0x101,
    RES_SESSION_STOP = 0x102,
    RES_SESSION_DELETE = 0x103,

    RES_APPINFO_GET = 0x200,
    RES_APPINFO_START = 0x201,
    RES_APPINFO_STOP = 0x202,
    RES_APPINFO_KILL = 0x203,
    //RES_APPINFO_RESTART = 0x204,

    RES_CPUINFO_GET = 0x300,
    RES_MEMINFO_GET = 0x301,
    RES_NETINFO_GET = 0x302,
    RES_COREINFO_GET = 0x303,

    RES_APP_STATUS_GET = 0x400,

    RES_APP_FREQ_GET = 0x500,
    RES_APP_MEM_GET = 0x501,
    RES_APP_NET_GET = 0x502,
    RES_APP_POWER_GET = 0x503,

    RES_APP_FORCE_REG = 0x800,
    RES_APP_FORCE_UNREG = 0x801
} SOCKET_RESPONSE;


pthread_t tid_session, tid_app;     /// thread id of each thread
int session_start, app_start;       /// thread start flag


/**
 * @breif   Worker function of socket thread
 *
 * @param   conn_fd      conntion socket fd
 * @param   msg          receive data
 *
 * @return  int          return;
 *
 */
int process_message(int, char *);


/**
 * @breif   payload parsing of receive data
 *
 * @param   total        length of receive data
 * @param   type         type of receive data
 * @param   buf          receive data
 *
 * @return  NULL
 *
 */
void parse_request(int *, unsigned int *, char *);


/**
 * @breif   make send buffer
 *
 * @param   type        type of send data
 * @param   payload     payload of send data
 * @param   buf         send buf
 *
 * @return  int         length of buf
 *
 */
int make_packets(unsigned int, char *, char *);


#endif  /* __SKB_PROTOCOL_HANDLER_H */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
//#include "getprivateprofilestring.h"
#include "neural_sa.h"
#define MAX_LINE_LENGTH 512


double predict( double pme, double psub, double hb , double _me, double _sub_me);
void openTrainedNF(char file_name[]);
void propagateNetwork();
double computeTransferFunction(double inX);
double test( int pme, int psub_me, double h_hb, int me, int sub_me );

	int 
GetPrivateProfileString(const char *section, const char *entry
		, const char *def
		, char *buffer, int buffer_len
		, const char *file_name)
{   
	char buff[MAX_LINE_LENGTH];
	char *ep;
	char t_section[MAX_LINE_LENGTH];
	char *ptr;
	int tlen;
	int len = strlen(entry);

	FILE *fp = fopen(file_name,"r");
	if( !fp ) return(0);

	//printf("FILE OPEN?\n");
	sprintf(t_section,"[%s]",section);    /* Format the section name */
	tlen=strlen(t_section);

	/*  Move through file 1 line at a time until a section is matched or EOF */
	do
	{   if( fgets(buff,MAX_LINE_LENGTH-1,fp) == NULL)
		{   fclose(fp);
			strncpy(buffer,def,buffer_len);     
			return(strlen(buffer));
		}
	}
	while( strncasecmp(buff,t_section,tlen) );

	/* Now that the section has been found, find the entry.
	 * Stop searching upon leaving the section's area. */
	do
	{   
		if ( fgets(buff,MAX_LINE_LENGTH-1,fp) == NULL)
		{   fclose(fp);
			strncpy(buffer,def,buffer_len);     
			return(strlen(buffer));
		}
	}  while( strncasecmp(buff,entry,len) );

	fclose(fp);

	ep = strrchr(buff,'=');    /* Parse out the equal sign */
	if (ep == NULL)
	{
		strncpy(buffer,def,buffer_len);     
		return(strlen(buffer));
	}
	ep++;

	/* remove leading spaces*/
	while (*ep && (isspace(*ep) || *ep == 10))
		ep++;
	if (ep == NULL)
	{
		strncpy(buffer,def,buffer_len);     
		return(strlen(buffer));
	}

	/* remove trailing spaces*/
	ptr = ep;
	while(*ptr) // go to the end, point to a NULL
		ptr++;

	ptr--;
	while (ptr > ep)  // backup and put in nulls if there is a space
	{
		if (isspace(*ptr) || *ep == 10)
		{
			*ptr = 0;
			ptr--;
		}
		else
			break;
	}

	/* Copy up to buffer_len chars to buffer */
	strncpy(buffer,ep,buffer_len - 1);

	buffer[buffer_len] = '\0';


	return(strlen(buffer));
}

//pred_t* test( int pme, int psub_me, double h_hb, int me, int sub_me, int size )
double test( int pme, int psub_me, double h_hb, int me, int sub_me )
{
	double _me = (double)pme/5;
	double _sub_me = (double)psub_me/7.5;
	//	double hb = (double)h_hb / 20;
	double hb = (double)h_hb / 5;
	double h_me = (double)me / 5;
	double h_sub = (double)sub_me / 7.5;

	return predict( _me, _sub_me, hb, h_me, h_sub );

}

//pred_t* predict( double pme, double psub, double hb, double _me, double _sub_me, double size )
double predict( double pme, double psub, double hb , double _me, double _sub_me)
{
	nodeValue[0][0] = pme;
	nodeValue[0][1] = psub;
	nodeValue[0][2] = hb;
	nodeValue[0][3] = _me;
	nodeValue[0][4] = _sub_me;

	propagateNetwork();

	double new_hb = nodeValue[hiddenLayerCount + 2][0]*5;



	return new_hb;

}


double computeTransferFunction(double inX)
{

	double ret;
	ret = tanh(inX);
	return ret;
}


void propagateNetwork()
{

	int w_n,cur_Layer,w_n_1;
	for ( w_n = 0; w_n < nodeCountInHiddenlayers[1]; w_n++) {
		nodeValue[1][w_n]  
			= computeTransferFunction(nodeValue[0][w_n]
					+ nodeThreshold[1][w_n]);


	}

	double current_Sum;
	for ( cur_Layer = 2; cur_Layer <= (hiddenLayerCount + 2); cur_Layer++) {
		for ( w_n = 0; w_n < nodeCountInHiddenlayers[cur_Layer]; w_n++) {
			current_Sum = 0;
			for ( w_n_1 = 0; w_n_1 < nodeCountInHiddenlayers[cur_Layer - 1]; w_n_1++) {
				current_Sum = current_Sum
					+ connectWeightFactor[cur_Layer][w_n_1][w_n]
					* nodeValue[cur_Layer - 1][w_n_1];
			}
			current_Sum = current_Sum + nodeThreshold[cur_Layer][w_n];
			nodeValue[cur_Layer][w_n]
				= computeTransferFunction(current_Sum);

			nodeTotAct[cur_Layer][w_n] = current_Sum;

		}
	}

}


void openTrainedNF(char file_name[])
{


	char buf[3];


	/* Topology */
	GetPrivateProfileString("Topology","Total input","no val",buf,sizeof(buf),neural_path);

	inputCount= atoi(buf);

	//	printf("input_num : %d\n",inputCount);

	GetPrivateProfileString("Topology","Total output","no val",buf,sizeof(buf),neural_path);
	outputCount= atoi(buf);
	//	printf("output_num : %d\n",outputCount);


	GetPrivateProfileString("Topology","Total layer","no val",buf,sizeof(buf),neural_path);
	hiddenLayerCount= atoi(buf) -2;
	//	printf("hidden_layer_num : %d\n",hiddenLayerCount);


	/* Total node */

	GetPrivateProfileString("Total node","02","no val",buf,sizeof(buf),neural_path);
	nodeCountInHiddenlayers[2]= atoi(buf);
	//	printf("layer1_num : %d\n",nodeCountInHiddenlayers[2]);

	GetPrivateProfileString("Total node","03","no val",buf,sizeof(buf),neural_path);
	nodeCountInHiddenlayers[3]= atoi(buf);
	//	printf("layer2_num : %d\n",nodeCountInHiddenlayers[3]);


	//	GetPrivateProfileString("Total node","04","no val",buf,sizeof(buf),"/home/odroid/Desktop/internal/skb/skb-service/src/hb_1.vgn");
	//	nodeCountInHiddenlayers[4]= atoi(buf);
	//	printf("layer3_num : %d\n",nodeCountInHiddenlayers[4]);

	//	GetPrivateProfileString("Total node","05","no val",buf,sizeof(buf),"/home/odroid/Desktop/foo/skb/skb-service/src/hb_1.vgn");
	//	nodeCountInHiddenlayers[5]= atoi(buf);
	//	printf("layer4_num : %d\n",nodeCountInHiddenlayers[5]);
	/*
	   GetPrivateProfileString("Total node","06","no val",buf,sizeof(buf),"./neural_hb_new.vgn");
	   nodeCountInHiddenlayers[6]= atoi(buf);
	   printf("layer5_num : %d\n",nodeCountInHiddenlayers[6]);
	 */
	/* layer-node = threshold value */

	FILE* file;
	file = fopen(file_name,"r");
	char line[120];
	char curl[50];
	char node_1[50];
	char node_2[50];
	char threshold[50];
	int curLine = 0;

	do{

		fgets(line,100,file);

		if( strstr(line,"layer-node=threshold value") != NULL)
			break;

	}while(1);




	do{
		fscanf(file,"%2s-%2s= %s",curl,node_1,threshold);
		//	 printf("%s-%s= %s\n",curl,node,threshold);
		int layer_i = atoi(curl);
		int node_i = atoi(node_1);
		double thres = atof(threshold);
		nodeThreshold[layer_i][node_i -1] = thres;
		//			 printf("%d: %d-%d= %.15f\n",curLine,layer_i,node_i,thres);
		curLine++;	 		 

	}while( curLine != 16);



	do{
		fgets(line,100,file);

		if( strstr(line,
					"layer-node(layer n-1)-node(layer n)=weight factor") != NULL)
			break;

	}while(1);

	curLine = 0;
	do{
		fscanf(file,"%2s-%2s-%2s= %s",curl,node_1,node_2,threshold);
		//			 printf("%s-%s-%s= %s\n",curl,node_1,node_2,threshold);
		int layer_i = atoi(curl);
		int node_i = atoi(node_1);
		int node_j = atoi(node_2);
		double thres = atof(threshold);
		connectWeightFactor[layer_i][node_i -1][node_j -1] = thres;
		//			 printf("%d :%d-%d-%d= %.15f\n",curLine,layer_i,node_i,node_j,thres);
		curLine++;	 		 

	}while( curLine != 58);

	nodeCountInHiddenlayers[0] = inputCount;
	nodeCountInHiddenlayers[1] = inputCount;
	nodeCountInHiddenlayers[hiddenLayerCount + 2] = outputCount;

}


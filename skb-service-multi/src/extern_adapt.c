#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <string.h>
#define FAST_ADAPTATION 1
#include "extern-adapt.h"

int l_core=1, b_core=0;
int l_freq_level=0;
int Frequency_table[30]={400000, 500000, 600000, 700000, 800000, 900000, 1000000, 1100000, 1200000, 1300000, 1400000, 1500000, 1600000, 1700000, 18000000, 1900000, 2000000};

void external_actuator(int clk_cnt)
{
	int ret;
	printf(" =====================================================\n");	

	printf("\n Little core Frequency\n");	
	snprintf(buffer, 100, "sudo sh -c \"echo %d > /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq\"", Frequency_table[clk_cnt]);
	printf(" work with [  %d   ] MHz \n\n", Frequency_table[clk_cnt]/1000);
	//                puts(buffer);
	ret = system(buffer);
/*
	printf(" BIG core Frequency\n");	
	snprintf(buffer, 100, "sudo sh -c \"echo %d > /sys/devices/system/cpu/cpu4/cpufreq/scaling_max_freq\"", Frequency_table[clk_cnt]);
	//               puts(buffer);
	ret = system(buffer);
*/

	if( ret < 0 )
		printf("Error : external_actuator()\n");

}

int self_adaptation(double instant_hb, double target, double time_p, uint64_t l_inst, uint64_t b_inst, int frame)
{
	int mode=0, flag=-1;
  double temp_heartbeat=0;

/*
  printf("========== frame = %d\n", frame);
  printf("========== target = %f\n", target);
  printf("========== hb = %f\n", instant_hb);
  printf("========== target+THRESHOLD = %f\n", target+THRESHOLD);
*/
	if(instant_hb > target) {
		if(instant_hb > target+THRESHOLD) {

			printf("\nOverflow heartbeat!!\n");
			flag=1;

#if FAST_ADAPTATION                       
			while(1)
			{
				mode=Controller(time_p, l_inst, b_inst, flag); // core up? frequency up?

downback:
				if(mode==0) //frequency
				{

					printf("-------------------- down frequency -----------------\n");
					l_freq_level--;
					temp_heartbeat=instant_hb;
					instant_hb*=(double)Frequency_table[l_freq_level]/Frequency_table[l_freq_level+1];
					if(instant_hb < target)
					{
						l_freq_level++;
						instant_hb=temp_heartbeat;
						break;
					}
					time_p*=(double)Frequency_table[l_freq_level+1]/Frequency_table[l_freq_level];
				}

				else if(mode==1)
				{
					printf("-------------------- down core -----------------\n");
					l_core--;
					temp_heartbeat= instant_hb;
          
					instant_hb=instant_hb*l_core/(l_core+1);

					if(instant_hb < target)
					{
						l_core++;
						instant_hb=temp_heartbeat;
						if(l_freq_level>0)
						{
							mode=0;
							goto downback;
						}
						else mode=-1;
					}
					time_p=(double)(l_core+1)/l_core*time_p;
				}
				else break;

				if(instant_hb<=target*THRESHOLD)
				{
					//      printf(">>>>>>>>>>>>>>>>  expected heartbeat = %f\n", instant_hb);
					break;
				}


				printf("predicted heartbeat_val = %f\n", instant_hb);

				if(l_freq_level>CLOCK_NUM && l_core>CORE_NUM || mode==-1) break;
			}

#else                         

			//change s
			mode=Controller(time_p, l_inst, b_inst, flag); // core up? frequency up?

			if(mode==0) l_freq_level--;
			else if(mode==1) l_core--;
			//change e
#endif
		}
		else {
			printf("\n Enough heartbeat!! \n")    ;
			flag=-1;
		}
	}
	else {
		flag=0;
		printf("\n Insufficient heartbeat!! \n");

#if FAST_ADAPTATION
		while(1)
		{
			mode=Controller(time_p, l_inst, b_inst, flag); // core up? frequency up?

upback:
			if(mode==0) //frequency
			{
				printf("-------------------- up frequency -----------------\n");
				l_freq_level++;
				time_p*=(double)Frequency_table[l_freq_level-1]/Frequency_table[l_freq_level];
				instant_hb*=(double)Frequency_table[l_freq_level]/Frequency_table[l_freq_level-1];
			}

			else if(mode==1)
			{
				printf("-------------------- up core -----------------\n");
				l_core++;
				temp_heartbeat=instant_hb;
				instant_hb=instant_hb*l_core/(l_core-1);

        printf("instant_hb = %f\n", instant_hb);

				if(instant_hb > target+THRESHOLD)
				{
					l_core--;
					instant_hb=temp_heartbeat;
					mode=0;
					goto upback;
				}

				time_p=(double)(l_core-1)/l_core*time_p;
			}
			else break;

			if(instant_hb>target)
			{
				//               printf(">>>>>>>>>>>>>>>>  predicted heartbeat = %f\n", instant_hb);
				break;
			}

			printf("predicted heartbeat_val = %f\n", instant_hb);
			if(l_freq_level>CLOCK_NUM && l_core>CORE_NUM) break;

		}
#else

		// change s
		mode=Controller(time_p, l_inst, b_inst, flag); // core up? frequency up?

		if(mode==0) l_freq_level++;
		else if(mode==1) l_core++;

		// change e

#endif
	}

	if( l_core<1 ) {
		printf("[Core abnormaly detected !!]\n");
			l_core = 1;
	}

	external_actuator( l_freq_level ); //FIXME : Position of actuator diffrent from original code
	return l_core;


}

double set_little_power(int l_core, int l_freq)
{
        double l_power;

        l_power=l_core*0.046+l_freq/100000*0.035-0.201;
        if(l_power<0) l_power=0.04;

       return l_power;
}

double set_big_power(int b_core, int b_freq)
{
        double b_power;

        b_power=b_core*0.132+b_freq/100000*0.078-0.393;
        if(b_power<0) b_power=0.2;

        return b_power;
}

int Controller(double time_m, uint64_t l_inst, uint64_t b_inst, int flag) {

        double l_time_up, b_time_up, time_c, l_time, b_time, time_total_cal;
        double time_m_cal, time_m_delay;
        double E_lf, E_lc, E_bc, EFF_lf=-1.0, EFF_lc=-1.0, EFF_bc=-1.0;
        double a0, a1, cur_l_power, cur_b_power, next_l_power, next_b_power;
        int mode;
        int l_freq, b_freq;
        double max_mode;

        l_freq=Frequency_table[l_freq_level];
        b_freq=2000000;
  
        mode=-1;
        if(l_inst==0) l_time=0.0;
        else l_time = l_inst/l_freq*1.0/1000 / ISSUE_WIDTH / l_core;
        if(b_inst==0) b_time=0.0;
        else b_time = b_inst/b_freq*1.0/1000 / ISSUE_WIDTH / b_core;

        time_total_cal= l_time + b_time;

        printf("time_total = %f\n", time_total_cal);

        cur_l_power=set_little_power(l_core, l_freq);
        cur_b_power=set_big_power(b_core, b_freq);

        next_l_power=cur_l_power;
        next_b_power=cur_b_power;

        l_freq=Frequency_table[l_freq_level];

        // case of changing a clock frequency of l_core
        if(flag==0 && l_freq_level<CLOCK_NUM-1)
        {
                next_l_power = set_little_power(l_core, Frequency_table[l_freq_level+1]);

                l_time_up = l_time * Frequency_table[l_freq_level]/Frequency_table[l_freq_level+1];

                b_time_up = b_time;

                time_c=time_total_cal-(l_time_up+b_time_up);

                E_lf = (l_time * cur_l_power + b_time * cur_b_power) -
                        (l_time_up * next_l_power + b_time_up * next_b_power);

                E_lf+=10;
  
                EFF_lf =(double)time_c / E_lf;
        }
        else if(flag==1 && l_freq_level>0) {
                next_l_power=set_little_power(l_core, Frequency_table[l_freq_level-1]);

                l_time_up = l_time*Frequency_table[l_freq_level]/Frequency_table[l_freq_level-1];
                b_time_up = b_time;

                time_c=(l_time_up+b_time_up)-time_total_cal;

                E_lf = (l_time_up * next_l_power + b_time_up * next_b_power) -
                        (l_time * cur_l_power + b_time * cur_b_power);

                E_lf+=10;
                EFF_lf =(double)time_c / E_lf;
        }

        // case of changing number of big cores
        if(flag==0 && b_core<CORE_NUM){
                next_b_power=set_big_power(b_core+1, b_freq);

                l_time_up = l_time;
                b_time_up = b_time*b_core/(b_core+1);

                time_c=time_total_cal-(l_time_up+b_time_up);

                E_bc = (l_time * cur_l_power + b_time * cur_b_power) -
                        (l_time_up * next_l_power + b_time_up * next_b_power);

                E_bc+=10;
                EFF_bc =(double)time_c / E_bc;

        }

        else if(flag==1 && b_core>1) { /////////////// being changed
                next_b_power=set_big_power(b_core-1, b_freq);

                l_time_up = l_time;
                b_time_up = b_time*b_core/(b_core-1)*1.0;

                time_c = (l_time_up+b_time_up)-time_total_cal;

                E_bc = (l_time_up * next_l_power + b_time_up * next_b_power) -
                        (l_time * cur_l_power + b_time * cur_b_power);

                E_bc+=10;
                EFF_bc = (double)time_c / E_bc;
        }
        // case of changing number of little cores
        if(flag==0 && l_core<CORE_NUM){
                next_l_power=set_little_power(l_core+1, l_freq);

                l_time_up = l_time*l_core/(l_core+1);
                b_time_up = b_time;

                time_c=time_total_cal-(l_time_up+b_time_up);

                E_lc = (l_time * cur_l_power + b_time * cur_b_power) -
                        (l_time_up * next_l_power + b_time_up * next_b_power);

                E_lc+=10;
                EFF_lc =(double)time_c / E_lc;
/*
                printf("next_l_power = %f\n", next_l_power);
                printf("l_time_up = %f\n", l_time_up);
                printf("b_time_up = %f\n", b_time_up);
                printf("time_c = %f\n", time_c);
                printf("E_lc = %f\n", E_lc);
*/
        }

        else if(flag==1 && l_core>1) { /////////////// being changed
                next_l_power=set_little_power(l_core-1, l_freq);

                l_time_up = l_time*l_core/(l_core-1)*1.0;
                b_time_up = b_time;

                time_c = (l_time_up+b_time_up)-time_total_cal;

                E_lc = (l_time_up * next_l_power + b_time_up * next_b_power) -
                        (l_time * cur_l_power + b_time * cur_b_power);

                E_lc+=10;
                EFF_lc = (double)time_c / E_lc;
        }
/*
        printf("EFF_lf = %f\n", EFF_lf);
        printf("EFF_lc = %f\n", EFF_lc);
        printf("EFF_bc = %f\n", EFF_bc);
*/
        if(EFF_lc<EFF_lf && EFF_bc<EFF_lf)
                max_mode=EFF_lf;
        else if(EFF_lf<EFF_lc && EFF_bc<EFF_lc)
                max_mode=EFF_lc;
        else max_mode=EFF_bc;

        if(max_mode==-1) return -1;

        if(max_mode == EFF_lf)
                return 0;
        else if(max_mode == EFF_lc)
                return 1;
        else return 2;
}


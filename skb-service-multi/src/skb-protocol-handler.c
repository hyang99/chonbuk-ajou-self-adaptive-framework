#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>

#include "skb-types.h"
#include "skb-proc.h"
#include "skb-server.h"
#include "skb-mq.h"
#include "skb-protocol-handler.h"
#include "skb-app.h"
#include "skb-common.h"


int pid_list[MAX_APP_COUNT];
static int app_list[MAX_APP_COUNT] = {0};
int pid_count;
int req_app_count;
int req_process_count;

int64_t clock_ts;

int cmp_by_usage(const void *a, const void *b)
{
    PROCESS_INFO *ia = (PROCESS_INFO *)a;
    PROCESS_INFO *ib = (PROCESS_INFO *)b;

    if(ia->usage > ib->usage)
        return -1;
    else if(ia->usage == ib->usage)
        return 0;
    else
        return 1;
}



void parse_request(int *total, unsigned int *type, char *buf)
{
    char *ptr = NULL;
    char temp_session[MAX_MSGSIZE];

    memset(pid_list, 0x00, sizeof(pid_list));
    pid_count = 0;

    memcpy(total, buf+INDEX_LENG, sizeof(int));
    memcpy(type, buf+INDEX_TYPE, sizeof(unsigned int));

    if((*total > (sizeof(int)+sizeof(unsigned int))) && (&buf[INDEX_LOAD] != NULL))
    {
        if(*type != REQ_SESSION_REGISTER)
        {
            ptr = strtok(buf+INDEX_LOAD, "|");
            pid_list[pid_count] = atoi(ptr);

            pid_count++;
            while((ptr = strtok(NULL, "|")))
            {
                pid_list[pid_count] = atoi(ptr);
                pid_count++;
            }
        }
        else
        {
            memcpy(temp_session, buf+INDEX_LOAD, MAX_MSGSIZE);
        }
    }

    free(ptr);
}


int make_packets(unsigned int type, char *payload, char *buf)
{
    int length = sizeof(unsigned int) + sizeof(int) + strlen(payload);

    memcpy(&buf[INDEX_LENG], &length, sizeof(int));
    memcpy(&buf[INDEX_TYPE], &type, sizeof(unsigned int));
    memcpy(&buf[INDEX_LOAD], payload, strlen(payload));

    return length;
}


/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
/////////////////      Create Response Messages         /////////////////
/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////
// Req : 0x000, Res : 0x900
void make_response_app_register(int conn_fd, int pid, char *buf)
{
    char payload[MAX_MSGSIZE];
    char s_buf[MAX_MSGSIZE];
    int length;
    unsigned int response_type;
#ifdef MOCO
    int* mq_id;
    App_ID = mQ_cnt;
#else
    int mq_id;
#endif

    mq_id = mq_init(pid);

    putmQ(pid, mq_id);
#ifdef DEBUG
    printf(" < make_response_app_register > pid : %d\n", pid);
#endif


    memset(s_buf, 0x00, sizeof(s_buf));
    memset(payload, 0x00, sizeof(payload));

    collect_app_static_info(buf, mq_id, SA_OK);
    collect_process_info();

#ifdef MOCO
    sprintf(payload, "%d|%d|%d\r\n", pid, mq_id[0], mq_id[1]);
#else
    sprintf(payload, "%d|%d\r\n", pid, mq_id);
#endif

    response_type = 0x900;

    length = make_packets(response_type, payload, s_buf);
    send_message(conn_fd, s_buf, length);

#ifdef DEBUG
    printf(" < make_response_app_register > \n");

#ifdef MOCO
    printf(" created  mq_id(APP2SERVER) : %d\n", mq_id[0]);
    printf(" created  mq_id(SERVER2APP) : %d\n", mq_id[1]);
#else
    printf(" created  mq_id : %d\n", mq_id);    
#endif

    printf("----------------------------------\n");
#endif
}





/////////////////////////////////////////////////////////////////////////
// Req : 0x010, Res : 0x100
int make_session_register(int conn_fd)
{
    char payload[MAX_MSGSIZE];
    char s_buf[MAX_MSGSIZE];
    int length, ret;
    unsigned int response_type;


    memset(s_buf, 0x00, sizeof(s_buf));
    memset(payload, 0x00, sizeof(payload));

    response_type = RES_SESSION_REGISTER;

    length = make_packets(response_type, payload, s_buf);
    ret = send_message(conn_fd, s_buf, length);

#ifdef DEBUG
    printf(" < make_session_register > \n");
    printf("----------------------------------\n");
#endif

    return ret;
}


/////////////////////////////////////////////////////////////////////////
// Req : 0x011, Res : 0x1001
int make_app_info(int conn_fd)
{
    int i;
    unsigned int response_type;
    char payload[MAX_MSGSIZE];
    char tmp[MAX_MSGSIZE];
    char s_buf[MAX_MSGSIZE];
    int length;
    int ret;


    memset(s_buf, 0x00, sizeof(s_buf));
    memset(tmp, 0x00, sizeof(tmp));
    memset(payload, 0x00, sizeof(payload));

    for(i=0 ; i<MAX_PROCESS_COUNT ; i++)
    {
        if(app_info[i].pid > 0)
        {
            memset(tmp, 0x00, sizeof(tmp));

            sprintf(tmp, "%d|%s|%d|%d|%f|%f|%f|%f|%ld|%d\r\n",
                    app_info[i].pid,
                    app_info[i].app_name,
#ifdef MOCO
                    app_info[i].mq_id[0],
#else
                    app_info[i].mq_id,
#endif
                    app_info[i].app_static.app_type,
                    app_info[i].app_static.app_max_qos,
                    app_info[i].app_static.app_target_qos,
                    app_info[i].app_static.app_min_qos,
                    app_info[i].app_static.app_window_size,
                    app_info[i].app_static.app_sampling_time,
                    app_info[i].app_static.app_hb_count);

            strcat(payload, tmp);
        }
    }

    //printf(" >> mq_id : %d\n", app_info[i].mq_id);

    response_type = RES_SESSION_APP_DATA;

    length = make_packets(response_type, payload, s_buf);
    ret = send_message(conn_fd, s_buf, length);

#ifdef DEBUG
    printf(" < make_app_info > \n");
    printf("%s\n", payload);
    printf("----------------------------------\n");
#endif

    return ret;
}




/////////////////////////////////////////////////////////////////////////
// Req : 0x011, Res : 0x1002
int make_syswide_info(int conn_fd)
{
    int i;
    unsigned int response_type;
    char payload[MAX_MSGSIZE];
    char tmp[MAX_MSGSIZE];
    char s_buf[MAX_MSGSIZE];
    int length;
    int ret;

    memset(s_buf, 0x00, sizeof(s_buf));
    memset(tmp, 0x00, sizeof(tmp));
    memset(payload, 0x00, sizeof(payload));

    sprintf(payload, "%lld|%f|%f|%lu|%lu|",
            clock_ts,
            core_info[0].cpu_total_usage,
            core_info[0].mem_total_usage,
            core_info[0].net_info.delta_tx_bytes,
            core_info[0].net_info.delta_rx_bytes);

    for(i=0 ; i<max_core_count ; i++)
    {
        memset(tmp, 0x00, sizeof(tmp));
        if(core_info[i].core_dynamic.core_active == 1)
        {
            sprintf(tmp, "%f", core_info[i].core_dynamic.core_usage);
        }
        else
        {
            sprintf(tmp, "%f", (double)0);
        }
        strcat(payload, tmp);

        if(i != max_core_count-1)
        {
            strcat(payload, "|");
        }
    }

    response_type = RES_SESSION_SYS_DATA;

    length = make_packets(response_type, payload, s_buf);
    ret = send_message(conn_fd, s_buf, length);


#ifdef DEBUG
    printf(" < make_syswide_info > \n");
    printf("%s\n", payload);
    printf("----------------------------------\n");
#endif

    return ret;
}


/////////////////////////////////////////////////////////////////////////
// Req : 0x011, RES : 0x101
int make_session_start_response(int conn_fd)
{
    unsigned int response_type;
    char payload[MAX_MSGSIZE];
    char s_buf[MAX_MSGSIZE];
    int length;
    int ret;

    memset(s_buf, 0x00, sizeof(s_buf));
    memset(payload, 0x00, sizeof(payload));

    response_type = RES_SESSION_START;

    length = make_packets(response_type, payload, s_buf);
    ret = send_message(conn_fd, s_buf, length);

#ifdef DEBUG
    printf(" < processing_session_start > \n");
    //printf("%s\n", payload);
    printf("----------------------------------\n");
#endif

    return ret;
}



/////////////////////////////////////////////////////////////////////////
// Req : 0x012, RES : 0x102
int make_session_stop(int conn_fd)
{
    unsigned int response_type;
    char payload[MAX_MSGSIZE];
    char s_buf[MAX_MSGSIZE];
    int length;
    int ret;

    memset(s_buf, 0x00, sizeof(s_buf));
    memset(payload, 0x00, sizeof(payload));

    session_start = 0;
    // 20150810
    pthread_cancel(tid_session);
    pthread_join(tid_session, NULL);

    response_type = RES_SESSION_STOP;

    length = make_packets(response_type, payload, s_buf);
    ret = send_message(conn_fd, s_buf, length);

#ifdef DEBUG
    printf(" < processing_session_stop > \n");
    //printf("%s\n", payload);
    printf("----------------------------------\n");
#endif

    return ret;
}


/////////////////////////////////////////////////////////////////////////
// Req : 0x020, RES : 0x200
int make_appinfo_get(int conn_fd)
{
    unsigned int response_type;
    char payload[MAX_MSGSIZE];
    char s_buf[MAX_MSGSIZE];
    int length;
    int ret;

    memset(s_buf, 0x00, sizeof(s_buf));
    memset(payload, 0x00, sizeof(payload));

    response_type = RES_APPINFO_GET;

    length = make_packets(response_type, payload, s_buf);
    ret = send_message(conn_fd, s_buf, length);

#ifdef DEBUG
    printf(" < make_appinfo_get > \n");
    printf("----------------------------------\n");
#endif

    return ret;
}




/////////////////////////////////////////////////////////////////////////
// Req : 0x021, Res : 0x2001
int make_app_qos_info(int conn_fd)
{
    int i, j;
    unsigned int response_type;
    char payload[MAX_MSGSIZE];
    char tmp[MAX_MSGSIZE];
    char s_buf[MAX_MSGSIZE];
    int length;
    int ret;


    memset(s_buf, 0x00, sizeof(s_buf));
    memset(tmp, 0x00, sizeof(tmp));
    memset(payload, 0x00, sizeof(payload));


    for(i=0 ; i<MAX_APP_COUNT ; i++)
    {
        for(j=0 ; j<MAX_APP_COUNT ; j++)
        {
            if((app_list[i] == app_info[j].pid) && (app_list[i] > 0))
            {
                memset(tmp, 0x00, sizeof(tmp));

                sprintf(tmp, "%d|%d|%lld|%f|%f\r\n",
                        app_info[j].pid,
                        app_info[j].app_dynamic.app_hb_id,
                        clock_ts,
                        app_info[j].app_dynamic.app_window_rate,
                        app_info[j].app_dynamic.app_instant_rate);

                strcat(payload, tmp);
            }
        }
    }

    response_type = RES_PID_APP_DATA;

    length = make_packets(response_type, payload, s_buf);
    ret = send_message(conn_fd, s_buf, length);

#ifdef DEBUG
    printf(" < make_app_qos_info > \n");
    printf("%s\n", payload);
    printf("----------------------------------\n");
#endif

    return ret;
}



/////////////////////////////////////////////////////////////////////////
// Req : 0x021, Res : 0x2002
int make_app_sys_info(int conn_fd)
{
    int i, j, k, l;
    unsigned int response_type;
    char payload[MAX_MSGSIZE];
    char tmp[MAX_MSGSIZE];
    char s_buf[MAX_MSGSIZE];
    int length;
    int ret;
    int cid = -1, count = -1;
    double mem_usage;
    unsigned long ur, ut;

    long core_freq[128] = {0x00};


    memset(s_buf, 0x00, sizeof(s_buf));
    memset(tmp, 0x00, sizeof(tmp));
    memset(payload, 0x00, sizeof(payload));


    for(i=0 ; i<MAX_APP_COUNT ; i++)
    {
        if(app_list[i] > 0)
        {
            for(j=0 ; j<MAX_PROCESS_COUNT ; j++)
            {
                if(app_list[i] == process_info[j].pid)
                {
                    memset(core_freq, 0x00, sizeof(core_freq));
                    cid = -1;

                    cid = process_info[j].core_id;
                    core_freq[cid] = core_info[cid].core_dynamic.core_cur_freq;

                    mem_usage = process_info[j].mem.usage;
                    ur = core_info[cid].net_info.delta_rx_bytes;
                    ut = core_info[cid].net_info.delta_tx_bytes;

                    for(l=0 ; l<MAX_APP_COUNT ; l++)
                    {
                        if(process_info[j].pid == app_info[l].pid)
                        {
                            count = 0;
                            count = app_info[l].app_dynamic.app_hb_id;
                            break;
                        }
                    }


                    sprintf(tmp, "%d|%d|%lld|", process_info[j].pid, count, clock_ts);
                    strcat(payload, tmp);

                    //for(k=0 ; k<core_count ; k++)
                    for(k=0 ; k<max_core_count ; k++)
                    {
                        memset(tmp, 0x00, sizeof(tmp));
                        sprintf(tmp, "%ld|", core_freq[k]);
                        strcat(payload, tmp);
                    }

                    memset(tmp, 0x00, sizeof(tmp));
                    sprintf(tmp, "%f|%lu|%lu\r\n", mem_usage, ur, ut);
                    strcat(payload, tmp);
                }
            }
        }
    }

    response_type = RES_PID_SYS_DATA;

    //printf(" make_app_sys_info > \n %s\n\n", payload);

    length = make_packets(response_type, payload, s_buf);
    ret = send_message(conn_fd, s_buf, length);

#ifdef DEBUG
    printf(" < make_app_sys_info > \n");
    printf("%s\n", payload);
    printf("----------------------------------\n");
#endif

    return ret;
}


/////////////////////////////////////////////////////////////////////////
// Req : 0x021, RES : 0x201
int make_app_start_response(int conn_fd)
{
    unsigned int response_type;
    char payload[MAX_MSGSIZE];
    char s_buf[MAX_MSGSIZE];
    int length;
    int ret, i;

    memset(s_buf, 0x00, sizeof(s_buf));
    memset(payload, 0x00, sizeof(payload));

    response_type = RES_APPINFO_START;

    length = make_packets(response_type, payload, s_buf);
    ret = send_message(conn_fd, s_buf, length);

    if(ret <= 0)
    {
        req_app_count = 0;
        for(i=0 ; i<MAX_APP_COUNT ; i++)
        {
            app_list[i] = 0;
        }
    }

#ifdef DEBUG
    printf(" < make_app_start_response > \n");
    //printf("%s\n", payload);
    printf("----------------------------------\n");
#endif

    return ret;
}





/////////////////////////////////////////////////////////////////////////
// Req : 0x022, RES : 0x202
int make_app_stop(int conn_fd)
{
    unsigned int response_type;
    char payload[MAX_MSGSIZE];
    char s_buf[MAX_MSGSIZE];
    int length;
    int ret;

    memset(s_buf, 0x00, sizeof(s_buf));
    memset(payload, 0x00, sizeof(payload));

    app_start = 0;
    // 20150810
    pthread_cancel(tid_app);
    pthread_join(tid_app, NULL);

    response_type = RES_APPINFO_STOP;

    length = make_packets(response_type, payload, s_buf);
    ret = send_message(conn_fd, s_buf, length);

#ifdef DEBUG
    printf(" < make_app_stop > \n");
    //printf("%s\n", payload);
    printf("----------------------------------\n");
#endif

    return ret;
}


/////////////////////////////////////////////////////////////////////////
// Req : 0x023, RES : 0x203
int make_appinfo_kill(int conn_fd)
{
    unsigned int response_type;
    char payload[MAX_MSGSIZE];
    char s_buf[MAX_MSGSIZE];
    int length;
    int ret;

    memset(s_buf, 0x00, sizeof(s_buf));
    memset(payload, 0x00, sizeof(payload));

    response_type = RES_APPINFO_KILL;

    length = make_packets(response_type, payload, s_buf);
    ret = send_message(conn_fd, s_buf, length);

#ifdef DEBUG
    printf(" < make_appinfo_kill > \n");
    printf("----------------------------------\n");
#endif

    return ret;
}



/////////////////////////////////////////////////////////////////////////
// Req : 0x030, RES : 0x300
int make_cpuinfo_get(int conn_fd)
{
    unsigned int response_type;
    char payload[MAX_MSGSIZE];
    char s_buf[MAX_MSGSIZE];
    int length;
    int ret;

    memset(s_buf, 0x00, sizeof(s_buf));
    memset(payload, 0x00, sizeof(payload));

    response_type = RES_CPUINFO_GET;

    length = make_packets(response_type, payload, s_buf);
    ret = send_message(conn_fd, s_buf, length);

#ifdef DEBUG
    printf(" < make_cpuinfo_get > \n");
    printf("----------------------------------\n");
#endif

    return ret;
}


/////////////////////////////////////////////////////////////////////////
// Req : 0x031, RES : 0x301
int make_meminfo_get(int conn_fd)
{
    unsigned int response_type;
    char payload[MAX_MSGSIZE];
    char s_buf[MAX_MSGSIZE];
    int length;
    int ret;

    memset(s_buf, 0x00, sizeof(s_buf));
    memset(payload, 0x00, sizeof(payload));

    response_type = RES_MEMINFO_GET;

    length = make_packets(response_type, payload, s_buf);
    ret = send_message(conn_fd, s_buf, length);

#ifdef DEBUG
    printf(" < make_meminfo_get > \n");
    printf("----------------------------------\n");
#endif

    return ret;
}


/////////////////////////////////////////////////////////////////////////
// Req : 0x032, RES : 0x302
int make_netinfo_get(int conn_fd)
{
    unsigned int response_type;
    char payload[MAX_MSGSIZE];
    char s_buf[MAX_MSGSIZE];
    int length;
    int ret;

    memset(s_buf, 0x00, sizeof(s_buf));
    memset(payload, 0x00, sizeof(payload));

    response_type = RES_NETINFO_GET;

    length = make_packets(response_type, payload, s_buf);
    ret = send_message(conn_fd, s_buf, length);

#ifdef DEBUG
    printf(" < make_netinfo_get > \n");
    printf("----------------------------------\n");
#endif

    return ret;
}



/////////////////////////////////////////////////////////////////////////
// Req : 0x033, RES : 0x303
int make_coreinfo_get(int conn_fd)
{
    unsigned int response_type;
    char payload[MAX_MSGSIZE];
    char s_buf[MAX_MSGSIZE];
    int length;
    int ret;

    memset(s_buf, 0x00, sizeof(s_buf));
    memset(payload, 0x00, sizeof(payload));

    response_type = RES_COREINFO_GET;

    length = make_packets(response_type, payload, s_buf);
    ret = send_message(conn_fd, s_buf, length);

#ifdef DEBUG
    printf(" < make_coreinfo_get > \n");
    printf("----------------------------------\n");
#endif

    return ret;
}



/////////////////////////////////////////////////////////////////////////
// Req : 0x050, RES : 0x500
int make_app_freq_get(int conn_fd)
{
    unsigned int response_type;
    char payload[MAX_MSGSIZE];
    char s_buf[MAX_MSGSIZE];
    int length;
    int ret;

    memset(s_buf, 0x00, sizeof(s_buf));
    memset(payload, 0x00, sizeof(payload));

    response_type = RES_APP_FREQ_GET;

    length = make_packets(response_type, payload, s_buf);
    ret = send_message(conn_fd, s_buf, length);

#ifdef DEBUG
    printf(" < make_app_freq_get > \n");
    printf("----------------------------------\n");
#endif

    return ret;
}

/////////////////////////////////////////////////////////////////////////
// Req : 0x051, RES : 0x501
int make_app_mem_get(int conn_fd)
{
    unsigned int response_type;
    char payload[MAX_MSGSIZE];
    char s_buf[MAX_MSGSIZE];
    int length;
    int ret;

    memset(s_buf, 0x00, sizeof(s_buf));
    memset(payload, 0x00, sizeof(payload));

    response_type = RES_APP_MEM_GET;

    length = make_packets(response_type, payload, s_buf);
    ret = send_message(conn_fd, s_buf, length);

#ifdef DEBUG
    printf(" < make_app_mem_get > \n");
    printf("----------------------------------\n");
#endif

    return ret;
}

/////////////////////////////////////////////////////////////////////////
// Req : 0x052, RES : 0x502
int make_app_net_get(int conn_fd)
{
    unsigned int response_type;
    char payload[MAX_MSGSIZE];
    char s_buf[MAX_MSGSIZE];
    int length;
    int ret;

    memset(s_buf, 0x00, sizeof(s_buf));
    memset(payload, 0x00, sizeof(payload));

    response_type = RES_APP_NET_GET;

    length = make_packets(response_type, payload, s_buf);
    ret = send_message(conn_fd, s_buf, length);

#ifdef DEBUG
    printf(" < make_app_net_get > \n");
    printf("----------------------------------\n");
#endif

    return ret;
}

/////////////////////////////////////////////////////////////////////////
// Req : 0x053, RES : 0x503
int make_app_power_get(int conn_fd)
{
    unsigned int response_type;
    char payload[MAX_MSGSIZE];
    char s_buf[MAX_MSGSIZE];
    int length;
    int ret;

    memset(s_buf, 0x00, sizeof(s_buf));
    memset(payload, 0x00, sizeof(payload));

    response_type = RES_APP_POWER_GET;

    length = make_packets(response_type, payload, s_buf);
    ret = send_message(conn_fd, s_buf, length);

#ifdef DEBUG
    printf(" < make_app_power_get > \n");
    printf("----------------------------------\n");
#endif

    return ret;
}

/////////////////////////////////////////////////////////////////////////
// Req : 0x080, RES : 0x800
int make_app_force_reg(int conn_fd, int pid)
{
    unsigned int response_type;
    char payload[MAX_MSGSIZE];
    char s_buf[MAX_MSGSIZE];
    int length;
    int ret;

    memset(s_buf, 0x00, sizeof(s_buf));
    memset(payload, 0x00, sizeof(payload));

    force_registration_app(pid);

    response_type = RES_APP_FORCE_REG;

    length = make_packets(response_type, payload, s_buf);
    ret = send_message(conn_fd, s_buf, length);

#ifdef DEBUG
    printf(" < make_app_force_reg > \n");
    printf("----------------------------------\n");
#endif

    return ret;
}

/////////////////////////////////////////////////////////////////////////
// Req : 0x081, RES : 0x801
int make_app_force_unreg(int conn_fd, int pid)
{
    unsigned int response_type;
    char payload[MAX_MSGSIZE];
    char s_buf[MAX_MSGSIZE];
    int length;
    int ret;

    memset(s_buf, 0x00, sizeof(s_buf));
    memset(payload, 0x00, sizeof(payload));

    force_unregistration_app(pid);

    response_type = RES_APP_FORCE_UNREG;

    length = make_packets(response_type, payload, s_buf);
    ret = send_message(conn_fd, s_buf, length);

#ifdef DEBUG
    printf(" < make_app_force_unreg > \n");
    printf("----------------------------------\n");
#endif

    return ret;
}
/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////



/**
 * @breif   Worker thread for REQ_SESSION_START
 *
 * @param   conn_Fd     conntion socket fd
 *
 * @return  NULL
 *
 */
void *session_response(void *data)
{
    int conn_fd = *(int *)data;
    int ret = 0;

    while((session_start) && (ret >= 0))
    {
        clock_ts = get_nano_timestamp();
        ret = make_app_info(conn_fd);
        ret = make_syswide_info(conn_fd);
        ret = make_session_start_response(conn_fd);
        usleep(1000*1000*1);
    }
    session_start = 0;
    pthread_exit(NULL);
}



/**
 * @breif   Worker thread for REQ_APPINFO_START
 *
 * @param   conn_Fd     conntion socket fd
 *
 * @return  NULL
 *
 */
void *app_response(void *data)
{
    int conn_fd = *(int *)data;
    int ret = 0;

    while((app_start) && (ret >= 0))
    {
        ret = make_app_qos_info(conn_fd);
        ret = make_app_sys_info(conn_fd);
        ret = make_app_start_response(conn_fd);
        usleep(1000*1000*1);
    }
    app_start = 0;
    pthread_exit(NULL);
}


#ifdef MOCO
extern pthread_t tid_mq[MQ_SIZE];
#endif

int process_message(int conn_fd, char *msg)
{
    int total_length = 0;
    unsigned int type;
    int ret;
    char tmp[MAX_MSGSIZE];
    int i, j;


    memcpy(tmp, msg+INDEX_LOAD, sizeof(tmp));
    parse_request(&total_length, &type, msg);

    switch(type)
    {

        case REQ_APP_REGISTER:      // Req : 0x000, Res : 0x900
            printf(" Req : REQ_APP_REGISTER\n");


#ifdef MOCO
            /****************************************
             * At this time, a new app is registered
             ****************************************/
      //      printf("mQ_cnt = %d \n", mQ_cnt);
            ret = pthread_create(&tid_mq[mQ_cnt], NULL, queue_listen, (void *)mQ_cnt);
            if(ret < 0)
            {
                perror("pthread_create():tid_mq");
                exit(EXIT_FAILURE);
            }
            /*******************************************************
             * From now on, the handshaking
             * is handled by the corresponding queue_listen thread.
             * NOTE! the ordering is important!
             * self-adapataion can only be applied after this thread
             * is successfully created.
             *******************************************************/
#endif



            make_response_app_register(conn_fd, pid_list[0], tmp);

            break;

        case REQ_APP_CLEAR:      // Req : 0x001
            printf(" Req : REQ_APP_CLEAR (0x001)\n");

            //clear_app_info(pid_list[0]);
            check_app();

            break;

        case REQ_SESSION_REGISTER:     // Req : 0x010, RES : 0x100
            printf(" Req : REQ_SESSION_REGISTER (0x010)\n");

            make_session_register(conn_fd);
            break;

        case REQ_SESSION_START:         // Req : 0x011, RES : 0x101
            printf(" Req : REQ_SESSION_START (0x011)\n");

            session_start = 1;
            ret = pthread_create(&tid_session, NULL, session_response, 
                    (void *)&conn_fd);
            if(ret < 0)
            {
                perror("pthread_create():tid_session");
                exit(EXIT_FAILURE);
            }

            break;

        case REQ_SESSION_STOP:          // Req : 0x012, RES : 0x102
            printf(" Req : REQ_SESSION_STOP (0x012)\n");

            req_app_count = 0;
            for(i=0 ; i<MAX_APP_COUNT ; i++)
            {
                app_list[i] = 0;
            }

            make_session_stop(conn_fd);
            break;

        case REQ_APPINFO_GET:           // Req : 0x020, RES : 0x200
            printf(" Req : REQ_APPINFO_GET (0x020)\n");

            //make_appinfo_get(conn_fd);
            make_app_info(conn_fd);
            break;

        case REQ_APPINFO_START:         // Req : 0x021, RES : 0x201
            printf(" Req : REQ_APPINFO_START (0x021)\n");

            if(app_start == 0)
            {
                app_start = 1;
                ret = pthread_create(&tid_app, NULL, app_response, 
                        (void *)&conn_fd);

                if(ret < 0)
                {
                    app_start = 0;
                    perror("pthread_create():tid_app");
                    exit(EXIT_FAILURE);
                }
            }

            for(i=0 ; i<pid_count ; i++)
            {
                if(pid_list[i] > 0)
                {
                    for(j=0 ; j<MAX_APP_COUNT ; j++)
                    {
                        if(app_list[j] <= 0)
                        {
                            app_list[j] = pid_list[i];
                            req_app_count++;
                            break;
                        }
                    }
                }
            }

            break;


        case REQ_APPINFO_STOP:          // Req : 0x022, RES : 0x202
            printf(" Req : REQ_APPINFO_STOP (0x022)\n");

            for(i=0 ; i<MAX_APP_COUNT ; i++)
            {
                for(j=0 ; j<pid_count ; j++)
                {
                    if((pid_list[j] > 0) && (app_list[i] == pid_list[j]))
                    {
                        app_list[i] = 0;
                        req_app_count--;
                    }
                }
            }

            if(req_app_count == 0)
            {
                for(i=0 ; i<MAX_APP_COUNT ; i++)
                {
                    app_list[i] = 0;
                }

                make_app_stop(conn_fd);
            }

            break;

        case REQ_APPINFO_KILL:          // Req : 0x023, RES : 0x203
            printf(" Req : REQ_APPINFO_KILL (0x023)\n");

            make_appinfo_kill(conn_fd);
            break;

        case REQ_CPUINFO_GET:           // Req : 0x030, RES : 0x300
            printf(" Req : REQ_CPUINFO_GET (0x030)\n");

            make_cpuinfo_get(conn_fd);
            break;

        case REQ_MEMINFO_GET:           // Req : 0x031, RES : 0x301
            printf(" Req : REQ_MEMINFO_GET (0x031)\n");

            make_meminfo_get(conn_fd);
            break;

        case REQ_NETINFO_GET:           // Req : 0x032, RES : 0x302
            printf(" Req : REQ_NETINFO_GET (0x032)\n");

            make_netinfo_get(conn_fd);
            break;

        case REQ_COREINFO_GET:          // Req : 0x033, RES : 0x303
            printf(" Req : REQ_COREINFO_GET (0x033)\n");

            make_coreinfo_get(conn_fd);
            break;

        case REQ_APP_STATUS_GET:        // Req : 0x040, RES : 0x400
            printf(" Req : REQ_APP_STATUS_GET (0x040)\n");

            make_app_qos_info(conn_fd);
            break;

        case REQ_APP_FREQ_GET:          // Req : 0x050, RES : 0x500
            printf(" Req : REQ_APP_FREQ_GET (0x050)\n");

            make_app_freq_get(conn_fd);
            break;

        case REQ_APP_MEM_GET:           // Req : 0x051, RES : 0x501
            printf(" Req : REQ_APP_MEM_GET (0x051)\n");

            make_app_mem_get(conn_fd);
            break;

        case REQ_APP_NET_GET:           // Req : 0x052, RES : 0x502
            printf(" Req : REQ_APP_NET_GET (0x052)\n");

            make_app_net_get(conn_fd);
            break;

        case REQ_APP_POWER_GET:         // Req : 0x053, RES : 0x503
            printf(" Req : REQ_APP_POWER_GET (0x053)\n");

            make_app_power_get(conn_fd);
            break;

        case REQ_APP_FORCE_REG:
            printf(" Req : REQ_APP_FORCE_REG (0x090)\n");

            make_app_force_reg(conn_fd, pid_list[0]);
            break;

        case REQ_APP_FORCE_UNREG:
            printf(" Req : REQ_APP_UNFORCE_REG (0x091)\n");

            make_app_force_unreg(conn_fd, pid_list[0]);
            break;          
    }

    return 0;
}

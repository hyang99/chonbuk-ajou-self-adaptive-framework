#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/types.h>
#include <regex.h>
#include <limits.h>
#include <sys/stat.h>
#include <fcntl.h>

//#define NDEBUG
#include <assert.h>

#include "skb-types.h"
#include "skb-proc.h"
#include "skb-app.h"
#include "skb-common.h"
#include "skb-shm.h"
#include "skb-power.h"

#define Hertz   (int)sysconf(_SC_CLK_TCK)

int get_physical_core()
{
    char location[64] = "/sys/devices/system/cpu/possible";
    FILE *fp;
    char *line = 0;
    size_t len = 0;
    int start, end;

    fp = fopen(location, "r");

    if(fp)
    {
        getline(&line, &len, fp);
        sscanf(line, "%d-%d", &start, &end);
        fclose(fp);

        free(line);
        
        return end - start + 1;
    }

    return 0;
}

int get_core_count()
{
    int c_core = 0;

    c_core = (int)sysconf(_SC_NPROCESSORS_ONLN);

    return c_core;
}


/**
 * @breif   get maximum frequnce of core
 *
 * @param   core_id     core id
 *
 * @return  NULL
 *
 */
long _get_core_max_freq(int core_id)
{
    char location[64];
    FILE *fp;
    long freq;

    //sprintf(location, "/sys/devices/system/cpu/cpu%d/cpufreq/cpuinfo_max_freq", core_id);
    sprintf(location, "/sys/devices/system/cpu/cpu%d/cpufreq/scaling_max_freq", core_id);
    fp = fopen(location, "r");

    if(fp != NULL)
    {
        fscanf(fp, "%ld", &freq);

        fclose(fp);
    }

    return freq;
}

/**
 * @breif   get minimum frequnce of core
 *
 * @param   core_id     core id
 *
 * @return  long        frequence
 *
 */
long _get_core_min_freq(int core_id)
{
    char location[64];
    FILE *fp;
    long freq;

    //sprintf(location, "/sys/devices/system/cpu/cpu%d/cpufreq/cpuinfo_min_freq", core_id);
    sprintf(location, "/sys/devices/system/cpu/cpu%d/cpufreq/scaling_min_freq", core_id);
    fp = fopen(location, "r");

    if(fp != NULL)
    {
        fscanf(fp, "%ld", &freq);

        fclose(fp);
    }

    return freq;
}

/**
 * @breif   get current frequnce of core
 *
 * @param   core_id     core id
 *
 * @return  long        frequence
 *
 */
long _get_core_cur_freq(int core_id)
{
    char location[64];
    FILE *fp;
    long freq;

    sprintf(location, "/sys/devices/system/cpu/cpu%d/cpufreq/scaling_cur_freq", core_id);
    fp = fopen(location, "r");

    if(fp != NULL)
    {
        fscanf(fp, "%ld", &freq);

        fclose(fp);
    }

    return freq;
}


void collect_core_freq_dynamic()
{
    int i;

    //for(i=0 ; i<core_count ; i++)
    for(i=0 ; i<current_core_count ; i++)
    {
        core_info[i].core_dynamic.core_cur_freq = _get_core_cur_freq(i);
    }
}

void collect_core_freq_static()
{
    int i;

    for(i=0 ; i<max_core_count ; i++)
    {
        core_info[i].core_static.core_max_freq = _get_core_max_freq(i);
        core_info[i].core_static.core_min_freq = _get_core_min_freq(i);
    }
}


/**
 * @breif   get core performance
 *
 * @return  NULL
 *
 */
void collect_core_mips()
{
    char *location = "/proc/cpuinfo";
    FILE *fp;
    char *line = 0, *match = 0;
    size_t len = 0;
    ssize_t read_size;
    int i = 0;
    int mips = 0;

    fp = fopen(location, "r");

    if(fp != NULL)
    {
        while((read_size = getline(&line, &len, fp)) != -1)
        {
#ifdef XU3
            match = strstr(line, "BogoMIPS");
#elif x86
            match = strstr(line, "bogomips");
#endif
            if(match)
            {
#ifdef XU3
                sscanf(line, "BogoMIPS  : %d", &mips);
#elif x86
                sscanf(line, "bogomips  : %d", &mips);
#endif
                core_info[i].core_static.core_mips = mips;
                i++;
            }
        }


        fclose(fp);

        free(line);
        free(match);
    }
}

/**
 * @breif   get available frequence of core
 *
 * @return  NULL
 *
 */
void collect_core_available_freq()
{
    int i, j;
    char location[MAX_MSGSIZE];
    FILE *fp;
    long freq;
    int freq_count;

    char *line = 0;
    size_t len = 0;
    ssize_t read_size;


    for(i=0 ; i<max_core_count ; i++)
    {
        if(core_info[i].core_dynamic.core_active == 1)
        {
            sprintf(location, "/sys/devices/system/cpu/cpu%d/cpufreq/stats/time_in_state", i);
            fp = fopen(location, "r");

            if(fp != NULL)
            {
                freq_count = 0;

                while((read_size = getline(&line, &len, fp)) != -1)
                {
                    sscanf(line, "%ld", &freq);
                    core_info[i].core_static.available_freq[freq_count] = freq;
                    freq_count++;
                }

                core_info[i].core_static.available_freq_count = freq_count;


                fclose(fp);
            }
            else
            {
                printf("\n\n");
                printf("\t\t//////////////////////////////////////////////////\n");
                printf("\t\t//     This system can NOT adjust FREQUENCY     //\n");
                printf("\t\t//     Available FREQUENCY info will set '-1'   //\n");
                printf("\t\t//////////////////////////////////////////////////\n");
                printf("\n\n");

                for(j=0 ; j<max_core_count ; j++)
                {
                    memset(core_info[j].core_static.available_freq, -1, MAX_FREQ_COUNT);
                    freq_count = -1;
                }

                break;
            }
        }
    }

    free(line);
}


/**
 * @breif   get active state of core
 *
 * @return  NULL
 *
 */
void collect_core_active()
{
    char location[64];
    FILE *fp;
    int core_enable;
    int i;

    //for(i=0 ; i<core_count ; i++)
    //for(i=0 ; i<current_core_count ; i++)
    for(i=0 ; i<max_core_count ; i++)
    {
        sprintf(location, "/sys/devices/system/cpu/cpu%d/online", i);
        fp = fopen(location, "r");

        if(fp != NULL)
        {
            fscanf(fp, "%d", &core_enable);

            core_info[i].core_dynamic.core_active = core_enable;

            fclose(fp);
        }
    }
}


/**
 * @breif   get timestamp of CORE_DYNAMIC
 *
 * @return  NULL
 *
 */
void collect_core_timestamp()
{
    int i;

    //for(i=0 ; i<core_count ; i++)
    for(i=0 ; i<current_core_count ; i++)
    {
        core_info[i].core_dynamic.timestamp = (long long int)get_nano_timestamp();
    }
}



/**
 * @breif   get thread count on core
 *
 * @return  NULL
 *
 */
void collect_proc_thread_count()
{
    int i;
    int core = -1;

    //for(i=0 ; i<core_count ; i++)
    for(i=0 ; i<current_core_count ; i++)
    {
        core_info[i].core_dynamic.thread_on_core = 0;
        core_info[i].core_dynamic.process_on_core = 0;
    }

    for(i=0 ; i<MAX_PROCESS_COUNT ; i++)
    {
        if(process_info[i].pid > 0)
        {
            core = process_info[i].core_id;
            core_info[core].core_dynamic.process_on_core++;
            core_info[i].core_dynamic.thread_on_core += process_info[i].thread_on_proc;
        }
    }
}

/**
 * @breif   get core usage - using for process usage
 *
 * @param   PROCESS_INFO     process info
 *
 * @return  double          core usage
 *
 */
double _get_core_time(PROCESS_INFO *p)
{
    unsigned long new_user, new_nice, new_system, new_idle;
    unsigned long delta;

    int cpuid;
    char location[64] = "/proc/stat";
    FILE *fp;
    char *line = 0, *match = 0;
    size_t len = 0;
    ssize_t read_size;
    char tmp[32];

    sprintf(tmp, "cpu%d", p->core_id);
    fp = fopen(location, "r");


    if(fp != NULL)
    {
        while((read_size = getline(&line, &len, fp)) != -1)
        {
            match = strstr(line, tmp);
            if(match)
            {
                sscanf(line, "cpu%d %lu %lu %lu %lu", 
                        &cpuid,
                        &new_user,
                        &new_nice,
                        &new_system,
                        &new_idle);
            }
        }


        fclose(fp);

        delta = new_user - p->user_mode
            + new_nice - p->nice_mode
            + new_system - p->system_mode
            + new_idle - p->idle_mode;


        p->user_mode = new_user;
        p->nice_mode = new_nice;
        p->system_mode = new_system;
        p->idle_mode = new_idle;

        free(line);
        free(match);
    }

    return (double)delta;
}

/**
 * @breif   get memory usage for core
 *
 * @param   NULL
 *
 * @return  NULL
 *
 */
void _get_core_memory_info()
{
    FILE *fp_sysstatm;
    char sysstatm_location[64] = "/proc/meminfo";
    char *line = 0; 
    size_t len; 
    int i;
    double usage;
    int memtotal = 0, memfree = 0; 
    int mem_size = 0;

    fp_sysstatm = fopen(sysstatm_location, "r");

    if(fp_sysstatm != NULL)
    {    
        while((getline(&line, &len, fp_sysstatm)) != -1)
        {    
            if(!strncmp(line, "MemTotal:", 9))
            {    
                sscanf(line, "%*s %d", &memtotal);
            }    
            else if(!strncmp(line, "MemFree:", 8))
            {    
                sscanf(line, "%*s %d", &memfree);
            }    
        }    

        fclose(fp_sysstatm);
    }    

    free(line);

    mem_size = memtotal - memfree;
    usage = (double)((memtotal-memfree)*100.0)/(double)memtotal;

    for(i=0 ; i<max_core_count ; i++) 
    {    
        core_info[i].mem_total_usage = usage;
        core_info[i].mem_size = mem_size;
    }    

    for(i=0 ; i<MAX_PROCESS_COUNT ; i++)
    {
        if(process_info[i].pid > 0)
        {
            process_info[i].mem.total_usage = usage;
        }
    }

}

/**
 * @breif   get memory usage for process
 *
 * @param   PROCESS_INFO     process info
 *
 * @return  NULL
 *
 */
void _get_proc_memory_info(PROCESS_INFO *p)
{
    char status_location[64];
    char *line = 0;
    size_t len;

    int vmsize = 0, vmpeak = 0, vmrss = 0, vmhwm = 0;
    int pid = p->pid;

    FILE *fp_status;

    sprintf(status_location, "/proc/%d/status", pid);

    fp_status = fopen(status_location, "r");

    if(fp_status != NULL)
    {
        while((getline(&line, &len, fp_status)) != -1)
        {
            if(!strncmp(line, "VmPeak:", 7))
            {
                sscanf(line, "%*s %d", &vmpeak);
            }
            else if(!strncmp(line, "VmSize:", 7))
            {
                sscanf(line, "%*s %d", &vmsize);
            }
            else if(!strncmp(line, "VmHWM:", 6))
            {
                sscanf(line, "%*s %d", &vmhwm);
            }
            else if(!strncmp(line, "VmRSS:", 6))
            {
                sscanf(line, "%*s %d", &vmrss);
            }
        }

        fclose(fp_status);
    }

    if(line != NULL)
        free(line);

    p->mem.vmpeak = vmpeak;
    p->mem.vmsize = vmsize;
    p->mem.vmhwm = vmhwm;
    p->mem.vmrss = vmrss;
    p->mem.usage = (double)vmsize / (double)1024.0;
    //p->mem.total_usage = (double)((memtotal-memfree)*100.0)/(double)memtotal;
}

void _get_network_info(PROCESS_INFO *p)
{
    char *location = "/proc/tcp_monitor_info";
    FILE *fp;
    char *line = 0, *match = 0;
    size_t len = 0;
    ssize_t read_size;
    int pid;
    char name[MAX_MSGSIZE];
    char tmp[32];
    unsigned long total_tx = 0, total_rx = 0;//, delta_tx = 0, delta_rx = 0;
    unsigned long rx_bw = 0, tx_bw = 0;
    unsigned long back_time = 0, preq_time = 0;
    unsigned long ooo_time = 0, recvq_time = 0;
    unsigned long latency_time = 0;

    sprintf(tmp, "%d", p->pid);
    fp = fopen(location, "r");

    if(fp != NULL)
    {
        while((read_size = getline(&line, &len, fp)) != -1)
        {
            match = strstr(line, tmp);
            if(match)
            {
                sscanf(line, "%d %s %lu %lu %lu %lu %lu %lu %lu %lu %lu",
                        &pid,
                        name,
                        &total_rx,
                        &total_tx,
                        &rx_bw,
                        &tx_bw,
                        &back_time,
                        &preq_time,
                        &ooo_time,
                        &recvq_time,
                        &latency_time);
                //printf(" >>> %s\n", line);

                memset(p->net.netID, 0x00, 10);     // NOT using;
                p->net.nic_count = 0;           // NOT using;
                p->net.delta_rx_bytes = total_rx - p->net.total_rx_bytes;
                p->net.delta_tx_bytes = total_tx - p->net.total_tx_bytes;
                p->net.total_rx_bytes = total_rx;
                p->net.total_tx_bytes = total_tx;
                p->net.rx_usage = (double)p->net.delta_rx_bytes * 100.0 / (double)core_info[p->core_id].net_info.total_rx_bytes;
                p->net.tx_usage = (double)p->net.delta_tx_bytes * 100.0 / (double)core_info[p->core_id].net_info.total_tx_bytes;
                p->net.rx_bandwidth = rx_bw;
                p->net.tx_bandwidth = tx_bw;
                p->net.backlogq_time = back_time;
                p->net.preq_time = preq_time;
                p->net.ooo_time = ooo_time;
                p->net.recvq_time = recvq_time;
                p->net.latency_time = latency_time;
            }
        }

        fclose(fp);

        if(line != NULL)
            free(line);
    }
    else
    {
        //printf("You MUST insert 'tcp_monitor_info' module!\n");

        return;
    }
}



/**
 * @breif   get process usage
 *
 * @param   PROCESS_INFO     process info
 * @param   u                new utime of process
 * @param   s                new stime of process
 * @param   cu               new cutime of process
 * @param   cs               new cstime of process
 *
 * @return  double           process usage
 *
 */
double _get_process_usage(PROCESS_INFO *p, unsigned long u, unsigned long s, unsigned int cu, unsigned int cs)
{
    unsigned long tmp_u = p->u_time;
    unsigned long tmp_s = p->s_time;
    unsigned int tmp_cu = p->cu_time;
    unsigned int tmp_cs = p->cs_time;

    unsigned long user_tick, sys_tick;
    double total_cpu_tick;
    double user_utilization;
    double sys_utilization;
    double usage = 0;

    unsigned long user_delta, sys_delta;
    unsigned int cu_delta, cs_delta;

    total_cpu_tick = _get_core_time(p);

    if(total_cpu_tick > 0)
    {
        user_delta = u - tmp_u;
        cu_delta = cu - tmp_cu;
        sys_delta = s - tmp_s;
        cs_delta = cs - tmp_cs;

        user_tick = user_delta + (unsigned long)cu_delta;
        user_tick = user_tick * 100;

        sys_tick = sys_delta + (unsigned long)cs_delta;
        sys_tick = sys_tick * 100;

        user_utilization = (double)user_tick / total_cpu_tick;
        sys_utilization = (double)sys_tick / total_cpu_tick;

        usage = user_utilization + sys_utilization;

        if(usage > 100)     //by 1st loop
            usage = 0;
    }
    else
        usage = 0;

    return usage;
}


/**
 * @breif   get power info using sensor
 *
 * @param   NULL
 *
 * @return  NULL
 *
 */
void collect_power_info()
{
    int i;

    read_sensor(&sensor[SENSOR_ARM]);
    read_sensor(&sensor[SENSOR_MEM]);
    read_sensor(&sensor[SENSOR_KFC]);
    read_sensor(&sensor[SENSOR_G3D]);

    //for(i=0 ; i<core_count ; i++)
    for(i=0 ; i<max_core_count ; i++)
    {
        memcpy(core_info[i].power_sensor, sensor , sizeof(sensor_t)*SENSOR_MAX);
    }
}


/**
 * @breif   get network usage
 *
 * @param   NULL
 *
 * @return  NULL
 *
 */
void collect_network_info()
{
    //char pidnetdev_location[64];
    char sysnetdev_location[64] = "/proc/net/dev";
    char *line = 0;
    size_t len;
    int nic_count;
    int i;
    char netID[10];

    char *token = NULL;

    int tokenCnt = 0;

    //int pid = p->pid;
    unsigned long rx_bytes, tx_bytes;
    unsigned long delta_rx, delta_tx;

    FILE *fp_sysnet; //*fp_pidnet, ;

    fp_sysnet = fopen(sysnetdev_location, "r");

    if(fp_sysnet != NULL)
    {
        while(getline(&line, &len, fp_sysnet) != -1)
        {
            tokenCnt = 0;
            nic_count = 0;

            if(strstr(line, "lo:") > 0 || strstr(line, "eth0") > 0) 
            {
                token = strtok(line, " ");
                sprintf(netID, "%s", token);

                while(token)
                {
                    tokenCnt++;
                    token = strtok(NULL, " ");
                    if(tokenCnt == 1) 
                    {
                        rx_bytes = strtoul(token, NULL, 0);
                    }
                    else if (tokenCnt == 2) 
                    {
                        // nothing.
                    }
                    else if (tokenCnt == 9) 
                    {
                        tx_bytes = strtoul(token, NULL, 0);
                    }
                    else if (tokenCnt == 10)
                    {
                        // nothing.
                    }
                }

                nic_count++;
            }            
        }

        fclose(fp_sysnet);

        //for(i=0 ; i<core_count ; i++)
        for(i=0 ; i<current_core_count ; i++)
        {
            delta_rx = rx_bytes - core_info[i].net_info.total_rx_bytes;
            delta_tx = tx_bytes - core_info[i].net_info.total_tx_bytes;

            core_info[i].net_info.rx_usage = (double)delta_rx * 100.0 / (double)rx_bytes;
            core_info[i].net_info.tx_usage = (double)delta_tx * 100.0 / (double)tx_bytes;

            core_info[i].net_info.total_rx_bytes = rx_bytes;
            core_info[i].net_info.total_tx_bytes = tx_bytes;
            core_info[i].net_info.delta_rx_bytes = delta_rx;
            core_info[i].net_info.delta_tx_bytes = delta_tx;
        }
    }

    free(line);
}


/**
 * @breif   get thread list of [pid]
 *
 * @param   PROCESS_INFO    struct process_info of [pid]
 *
 * @return  NULL
 *
 */
 void _get_thread_list(PROCESS_INFO *p)
 {
     char location[64];
     DIR *dp;
     struct dirent *dir;
     struct stat st;
     int index = 0;

     memset(p->thread_list, 0x00, sizeof(p->thread_list));
     sprintf(location, "/proc/%d/task", p->pid);

     if((dp = opendir(location)) == NULL)
     {
#ifdef DEBUG
         printf(" [%d] process is not present\n", p->pid);
#endif
         return;
     }

     chdir(location);

     while((dir = readdir(dp)))
     {
         memset(&st, 0, sizeof(struct stat));
         lstat(dir->d_name, &st);

         if(S_ISDIR(st.st_mode))
         {
             if(strncmp(".", dir->d_name, 1) == 0 ||
                 strncmp("..", dir->d_name, 2) == 0)
             {
                 continue;
             }

             p->thread_list[index] = atoi(dir->d_name);
             index++;
         }
     }

     closedir(dp);
 }



/**
 * @breif   get process data from /proc[pid]
 *
 * @param   pid      process id
 *
 * @return  NULL
 *
 */
void get_process_info(int pid)
{
    int i;
    char location[64];
    char buf[MAX_MSGSIZE];
    char *line = 0;
    size_t len = 0;

    FILE *fp;
    int where = -1;

    char state;
    int cpuid, parent, fore;
    unsigned int cu, cs, priority, thread;
    unsigned long u, s;
    unsigned long long start_time;
    double usage;

    sprintf(location, "/proc/%d/stat", pid);
    fp = fopen(location, "r");

    if(fp != NULL)
    {
        getline(&line, &len, fp);
        fclose(fp);

        sscanf(line, "%*d %*s %c %d %*d %*d %*d %d %*u %*u"
                " %*u %*u %*u %lu %lu %u %u %u %*d %u"
                " %*d %llu %*u %*d %*u %*u %*u %*u %*u %*u"
                " %*u %*u %*u %*u %*u %*u %*u %*d %d",
                &state, &parent, &fore,
                &u, &s, &cu, &cs, &priority, &thread,
                &start_time, &cpuid);

        free(line);

    }

    for(i=0 ; i<MAX_PROCESS_COUNT ; i++)
    {
        if(process_info[i].pid == pid)
        {
            where = i;
            break;
        }
    }

    if(where == -1)     // new process
    {
        for(i=0 ; i<MAX_PROCESS_COUNT ; i++)
        {
            if(process_info[i].pid <= 0)
            {
                where = i;
                break;
            }
        }
    }


    switch(state)
    {
        case 'R':
            process_info[where].proc_state = PROCESS_RUNNING;
            break;
        case 'S':
            process_info[where].proc_state = PROCESS_INTERRUPT_SLEEP;
            break;
        case 'D':
            process_info[where].proc_state = PROCESS_DISK_SLEEP;
            break;
        case 'T':
            process_info[where].proc_state = PROCESS_TRACED;
            break;
        case 'W':
            process_info[where].proc_state = PROCESS_PAGING;
            break;
        case 'Z':
            process_info[where].proc_state = PROCESS_ZOMBIE;
            break;
        default:
            process_info[where].proc_state = PROCESS_NONE;
            break;
    }


    process_info[where].pid = pid;
    process_info[where].core_id = cpuid;
    process_info[where].thread_on_proc = thread;
    process_info[where].foreground = fore;
    process_info[where].parent_process = parent;
    process_info[where].priority = priority;
    usage = _get_process_usage(&process_info[where], u, s, cu, cs);
    process_info[where].usage = usage;
    process_info[where].u_time = u;
    process_info[where].s_time = s;
    process_info[where].cu_time = cu;
    process_info[where].cs_time = cs;
    process_info[where].start_time = start_time;
    _get_proc_memory_info(&process_info[where]);
    _get_thread_list(&process_info[where]);
#ifdef XU3
    _get_network_info(&process_info[where]);
#endif
    process_info[where].timestamp = (long long int)get_nano_timestamp();

    if(log_flag)
        //if(log_flag && (usage > 0))
    {
        memset(buf, 0x00, sizeof(buf));
        sprintf(buf, "%lld|%d|%u|%f",
                process_info[where].timestamp,
                process_info[where].pid,
                process_info[where].thread_on_proc,
                process_info[where].usage);

        skb_logging(PROCESS_FILE, buf);
    }
}


void collect_process_info()
{
    int i;

    if(app_count > 0)
    {
        for(i=0 ; i<MAX_APP_COUNT ; i++)
        {
            if(app_info[i].pid > 0)
            {
                get_process_info(app_info[i].pid);
            }
        }
    }
}

/**
 * @breif   get core usage(total)
 *
 * @param   NULL
 *
 * @return  NULL
 *
 */
void collect_core_usage()
{
    unsigned long t_user, t_nice, t_sys, t_idle;
    unsigned long use_tick = 0, idle_tick = 0;
    unsigned long delta_use = 0, delta_idle = 0;
    //unsigned long total_delta_use = 0, total_delta_idle = 0;
    unsigned long sys_tmp[10];
    unsigned long total_jiffies = 0, work_jiffies = 0; 
    unsigned long prev_total = 0, prev_work = 0; 
    unsigned long delta_total = 0, delta_work = 0;
    double sys_total;
    
    int i, cpuid;
    char location[64] = "/proc/stat";
    FILE *fp;
    char *line = 0;
    size_t len = 0;
    char tmp[32];
    
    memset(sys_tmp, 0x00,sizeof(sys_tmp));
    
    prev_total = core_info[0].total_jiffies;
    prev_work = core_info[0].work_jiffies;

    fp = fopen(location, "r");

    if(fp != NULL)
    {
        getline(&line, &len, fp);
        sscanf(line, "cpu %lu %lu %lu %lu %lu %lu %lu",
                &sys_tmp[0],
                &sys_tmp[1],
                &sys_tmp[2],
                &sys_tmp[3],
                &sys_tmp[4],
                &sys_tmp[5],
                &sys_tmp[6]);

        for(i=0 ; i<10 ; i++)
        {
            total_jiffies += sys_tmp[i];

            if(i<3)
            {
                work_jiffies += sys_tmp[i];
            }
        }

        delta_work = work_jiffies - prev_work;
        delta_total = total_jiffies - prev_total;
        sys_total = (delta_work*100.0)/delta_total;

        //for(i=0 ; i<core_count ; i++)
        for(i=0 ; i<current_core_count ; i++)
        {
            sprintf(tmp, "cpu%d", i);

            getline(&line, &len, fp);
            sscanf(line, "cpu%d %lu %lu %lu %lu", 
                    &cpuid,
                    &t_user,
                    &t_nice,
                    &t_sys,
                    &t_idle);

            use_tick = t_user + t_nice + t_sys;
            idle_tick = t_idle;

            delta_use = use_tick - core_info[i].core_dynamic.total_use_tick;
            delta_idle = idle_tick - core_info[i].core_dynamic.total_idle_tick;

            core_info[i].core_dynamic.core_usage = (double)(delta_use * 100) / (double)(delta_use + delta_idle);

            core_info[i].core_dynamic.total_use_tick = use_tick;
            core_info[i].core_dynamic.total_idle_tick = idle_tick;
            
            core_info[i].core_dynamic.total_use_tick = use_tick;
            core_info[i].core_dynamic.total_idle_tick = idle_tick;
            core_info[i].total_jiffies = total_jiffies;
            core_info[i].work_jiffies = work_jiffies;
            core_info[i].delta_work = delta_work;
            core_info[i].delta_total = delta_total;

            if(sys_total)
                core_info[i].cpu_total_usage = sys_total;
        }

        fclose(fp);

        free(line);
    }

    _get_core_memory_info();
}


/**
 * @breif   get cluster type of core
 *
 * @param   NULL
 *
 * @return  NULL
 *
 */
void _get_bigLITTLE()
{
    long max = LONG_MIN, min = LONG_MAX;
    int i;

    //for(i=0 ; i<core_count ; i++)
    for(i=0 ; i<current_core_count ; i++)
    {
        if(core_info[i].core_static.core_max_freq > max)
        {
            max = core_info[i].core_static.core_max_freq;
        }

        if(core_info[i].core_static.core_min_freq < min)
        {
            min = core_info[i].core_static.core_min_freq;
        }
    }

    //for(i=0 ; i<core_count ; i++)
    for(i=0 ; i<current_core_count ; i++)
    {
        if(core_info[i].core_static.core_max_freq == max)
            core_info[i].core_static.bigLITTLE = 1;
        else
            core_info[i].core_static.bigLITTLE = 0;
    }
}

/**
 * @breif   get normalized value of core's mips
 *
 * @param   NULL
 *
 * @return  NULL
 *
 */
void _get_normalize_mips()
{
    float normalized;
    float d, n;
    int i;

    //for(i=0 ; i<core_count ; i++)
    for(i=0 ; i<current_core_count ; i++)
    {
        d = 0;
        n = 0;

        d = core_info[i].core_static.core_max_freq - core_info[i].core_static.core_min_freq;
        n = core_info[i].core_static.core_max_freq - core_info[i].core_dynamic.core_cur_freq;

        normalized = n / d;
        core_info[i].core_dynamic.normalized_mips = normalized;
    }
}

/**
 * @breif   collect cluster information of core
 *
 * @param   NULL
 *
 * @return  NULL
 *
 */
void collect_cluster_info()
{
    int i, count, first;//, cpu;
    char location[64];
    char *line = 0;
    size_t len = 0;
    FILE *fp;
    char *ptr;

    for(i=0 ; i<max_core_count ; i++)
    {
        count = 0;
        sprintf(location, "/sys/devices/system/cpu/cpu%d/cpufreq/related_cpus", i);
        fp = fopen(location, "r");

        if(fp != NULL)
        {
            getline(&line, &len, fp);

            fclose(fp);

            ptr = strtok(line, " ");
            first = atoi(ptr);
            count++;

            while((ptr = strtok(NULL, " ")))
            {
                count++;
            }

            free(line);
            line = 0;

            if(first == i)
            {
                core_info[i].core_static.cluster_master = 1;
            }
            else
            {
                core_info[i].core_static.cluster_master = 0;
            }
        }
    }

    _get_normalize_mips();
    _get_bigLITTLE();
}



void collect_core_static_info()
{
    collect_core_active();
    collect_core_freq_static();
    collect_core_mips();
    collect_core_available_freq();
    collect_cluster_info();
}

void collect_core_dynamic_info()
{
    char buf[512] = {0x00};
    int i;

    collect_core_active();
    collect_core_freq_dynamic();
    collect_proc_thread_count();
    collect_core_usage();
    collect_network_info();
    collect_core_timestamp();
#ifdef XU3
    collect_power_info();
#endif

    if(log_flag)
    {
        //for(i=0 ; i<core_count ; i++)
        for(i=0 ; i<current_core_count ; i++)
        {
            memset(buf, 0x00, sizeof(buf));
            sprintf(buf, "%lld|%d|%d|%ld|%d", 
                    core_info[i].core_dynamic.timestamp,
                    core_info[i].core_id,
                    core_info[i].core_dynamic.core_active,
                    core_info[i].core_dynamic.core_cur_freq,
                    core_info[i].core_dynamic.thread_on_core);

            skb_logging(CPU_FILE, buf);
        }
    }
}

void proc_update_pid_list()
{
    char location[64];
    FILE *fp;
    int i;
    char pid_tmp[8];
    int delim_flag = 0;

    memset(location, 0x00, sizeof(location));
    sprintf(location, "/proc/tcp_monitor_list");

    fp = fopen(location, "w");

    if(fp != NULL)
    {
        if(app_count == 0)
        {
            sprintf(pid_tmp, "%d", 0);
            fwrite(pid_tmp, 1, strlen(pid_tmp), fp);
        }
        else
        {
            for(i=0 ; i<MAX_PROCESS_COUNT ; i++)
            {
                memset(pid_tmp, 0x00, sizeof(pid_tmp));

                if(process_info[i].pid > 0)
                {
                    if(delim_flag == 0)
                    {
                        sprintf(pid_tmp, "%d", process_info[i].pid);
                        delim_flag = 1;
                    }
                    else
                    {
                        sprintf(pid_tmp, ",%d", process_info[i].pid);
                    }
                    fwrite(pid_tmp, 1, strlen(pid_tmp), fp);
                }
            }
        }

        if(fp != NULL)
            fclose(fp);

    }
}


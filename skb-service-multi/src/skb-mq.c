#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/msg.h>
#include <sys/ipc.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>

#include <errno.h>
#include <assert.h>

#include "skb-types.h"
#include "skb-mq.h"
#include "skb-proc.h"
#include "skb-app.h"
#include "skb-common.h"
#include "skb-shm.h"



#ifdef MOCO
int *mq_init( int pid ) {
#else
	/** struct for mq message */
	struct msgbuf {
		long mtype;         // message type
		char mtext[300];        // message buffer
	};

	int mq_init(int pid) {
#endif

#ifdef MOCO
        int i;
		int* mq_ID = malloc( 2 * sizeof(int));
        for( i=0; i < 2; i++) mq_ID[i] = 0;
#else
		int mq_ID = 0;
#endif

		key_t key;
		FILE *fp;
		char tmp[128];

		key = (key_t)pid;


#ifdef MOCO

		key_t _key;
		FILE *_fp;
		FILE *_pfp; // file pointer for process open( pid_max )
		char _tmp[128];


		int pid_max;
		char buf[256]; // buffer for pid_max
    printf(" mq_init entering\n");
		_pfp = popen("cat /proc/sys/kernel/pid_max","r");
		if( _pfp == NULL )
		{
			printf("error in popen() : mq_init\n");
			//	return -1;
		}

		while( fgets(buf, 256, _pfp) != NULL );

		pclose(_pfp);

		pid_max = atoi(buf);
		printf("Max PID : %d\n",pid_max);


		_key = 	(key_t)(pid + pid_max);		// to make unique key id  

    printf(" before creating mq entering\n");
		while(mq_ID[0] <= 0)
		{
    printf(" in  mq[0] entering\n");
			mq_ID[0] = msgget(key, IPC_CREAT | IPC_EXCL | 0640);
			printf("Msgget_init success : mq_ID[0]\n");
			if(mq_ID[0] <= 0)
			{
				msgctl(mq_ID[0], IPC_RMID, NULL);
			}
		}

		sprintf(tmp, "%d\n", mq_ID[0]);

		fp = fopen(msq_file, "a+");
		fwrite(tmp, 1, strlen(tmp), fp);
		fclose(fp);


		while(mq_ID[1] <= 0)
		{
    printf(" in  mq[1] entering\n");
			mq_ID[1] = msgget(_key, IPC_CREAT | IPC_EXCL | 0640);
			printf("Msgget_init success : mq_ID[1]\n");
			if(mq_ID[1] <= 0)
			{
				msgctl(mq_ID[1], IPC_RMID, NULL);
			}
		}

		sprintf(_tmp, "%d\n", mq_ID[1]);

		_fp = fopen(msq_file_ext, "a+");
		fwrite(_tmp, 1, strlen(_tmp), _fp);
		fclose(_fp);



		return mq_ID;


#else


		while(mq_ID <= 0)
		{
			mq_ID = msgget(key, IPC_CREAT | IPC_EXCL | 0640);
			if(mq_ID <= 0)
			{
				msgctl(mq_ID, IPC_RMID, NULL);
			}
		}

		sprintf(tmp, "%d\n", mq_ID);

		fp = fopen(msq_file, "a+");
		if(fp)
		{
			fwrite(tmp, 1, strlen(tmp), fp);
			fclose(fp);
		}

		return mq_ID;

#endif

	}
#ifdef MOCO
	void mq_recv(int id,char *buf) {
#else
	void mq_recv(char *buf) {
#endif
        //printf("%d: mq_recv invoked\n", id);
		int i, type;
		int ret;
		struct msgbuf mb;


		type = 1;


#ifdef MOCO
        i = id;
   //     printf(" mQ.flag = %d\n", mQ[i].flag);
#else
		for (i=0; i<mQ_cnt; i++) {
#endif

            //FIXME I'm not sure what I'm doing... H. Yang
            #ifdef MOCO
            if (mQ[i].flag==0) return;
            else if (mQ[i].flag== -1) return;
            #else
			if (mQ[i].flag == 0) 
				continue;
			else if (mQ[i].flag == -1) 
				break;
            #endif
			else {
#ifdef MOCO

				//	    ret = msgrcv(mQ[i].mid[0], &mb, sizeof(mb.mtext), type, IPC_NOWAIT);
				ret = msgrcv(mQ[i].mid[0], &mb, sizeof(mb.mtext), type, 0);

#else
				ret = msgrcv(mQ[i].mid, &mb, sizeof(mb.mtext), type, IPC_NOWAIT);
#endif

				memcpy(buf, mb.mtext, sizeof(mb.mtext));



				if((ret > 0) && (mb.mtype == type))
				{
#ifdef DEBUG
					printf(" < Received MQ's message > \n");
					printf("msg type = %ld, msg = %s \n", mb.mtype, mb.mtext);
					printf("----------------------------------\n");
#endif

#ifdef MOCO
					collect_app_dynamic_info( id, buf );
#else
	    			collect_app_dynamic_info(buf);
#endif
				}
			}



#ifdef MOCO
#else
		}
#endif
	}




#ifdef MOCO
	void putmQ(int pid, int* mid) {
#else
		void putmQ(int pid, int mid) {
#endif
			if (mQ_cnt >= 50) {
				printf("Error: mQStruct overflow \n");
				printf("SKB system adjusts mQStruct. \n");
				mQ_cnt = 0;
			}
            printf("mQ flag gets active\n");// hyang
            printf(" mQ_cnt = %d\n",mQ_cnt);
			mQ[mQ_cnt].flag = 1;  //0 is inactive, 1 is active, -1 is empty.
			mQ[mQ_cnt].pid = pid;
#ifdef MOCO
			mQ[mQ_cnt].mid[0] = mid[0];
			mQ[mQ_cnt].mid[1] = mid[1];
			printf("mQ[%d].mid pair = (%d,%d) \n",mQ_cnt,mid[0],mid[1]);
#else
			mQ[mQ_cnt].mid = mid;
			printf("mQ[%d].mid = %d \n",mQ_cnt,mid);
#endif
			mQ_cnt++;
		}


#ifdef MOCO
		void mq_clean_server()
		{
			FILE *fp;
			char *line = 0;
			size_t len = 0;
			ssize_t read_size;
			//char line[MAX_MSGSIZE];
			int msqid;

			fp = fopen(msq_file_ext, "r");

			if(fp != NULL)
			{
				while((read_size = getline(&line, &len, fp)) != -1)
					//while(fgets(line, sizeof(line), fp) != NULL)
				{
					sscanf(line, "%d", &msqid);

					msgctl(msqid, IPC_RMID, NULL);
					line = 0;
				}

				fclose(fp);
				free(line);
			}

			fp = fopen(msq_file_ext, "w");
			fclose(fp);


		}
#endif


		void mq_clean()
		{
			FILE *fp;
			char *line = 0;
			size_t len = 0;
			ssize_t read_size;
			int msqid;

			fp = fopen(msq_file, "r");

			if(fp != NULL)
			{
				while((read_size = getline(&line, &len, fp)) != -1)
				{
					sscanf(line, "%d", &msqid);

					msgctl(msqid, IPC_RMID, NULL);
					line = 0;
				}

				fclose(fp);
			}

			if(line != NULL)
				free(line);

			fp = fopen(msq_file, "w");
			fclose(fp);



#ifdef MOCO
			FILE *_fp;
			char *_line = 0;
			size_t _len = 0;
			ssize_t _read_size;
			//char line[MAX_MSGSIZE];
			int _msqid;

			_fp = fopen(msq_file_ext, "r");

			if(_fp != NULL)
			{
				while((_read_size = getline(&_line, &_len, _fp)) != -1)
					//while(fgets(line, sizeof(line), fp) != NULL)
				{
					sscanf(_line, "%d", &_msqid);

					msgctl(_msqid, IPC_RMID, NULL);
					line = 0;
				}

				fclose(_fp);
				free(_line);
			}

			_fp = fopen(msq_file_ext, "w");
			fclose(_fp);

#endif

		}

#ifdef MOCO
        void initStruct(int id) {
//          printf("%d: initStruct invoked\n",id);
            mQ[id].flag = -1;
            mQ[id].pid = -1;
            mQ[id].mid[0] = -1;
            mQ[id].mid[1] = -1; 
        }
#else

		void initStruct() {
			int i;

			for(i=0; i<MQ_SIZE; i++) {
				mQ[i].flag = -1;
				mQ[i].pid = -1;
				mQ[i].mid = -1;
			}
		}
#endif

#ifdef MOCO
/***********************************************
* Pthread; queue_listen                        *
* From now on, all variable is locally worked  *
************************************************/
		void *queue_listen(void *id) {
			initStruct((int)id);
#else
		void *queue_listen() {
			initStruct();
#endif
			char recv_buf[300];

			mQ_flag = 1;

			while(mQ_flag) {
				memset(recv_buf, 0x00, sizeof(recv_buf));

#ifdef MOCO
				mq_recv(id, recv_buf);
#else
				mq_recv(recv_buf);
#endif

				sa_process_update();
				check_app( );
				usleep(mq_interval);
			}

			pthread_exit(NULL);
		}

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "skb-common.h"
#include "skb-types.h"

static const char *short_options = "lhacpn:";

static struct option long_options[] = {
    {"log", no_argument, NULL, 'l'},
    {"help", no_argument, NULL, 'h'},
    {"app", no_argument, NULL, 'a'},
    {"core", no_argument, NULL, 'c'},
    {"process", no_argument, NULL, 'p'},
    {"interval", required_argument, NULL, 'n'},
    {0, 0, 0, 0}
};


int64_t get_nano_timestamp()
{
    struct timespec ts;
    int64_t timestamp;

    clock_gettime(CLOCK_REALTIME, &ts);

    timestamp = ((int64_t)ts.tv_sec * 1000000000 + (int64_t)ts.tv_nsec);

    return timestamp;
}


void skb_logging(char *file, char *buf)
{
    FILE *fp;
    char tmp[300];

    memset(tmp, 0x00, sizeof(tmp));
    sprintf(tmp, "%s\n", buf);

    fp = fopen(file, "a+");
    if(fp)
    {
        fwrite(tmp, 1, strlen(tmp), fp);
        fclose(fp);
    }
}

void help()
{
    printf("Usage : ./skb-service [options]\n");
    printf("Options:\n");
    printf("  -l, --log \t\t\tLog Enable\n");
    printf("  -h, --help \t\t\tDisplay this information\n\n");

    printf("  -a, --app \t\t\tDisplay Info of SA Application\n");
    printf("  -c, --core \t\t\tDisplay Info of Core\n");
    printf("  -p, --process \t\tDisplay Info of process\n\n");

    printf("  -n, --interval \t\tSet < us > interval of MQ (default is 500000)\n");
}

void hb_info_print()
{
    int i;

    printf("=========================================================\n");
    for(i=0 ; i<MAX_APP_COUNT ; i++)
    {
        if(app_info[i].pid > 0)
        {
            printf("---------------------------------------------------------\n");
            printf("%-25s : \t %d\n", " Application PID", app_info[i].pid);
            printf("%-25s : \t %s\n", " Application Name", app_info[i].app_name);
#ifdef MOCO
		printf("%-25s : \t %d\n", " Application MQ_ID", app_info[i].mq_id[0]);
#else
            printf("%-25s : \t %d\n", " Application MQ_ID", app_info[i].mq_id);
#endif
            printf("%-25s : \t %f\n", " Max QOS", app_info[i].app_static.app_max_qos);
            printf("%-25s : \t %f\n", " Target QOS", app_info[i].app_static.app_target_qos);
            printf("%-25s : \t %f\n", " Min QOS", app_info[i].app_static.app_min_qos);
            printf("%-25s : \t %f\n", " Window Size", app_info[i].app_static.app_window_size);
            printf("%-25s : \t %ld\n", " Sampling Time", app_info[i].app_static.app_sampling_time);
            //printf("%-25s : \t %d\n\n", " HeartBeat ID", app_info[i].app_dynamic.app_hb_id);

            printf("%-25s : \t %d\n", " HeartBeat Count", app_info[i].app_static.app_hb_count);
            printf("%-25s : \t %f\n", " Window Rate", app_info[i].app_dynamic.app_window_rate);
            printf("%-25s : \t %f\n", " Instant Rate", app_info[i].app_dynamic.app_instant_rate);
            printf("%-25s : \t %lld\n", " Time Stamp", app_info[i].app_dynamic.app_timestamp);
            printf("%-25s : \t %f\n", " Global Rate", app_info[i].app_dynamic.app_global_rate);
            printf("---------------------------------------------------------\n");
        }
    }
    printf("=========================================================\n");
}

void core_info_print()
{
    int i;

    printf("=========================================================\n");
    for(i=0 ; i<max_core_count ; i++)
    {
        printf("---------------------------------------------------------\n");
        printf("%-20s : \t %d\n", " Core ID", core_info[i].core_id);
        printf("%-20s : \t %d\n", " Enabled", core_info[i].core_dynamic.core_active);
        printf("%-20s : \t %ld\n", " Max Freq.", core_info[i].core_static.core_max_freq);
        printf("%-20s : \t %ld\n", " Min Freq.", core_info[i].core_static.core_min_freq);
        printf("%-20s : \t %d\n\n", " Performance (mips)", core_info[i].core_static.core_mips);
        
        printf("%-20s : \t %ld\n", " Cur Freq.", core_info[i].core_dynamic.core_cur_freq);
        printf("%-20s : \t %d\n", " Process cnt", core_info[i].core_dynamic.process_on_core);
        printf("%-20s : \t %d\n", " Thread Cnt", core_info[i].core_dynamic.thread_on_core);
        printf("---------------------------------------------------------\n");
    }
    printf("=========================================================\n");
}

void process_info_print()
{
    int i, j;

    printf("=========================================================\n");
    for(i=0 ; i<MAX_PROCESS_COUNT ; i++)
    {
        for(j=0 ; j<MAX_APP_COUNT ; j++)
        {
            if(process_info[i].pid == app_info[j].pid)
            {
                printf("---------------------------------------------------------\n");
                printf("%-15s : \t %s\n", " Name", app_info[j].app_name);
                printf("%-15s : \t %d\n", " PID", process_info[i].pid);
                printf("%-15s : \t %d\n", " PPID", process_info[i].parent_process);
                printf("%-15s : \t %d\n", " On core", process_info[i].core_id);
                printf("%-15s : \t %d\n", " Thread Cnt", process_info[i].thread_on_proc);
                printf("%-15s : \t %u\n", " State", process_info[i].proc_state);
                printf("%-15s : \t %u\n", " Priority", process_info[i].priority);
                printf("%-15s : \t %f\n", " Usage", process_info[i].usage);
                printf("---------------------------------------------------------\n");
            }
        }
    }
    printf("=========================================================\n");
}

void skb_getopt(int argc, char **argv)
{
    int opt;
    int index = 0;

    while(1)
    {
        opt = getopt_long(argc, argv, short_options, long_options, &index);

        if(opt == -1)
            break;

        switch(opt)
        {
            case 'a':
                hb_print_flag = 1;
                break;

            case 'c':
                core_print_flag = 1;
                break;

            case 'p':
                process_print_flag = 1;
                break;

            case 'l':
                log_flag = 1;
                break;

            case 'n':
                mq_interval = atoi(optarg);
                break;

            case 'h':
            default:
                help();
                exit(EXIT_SUCCESS);
                break;
        }
    }
}

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <string.h>

#include "extern-adapt.h"

int core=1;
int clock_count=0;
int Frequency_table[30]={400000, 500000, 600000, 700000, 800000, 900000, 1000000, 1100000, 1200000, 1300000, 1400000, 1500000, 1600000, 1700000, 18000000, 1900000, 2000000};


void external_actuator(int clk_cnt)
{
	int ret;
	printf(" =====================================================\n");	

	printf("\n BIG core Frequency\n");	
	snprintf(buffer, 100, "sudo sh -c \"echo %d > /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq\"", Frequency_table[clk_cnt]);
	printf(" work with [  %d   ] MHz \n\n", Frequency_table[clk_cnt]/1000);
	//                puts(buffer);
	ret = system(buffer);

	printf(" Little core Frequency\n");	
	snprintf(buffer, 100, "sudo sh -c \"echo %d > /sys/devices/system/cpu/cpu4/cpufreq/scaling_max_freq\"", Frequency_table[clk_cnt]);
	//               puts(buffer);
	ret = system(buffer);


	if( ret < 0 )
		printf("Error : external_actuator()\n");

}




int self_adaptation(double instant_hb, double target, double time_p, uint64_t inst_m)
{
	int mode=0, flag=-1;

	external_actuator( clock_count ); //FIXME : Position of actuator diffrent from original code

	if(instant_hb > target) {
		if(instant_hb > target+THRESHOLD) {
			printf("\nOverflow heartbeat!!\n");
			flag=1;
#if FAST_ADAPTATION                       
			while(1)
			{
				mode=Controller(time_p, inst_m, flag); // core up? frequency up?

downback:
				if(mode==0) //frequency
				{

					printf("-------------------- down frequency -----------------\n");
					clock_count--;
					temp_heartbeat=instant_hb;
					time_s*=(double)Frequency_table[clock_count+1]/Frequency_table[clock_count];
					time_p*=(double)Frequency_table[clock_count+1]/Frequency_table[clock_count];
					instant_hb*=(double)Frequency_table[clock_count]/Frequency_table[clock_count+1];
					if(instant_hb < target)
					{
						clock_count++;
						instant_hb=temp_heartbeat;
						break;
					}
				}

				else if(mode==1)
				{
					printf("-------------------- down core -----------------\n");
					core--;
					temp_heartbeat= instant_hb;
					instant_hb=instant_hb*time_p/(time_s+time_p)*core/(core+1) + (instant_hb*time_s/(time_s+time_p));

					if(instant_hb < target)
					{
						core++;
						instant_hb=temp_heartbeat;
						if(clock_count>0)
						{
							mode=0;
							goto downback;
						}
						else mode=-1;
					}
					time_p=(double)(core+1)/core*time_p;
				}
				else break;

				if(instant_hb<=target*THRESHOLD)
				{
					//      printf(">>>>>>>>>>>>>>>>  expected heartbeat = %f\n", instant_hb);
					break;
				}


				printf("predicted heartbeat_val = %f\n", instant_hb);

				if(clock_count>CLOCK_NUM && core>CORE_NUM || mode==-1) break;
			}
#else                         

			//change s
			mode=Controller(time_p, inst_m, flag); // core up? frequency up?

			if(mode==0) clock_count--;
			else if(mode==1) core--;
			//change e
#endif
		}
		else {
			printf("\n Enough heartbeat!! \n")    ;
			flag=-1;
		}
	}
	else {
		flag=0;
		printf("\n Insufficient heartbeat!! \n");

#if FAST_ADAPTATION
		while(1)
		{
			mode=Controller(time_p, inst_m, flag); // core up? frequency up?

upback:
			if(mode==0) //frequency
			{
				printf("-------------------- up frequency -----------------\n");
				clock_count++;
				time_s*=(double)Frequency_table[clock_count-1]/Frequency_table[clock_count];
				time_p*=(double)Frequency_table[clock_count-1]/Frequency_table[clock_count];
				instant_hb*=(double)Frequency_table[clock_count]/Frequency_table[clock_count-1];
			}

			else if(mode==1)
			{
				printf("-------------------- up core -----------------\n");
				core++;
				temp_heartbeat=instant_hb;
				instant_hb=instant_hb*time_p/(time_s+time_p)*core/(core-1) + (instant_hb*time_s/(time_s+time_p));

				if(instant_hb > target+THRESHOLD)
				{
					core--;
					instant_hb=temp_heartbeat;
					mode=0;
					goto upback;
				}

				time_p=(double)(core-1)/core*time_p;
			}
			else break;

			if(instant_hb>target)
			{
				//               printf(">>>>>>>>>>>>>>>>  predicted heartbeat = %f\n", instant_hb);
				break;
			}

			printf("predicted heartbeat_val = %f\n", instant_hb);
			if(clock_count>CLOCK_NUM && core>CORE_NUM) break;

		}
#else
		// change s
		mode=Controller(time_p, inst_m, flag); // core up? frequency up?

		if(mode==0) clock_count++;
		else if(mode==1) core++;

		// change e

#endif
	}

	if( core<1 ) {
		printf("[Core abnormaly detected !!]\n");
		//	core = 1;
	}


	return core;


}

int Controller(double time_m, uint64_t inst_m, int flag) {

	int frequency=Frequency_table[clock_count];
	double time_m_up, time_m_core_up, time_m_c, time_m_core_c;
	double time_m_cal, time_m_delay, E_m, EFF_m, E_m_core, EFF_m_core;
	double a0, a1, current_power, next_power;
	int mode;

	mode=-1;
	time_m_cal=inst_m/ISSUE_WIDTH /(frequency*1.0)/1000;
	time_m_delay = time_m-time_m_cal;


	// change s
	a0=0.16*pow(1.2,clock_count);
	a1=0.15*pow(1.2,clock_count);
	current_power=a0+a1*core;
	// change e

	if(flag==0) {
		if(clock_count>=CLOCK_NUM && core>=CORE_NUM)
		{
			printf("can not increase any resources\n");
			mode=-1;

			return -1;
		}

		else if(clock_count<CLOCK_NUM && core>=CORE_NUM) mode=0;
		else if(clock_count>=CLOCK_NUM && core<CORE_NUM) mode=1;
	}
	else if(flag==1) {
		if(clock_count<=0 && core<=1)
		{
			printf("can not increase any resources\n");
			mode=-1;

			return -1;
		}

		else if(clock_count>0 && core<=1) mode=0;
		else if(clock_count<=0 && core>1) mode=1;
	}


	//        printf("=========== mode = %d\n",mode);
	if(mode!=1) {

		if(flag==0)
		{
			a0=0.16*pow(1.2,clock_count+1);
			a1=0.15*pow(1.2,clock_count+1);
			next_power=a0+a1*core;
		}

		else if(flag==1) {
			if(clock_count>1)
			{
				a0=0.16*pow(1.2,clock_count-1);
				a1=0.15*pow(1.2,clock_count-1);
				next_power=a0+a1*core;
			}
			else
			{
				mode=1;
				return 1;
			}
		}
		//case1. change clock frequency
		if(flag==0){      //  If It can up the frequency, up.
			//                       printf("=== Increase Frequency cal\n");
			//                       time_s_up = (double)(inst_s/ISSUE_WIDTH / (Frequency_table[clock_count+1]*1.0)/1000 + time_s_delay);

			time_m_up = (double)(inst_m/ISSUE_WIDTH / (Frequency_table[clock_count+1]*1.0)/1000 + time_m_delay);

		}

		else if(flag==1) {
			//                     printf("Decrease Frequency cal\n");

			time_m_up = (double)(inst_m/ISSUE_WIDTH / (Frequency_table[clock_count-1]*1.0)/1000 + time_m_delay);

		}
		if(flag==0) {

			time_m_c=time_m-time_m_up;
			E_m = (time_m-time_m_delay) * current_power - (time_m_up-time_m_delay) * next_power + 1;

		}
		else if(flag==1) {
			time_m_c=time_m_up-time_m;
			E_m = ((time_m_up-time_m_delay) * next_power + 1) - (time_m-time_m_delay) * current_power;
		}
		E_m+=10;
		EFF_m =(double)time_m_c / E_m;
	}

	if(mode!=0) {

		//case2. add core
		if(flag==0){

			a0=0.16*pow(1.2,clock_count);
			a1=0.15*pow(1.2,clock_count);
			next_power=a0+a1*(core+1);

			time_m_core_up = (core*1.0 / (core+1)*1.0) * (inst_m/ISSUE_WIDTH / Frequency_table[clock_count]*1.0/1000) + time_m_delay;

			time_m_core_c=time_m-time_m_core_up;

			E_m_core = (time_m-time_m_delay) * current_power - (time_m_core_up - time_m_delay) * next_power;
		}

		else if(flag==1) {
			a0=0.16*pow(1.2,clock_count);
			a1=0.15*pow(1.2,clock_count);
			next_power=a0+a1*(core-1);

			time_m_core_up = (core*1.0 / (core-1)*1.0) * (inst_m/ISSUE_WIDTH / Frequency_table[clock_count]*1.0/1000) + time_m_delay;

			time_m_core_c=time_m_core_up-time_m;

			E_m_core = (time_m_core_up - time_m_delay) * next_power - (time_m-time_m_delay) * current_power;
		}
		E_m_core+=10;
		EFF_m_core = (double)time_m_core_c / E_m_core;
	}

	//       printf("EFF_m = %f\n", EFF_m);
	//       printf("EFF_m_core = %f\n\n", EFF_m_core);

	if(mode!=-1) return mode;
	else {
		if(EFF_m > EFF_m_core) {
			return 0;
		}

		else return 1;
	}
}



#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <netinet/in.h>

//#define NDEBUG
#include <assert.h>

#include "skb-types.h"
#include "skb-proc.h"
#include "skb-server.h"
#include "skb-protocol-handler.h"


int create_socket(int port)
{
	int sock;
	struct sockaddr_in name;
	int t = 1;

	sock = socket(PF_INET, SOCK_STREAM, 0);
	if(sock < 0)
	{
		perror("socket");
		exit(EXIT_FAILURE);
	}

	if(setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &t, sizeof(int)) == -1)
	{
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}

	name.sin_family = AF_INET;
	name.sin_port = htons(port);
	name.sin_addr.s_addr = htonl(INADDR_ANY);

	if(bind(sock, (struct sockaddr *)&name, sizeof(name)) < 0)
	{
		perror("bind");
		exit(EXIT_FAILURE);
	}

	return sock;
}


int read_message(int fd, char *buf, int len)
{
	int nbytes = 0;
	int recv_data = 0;
	int size;

	nbytes = read(fd, buf, len);

	if(nbytes < 0)
	{
		perror("read error");
		return -1;
	}
	else if(nbytes == 0)
		return -1;
	else
	{
		recv_data += nbytes;

		if(recv_data > 4)
		{
			memcpy(&size, buf, sizeof(int));

			while(recv_data < size)
			{
				nbytes = read(fd, buf+recv_data, len);
				if(nbytes > 0)
					recv_data += nbytes;
			}
		}
	}

	return recv_data;
}


int send_message(int fd, char *buf, int size)
{
	int remain = 0;
	int ret;

	while(remain != size)
	{
		ret = write(fd, buf+remain, size);

		if(remain == size)
			break;
		if(ret <= 0)
		{
			switch(errno)
			{
				case EAGAIN:
					if(ret == 0)
						return -1;
					continue;
				case EINTR:
				default:
					return -1;
			}
		}
		remain += ret;
	}


	return ret;
}


void *server_listen(void *t)
{
	int sock;
	fd_set active_fd_set, read_fd_set;
	int i;
	struct sockaddr_in client;
	//size_t size;
	socklen_t size;
	char recv_buf[MAX_MSGSIZE];
	int ret;
	struct timeval tv;

	sock = create_socket(VISUALIZER_PORT);
	if(listen(sock, MAX_CONNECTION) < 0)
	{
		perror("listen");
		exit(EXIT_FAILURE);
	}

	FD_ZERO(&active_fd_set);
	FD_SET(sock, &active_fd_set);

	while(1)
	{
		tv.tv_sec = 5;
		tv.tv_usec = 0;
		read_fd_set = active_fd_set;

		if(select(FD_SETSIZE, &read_fd_set, NULL, NULL, &tv) < 0)
		{
			perror("select");
		}

		for(i=0 ; i<FD_SETSIZE ; ++i)
		{
			if(FD_ISSET(i, &read_fd_set))
			{
				if(i == sock)
				{
					int new;
					size = sizeof(client);
					new = accept(sock, (struct sockaddr *)&client, &size);
#ifdef DEBUG
					printf(" Socket accepted : < %d >\n", new);
#endif

					if(new < 0)
					{
						perror("accept");
					}

					FD_SET(new, &active_fd_set);
				}
				else
				{
					memset(recv_buf, 0x00, sizeof(recv_buf));

					ret = read_message(i, recv_buf, sizeof(recv_buf));
					if(ret < 0)
					{
						close(i);
						FD_CLR(i, &active_fd_set);
#ifdef DEBUG
						printf(" Socket closed : < %d >\n", i);
#endif
					}
					else
					{
						process_message(i, recv_buf);
					}
				}
			}
		}
	}

	pthread_exit(NULL);
}

int skb_server_start(pthread_t *tid)
{
	int ret;

	ret = pthread_create(tid, NULL, (void *)server_listen, NULL);

	return ret;
}

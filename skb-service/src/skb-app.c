#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include <sys/msg.h>
#include <sys/ipc.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>


#include "skb-types.h"
#include "skb-app.h"
#include "skb-common.h"
#include "skb-proc.h"
#include "skb-shm.h"

#ifdef MOCO
#include "skb-mq.h"
#include "neural_sa.h"
#include "extern-adapt.h"
#endif


void clear_app_info(int pid)
{
	int i, j;
	int msqid;
#ifdef MOCO
	int msqid_t;
#endif
	for(i=0 ; i<MAX_APP_COUNT ; i++)
	{
		if(app_info[i].pid == pid)
		{
#ifdef MOCO
			msqid = app_info[i].mq_id[0];
			msqid_t = app_info[i].mq_id[1];
#else
			msqid = app_info[i].mq_id;
#endif


			msgctl(msqid, IPC_RMID, NULL);

#ifdef MOCO
			msgctl(msqid_t, IPC_RMID, NULL);
#endif

			memset(&app_info[i], -1, sizeof(APP_INFO));

			for(j=0 ; j<MAX_PROCESS_COUNT ; i++)
			{
				if(process_info[i].pid == pid)
				{
					memset(&process_info[i], -1, sizeof(PROCESS_INFO));
					break;
				}
			}

			app_count--;
			break;
		}
	}
}


void check_app()
{
	int i;
	char tmp[256] = {0x00};
	int pid;

	for(i=0 ; i<MAX_APP_COUNT ; i++)
	{
		pid = app_info[i].pid;
		if(pid > 0)
		{
			memset(tmp, 0x00, sizeof(tmp));
			sprintf(tmp, "/proc/%d", pid);

			if(0 != access(tmp, F_OK))
			{
				if(errno == ENOENT)
				{
					clear_app_info(pid);
				}
			}
		}
	}
}

#ifdef MOCO




/************************************************************

 * Predict Model: predict function algorithm
 * NOTE: user defined section III

 *************************************************************/


int prev_me_mode =0 ; 
int prev_subme_mode = 1;
double pr_hb[2] = {0,0};

int DecMaker_internal( double min_qos, double inst_hb )
{
	int min_me_mode = 0;
	int max_me_mode = 3;
	int min_subme_mode = 1;
	int max_subme_mode = 5;
	int i,j;
	double p_hb[5][6]; /* hard coded */


	pr_hb[1] = pr_hb[0];
	pr_hb[0] = inst_hb;

	for( i=min_me_mode; i <= max_me_mode; i ++ ){
		for( j=min_subme_mode; j <= max_subme_mode; j ++ ){

			p_hb[i][j] = test( prev_me_mode, prev_subme_mode, inst_hb,i,j);  // --> size shoule be received from App;
		}

	}

	double min;
	double margin = 0;

	min = p_hb[min_me_mode][min_subme_mode];

	int me, sub_me;
	for( i=min_me_mode; i <= max_me_mode; i ++ ){
		for( j=min_subme_mode; j <= max_subme_mode; j ++ ){
			if( p_hb[i][j]  >= (min_qos+margin) )
			{
				if( p_hb[i][j] < min ){
					min = p_hb[i][j];
					me = i;
					sub_me = j;
				}

			}


		}		
	}



	int alg_level;	
	printf("\n ***************************************");
	printf("\n Current hb : %f\n",inst_hb);
	printf("\n Predicted hb for the next iter: %f\n",min);
	printf("\n Selected ME = %d, SUB_ME = %d\n\n",me,sub_me);
	printf(" ***************************************\n");
	prev_me_mode = me;
	prev_subme_mode = sub_me;


	alg_level = 6*me + sub_me ; /* FIXME */


	return alg_level;	

}


int DecMaker_external(double instant, double min, double time_p, uint64_t inst_m)	// DecMaker for External adaptataion
{

	int ret;
	ret =  self_adaptation(instant, min, time_p, inst_m);  

	return ret;
}



void SA_Engine(int where)
{

	struct msgbuf mb;
	mb.mtype = 1;
	int ret,msg;
	double instant_rate = app_info[where].app_dynamic.app_instant_rate;  
	double min_qos = app_info[where].app_static.app_min_qos;
	double max_qos = app_info[where].app_static.app_max_qos;
	msg = DecMaker_internal(min_qos, instant_rate);	
	sprintf(mb.mtext, "%d", msg);
	ret = msgsnd( app_info[where].mq_id[1], &mb, sizeof(mb.mtext), 0); 
	if(ret == -1){
		printf("\nError: msgsnd in SA_Engine()\n");
	}

}

void SA_Engine2(int where)
{

	struct msgbuf mb;
	mb.mtype = 1;
	int ret,msg;
	double instant_rate = app_info[where].app_dynamic.app_instant_rate;  

	double _time_m = app_info[where].app_dynamic2.arg_1;
	double inst_m = (uint64_t) app_info[where].app_dynamic2.arg_2;

	double min_qos = app_info[where].app_static.app_min_qos;
	double max_qos = app_info[where].app_static.app_max_qos;


	msg = DecMaker_external(instant_rate, min_qos,_time_m, (uint64_t)inst_m);	


	printf("\n[ %d Cores ] will be running next iteration\n", msg );
	printf("======================================================\n\n\n");
	
	if(msg == 0) exit(1);
	sprintf(mb.mtext, "%d", msg);
	ret = msgsnd( app_info[where].mq_id[1], &mb, sizeof(mb.mtext), 0); 
	if(ret == -1){
		printf("Error: msgsnd in SA_Engine2()\n");
	}


}
#endif

void collect_app_dynamic_info(char *buf)
{
	int i;

	int temp_pid;
	double temp_window, temp_instant, temp_global;
	long long int temp_ts;
	int temp_hbID;
	char tmp[256] = {0x00};


	sscanf(buf, "%d|%lld|%d|%lf|%lf|%lf", &temp_pid, &temp_ts, 
			&temp_hbID, &temp_window, &temp_instant, &temp_global);

	for(i=0 ; i<MAX_APP_COUNT ; i++)
	{
#ifdef MOCO

		if(app_info[i].pid == temp_pid)
		{
			app_info[i].app_dynamic.app_hb_id = temp_hbID;
			app_info[i].app_dynamic.app_timestamp = temp_ts;
			app_info[i].app_dynamic.app_window_rate = temp_window;
			app_info[i].app_dynamic.app_instant_rate = temp_instant;
			app_info[i].app_dynamic.app_global_rate = temp_global;
			app_info[i].timestamp = (long long int)get_nano_timestamp();


			if( app_info[i].app_static.app_add_var == 1 )	
			{
				double temp_arg1;		/// instruction cycle
				double temp_arg2;
				double temp_arg3;
				double temp_arg4;


				sscanf(buf, "%d|%lld|%d|%lf|%lf|%lf|%lf|%lf|%lf|%lf", &temp_pid,&temp_ts, &temp_hbID, &temp_window, &temp_instant, &temp_global,&temp_arg1, &temp_arg2, &temp_arg3, &temp_arg4);

				app_info[i].app_dynamic2.arg_1 = temp_arg1;
				app_info[i].app_dynamic2.arg_2 = temp_arg2;
				app_info[i].app_dynamic2.arg_3 = temp_arg3;
				app_info[i].app_dynamic2.arg_4 = temp_arg4;

			}



			if( app_info[i].app_static.app_type == 0)
			{
#ifdef DEBUG
				printf(" Internal Adapt work\n");
#endif	
				SA_Engine( i );
			}
			else if(app_info[i].app_static.app_type == 1)
			{ 
#ifdef DEBUG
				printf(" External Adapt work\n");
#endif	
				SA_Engine2( i );
			}

			if(log_flag)
			{
				sprintf(tmp, "%lld|%s", app_info[i].timestamp, buf);
				skb_logging(HEARTBEAT_FILE, buf);
			}

			break;
		}
#else
		if(app_info[i].pid == temp_pid)
		{
			app_info[i].app_dynamic.app_hb_id = temp_hbID;
			app_info[i].app_dynamic.app_timestamp = temp_ts;
			app_info[i].app_dynamic.app_window_rate = temp_window;
			app_info[i].app_dynamic.app_instant_rate = temp_instant;
			app_info[i].app_dynamic.app_global_rate = temp_global;
			app_info[i].timestamp = (long long int)get_nano_timestamp();


			if(log_flag)
			{
				sprintf(tmp, "%lld|%s", app_info[i].timestamp, buf);
				skb_logging(HEARTBEAT_FILE, buf);
			}

			break;
		}
#endif
	}


	if(hb_print_flag)
	{
		hb_info_print();
	}
}






#ifdef MOCO
void collect_app_static_info(char *buf, int* mq_id, int sa_type)
#else
void collect_app_static_info(char *buf, int mq_id, int sa_type)
#endif
{
	if(sa_type == SA_OK)
	{
		int i;

		int temp_pid, temp_hb_count, temp_type;
#ifdef MOCO
		int temp_alg_total;
		int temp_add_var;
#endif
		double temp_max_qos, temp_target_qos, temp_min_qos, temp_window_size;
		long temp_samling_time, temp_buffer_depth;
		char temp_app_name[256];
		char temp_app_path[1024];
		char temp_logfile[MAX_MSGSIZE];

#ifdef MOCO
		sscanf(buf, "%d|%[^|]|%d|%lf|%lf|%lf|%lf|%d|%ld|%d|%d|%d|%[^|]",
				&temp_pid,
				temp_app_name,
				&temp_type,
				&temp_target_qos,
				&temp_max_qos,
				&temp_min_qos,
				&temp_window_size,
				&temp_buffer_depth,
				&temp_samling_time,
				&temp_hb_count,
				&temp_alg_total, // FIXME algorithm number 
				&temp_add_var,//FIXME
				temp_logfile);

#else
		sscanf(buf, "%d|%[^|]|%d|%lf|%lf|%lf|%lf|%ld|%ld|%d|%[^|]",
				&temp_pid,
				temp_app_name,
				&temp_type,
				&temp_target_qos,
				&temp_max_qos,
				&temp_min_qos,
				&temp_window_size,
				&temp_buffer_depth,
				&temp_samling_time,
				&temp_hb_count,
				temp_logfile);
#endif
		realpath(temp_app_name, temp_app_path);

#ifdef DEBUG
		printf("pid : %d\n", temp_pid);
		printf("path : %s\n", temp_app_path);
		printf("name : %s\n", temp_app_name);
		printf("type : %d\n", temp_type);
		printf("max : %lf\n", temp_max_qos);
		printf("target : %lf\n", temp_target_qos);
		printf("min : %lf\n", temp_min_qos);
		printf("ws : %lf\n", temp_window_size);
		printf("st : %ld\n", temp_samling_time);
		printf("hbc : %d\n", temp_hb_count);
		printf("bd : %ld\n", temp_buffer_depth);
		printf("log : %s\n", temp_logfile);
#ifdef MOCO
		printf("total_alg_number : %d\n", temp_alg_total);
		printf("total_add_var : %d\n", temp_add_var);
#endif
#endif


#ifdef MOCO 	// Neural Network calculation file input
		if( temp_type == 0){  	//FIXME INTERNAL / EXTERNAL
			openTrainedNF(neural_path);
			printf("checked openTrainedNF\n");
		}

#endif


		for(i=0 ; i<MAX_APP_COUNT ; i++)
		{
			if(app_info[i].pid <= 0)
			{
				app_info[i].pid = temp_pid;
#ifdef MOCO
				app_info[i].mq_id[0] = mq_id[0];
				app_info[i].mq_id[1] = mq_id[1]; // added by henry
#else
				app_info[i].mq_id = mq_id;
#endif
				memcpy(app_info[i].app_name, temp_app_name, sizeof(temp_app_name));
				memcpy(app_info[i].app_path, temp_app_path, sizeof(temp_app_path));
				app_info[i].app_static.app_type = temp_type;
				app_info[i].app_static.app_max_qos = temp_max_qos;
				app_info[i].app_static.app_target_qos = temp_target_qos;
				app_info[i].app_static.app_min_qos = temp_min_qos;
				app_info[i].app_static.app_window_size = temp_window_size;
				app_info[i].app_static.app_sampling_time = temp_samling_time;
				app_info[i].app_static.app_hb_count = temp_hb_count;
				app_info[i].app_static.app_buffer_depth = temp_buffer_depth;
				memcpy(app_info[i].app_static.app_log_file, temp_logfile, MAX_MSGSIZE);
				app_info[i].timestamp = (long long int)get_nano_timestamp();
#ifdef MOCO
				app_info[i].app_static.app_alg_total = temp_alg_total;
				app_info[i].app_static.app_add_var = temp_add_var;
#endif
				app_count++;

				break;
			}
		}
	}

	if(sa_type == SA_NONE)
	{
		int i;
		int temp_pid;
		char temp_app_name[256];
		char temp_app_path[1024];


		sscanf(buf, "%d|%[^|]",
				&temp_pid,
				temp_app_name);

		realpath(temp_app_name, temp_app_path);

		for(i=0 ; i<MAX_APP_COUNT ; i++)
		{
			if(app_info[i].pid <= 0)
			{
				app_info[i].pid = temp_pid;
#ifdef MOCO
				app_info[i].mq_id[0] = mq_id[0];
				app_info[i].mq_id[1] = mq_id[1]; // added by henry
#else
				app_info[i].mq_id = mq_id;
#endif
				memcpy(app_info[i].app_name, temp_app_name, sizeof(temp_app_name));
				memcpy(app_info[i].app_path, temp_app_path, sizeof(temp_app_path));
				app_info[i].app_static.app_type = -1;
				app_info[i].app_static.app_max_qos = -1;
				app_info[i].app_static.app_target_qos = -1;
				app_info[i].app_static.app_min_qos = -1;
				app_info[i].app_static.app_window_size = -1;
				app_info[i].app_static.app_sampling_time = -1;
				app_info[i].app_static.app_hb_count = -1;
				app_info[i].app_static.app_buffer_depth = -1;
				memset(app_info[i].app_static.app_log_file, 0x00, MAX_MSGSIZE);
				app_info[i].timestamp = (long long int)get_nano_timestamp();

#ifdef MOCO
				app_info[i].app_static.app_alg_total = -1;
				app_info[i].app_static.app_add_var = -1;
#endif	 	
				app_count++;
				break;
			}
		}
	}
}

void force_registration_app(int pid)
{
	char name_location[64];
	char app_name[256];
	char buf[1024+256];
	FILE *fp;
	int i;

	for(i=0 ; i<MAX_APP_COUNT ; i++)
	{
		if(pid == app_info[i].pid)
		{
			printf("Applications that have ALREADY been registered\n");
			return;
		}
	}

	memset(buf, 0x00, sizeof(buf));
	sprintf(name_location, "/proc/%d/comm", pid);
	fp = fopen(name_location, "r");

	if(fp != NULL)
	{
		fscanf(fp, "%s", app_name);
		fclose(fp);

		sprintf(buf, "%d|%s", pid, app_name);
		collect_app_static_info(buf, -1, SA_NONE);
		get_process_info(pid);
		printf(" -->  [%d] process is registered\n", pid);
		//process_info_print();
	}
	else
	{
		printf(" --> [%d] process is NOT present\n", pid);
	}
}

void force_unregistration_app(int pid)
{
	int i;

	for(i=0 ; i<MAX_APP_COUNT ; i++)
	{
		if(app_info[i].pid == pid)
		{
			clear_app_info(pid);
			printf("  -->  [%d] process revomed from Application list\n", pid);
			//process_info_print();
			return;
		}
	}

	printf(" --> [%d] process is NOT in Application list\n", pid);
}

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "skb-power.h"

void open_sensor(const char *node, sensor_t *sensor)
{
    if((sensor->fd = open(node, O_RDWR)) < 0)
        printf("%s Open Fail!\n", node);
    /*
    else
        printf("%s Open Success!\n", node);
    */
}

void enable_sensor(sensor_t *sensor, unsigned char enable)
{
    if(sensor->fd > 0)
    {
        sensor->data.enable = enable ? 1 : 0;
        if(ioctl(sensor->fd, INA231_IOCSSTATUS, &sensor->data) < 0)    
            printf("%s IOCTL Error!\n", sensor->data.name);
        /*
        else    
            printf("%s %s!\n", sensor->data.name, enable ? "enable" : "disable");
        */
    }
}

void read_sensor_status(sensor_t *sensor)
{
    if(sensor->fd > 0)
    {
        if(ioctl(sensor->fd, INA231_IOCGSTATUS, &sensor->data) < 0)    
            printf("%s IOCTL Error!\n", sensor->data.name);
        /*
        else
            printf("%s read status!\n", sensor->data.name);
        */
    }
}

void read_sensor(sensor_t *sensor)
{
    if(sensor->fd > 0)
    {
        if(ioctl(sensor->fd, INA231_IOCGREG, &sensor->data) < 0)  
            printf("%s IOCTL Error!\n", sensor->data.name);
        /*
        else
        {
            printf("Name : %s,  current uV : %d, current uA = %d, current uW = %d\n", sensor->data.name, sensor->data.cur_uV, sensor->data.cur_uA, sensor->data.cur_uW);
        }
        */
    }
}

void close_sensor(sensor_t *sensor)
{
        if(sensor->fd > 0)
                close(sensor->fd);
}


void init_sensor()
{
    // Sensor Device Open
    open_sensor(DEV_SENSOR_ARM, &sensor[SENSOR_ARM]);
    open_sensor(DEV_SENSOR_MEM, &sensor[SENSOR_MEM]);
    open_sensor(DEV_SENSOR_KFC, &sensor[SENSOR_KFC]);
    open_sensor(DEV_SENSOR_G3D, &sensor[SENSOR_G3D]);

    // Sensor Status Check
    read_sensor_status(&sensor[SENSOR_ARM]);
    read_sensor_status(&sensor[SENSOR_MEM]);
    read_sensor_status(&sensor[SENSOR_KFC]);
    read_sensor_status(&sensor[SENSOR_G3D]);

    // Sensor Enable
    if(!sensor[SENSOR_ARM].data.enable)
        enable_sensor(&sensor[SENSOR_ARM], 1);
    if(!sensor[SENSOR_MEM].data.enable)
        enable_sensor(&sensor[SENSOR_MEM], 1);
    if(!sensor[SENSOR_KFC].data.enable)
        enable_sensor(&sensor[SENSOR_KFC], 1);
    if(!sensor[SENSOR_G3D].data.enable)
        enable_sensor(&sensor[SENSOR_G3D], 1);
}


void exit_sensor()
{
    // Sensor Device Close
    close_sensor(&sensor[SENSOR_ARM]);
    close_sensor(&sensor[SENSOR_MEM]);
    close_sensor(&sensor[SENSOR_KFC]);
    close_sensor(&sensor[SENSOR_G3D]);
}

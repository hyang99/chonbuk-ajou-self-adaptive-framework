#include <stdio.h>
#include <stdio_ext.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <memory.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>

//#define NDEBUG
#include <assert.h>

#include "skb-types.h"
#include "skb-proc.h"
#include "skb-server.h"
#include "skb-mq.h"
#include "skb-common.h"
#include "skb-app.h"
#include "skb-shm.h"
#include "skb-power.h"




/**************************************
 * Predict Model : predictive model header
 * NOTE : user defined secton I
 **************************************/
#ifdef MOCO
#include "neural_sa.h"
#endif




pthread_t tid_collector;
pthread_t tid_socket;
pthread_t tid_mq;
pthread_t tid_console;

/**
 * @breif   mem free check, exit pthread
 *
 * @param   buf         receive data from socket
 * @param   mq_id       MQ's id
 *
 */
void exit_skb(int type)
{
	skb_shm_exit();

	// Sensor Disable
	if(sensor[SENSOR_ARM].data.enable)
		enable_sensor(&sensor[SENSOR_ARM], 0);
	if(sensor[SENSOR_MEM].data.enable)
		enable_sensor(&sensor[SENSOR_MEM], 0);
	if(sensor[SENSOR_KFC].data.enable)
		enable_sensor(&sensor[SENSOR_KFC], 0);
	if(sensor[SENSOR_G3D].data.enable)
		enable_sensor(&sensor[SENSOR_G3D], 0);

	exit_sensor();

	pthread_cancel(tid_mq);
	pthread_cancel(tid_socket);
	pthread_cancel(tid_collector);

#ifdef CONSOLE
	pthread_cancel(tid_console);
#endif

	exit(type);
}


/**
 * @breif   signal handler
 *
 * @param   sig         sig number
 *
 */
void catch_signal(int sig)
{
	//printf("Got Signal %d\n", sig);
	exit_skb(EXIT_FAILURE);
}


/**
 * @breif   init signal
 *
 */
void init_signal_handler()
{
	signal(SIGTERM, catch_signal);
	signal(SIGINT, catch_signal);
	signal(SIGSEGV, catch_signal);
}

/**
 * @breif   mem allocation for data, and init data
 *
 */
void init_skb()
{
	int i;

	mq_clean();

	if(mq_interval == 0)
		mq_interval = DEFAULT_MQ_INTERVAL;

	max_core_count = get_physical_core();
	core_info = (CORE_INFO *)malloc(sizeof(CORE_INFO)*max_core_count);
	skb_shm_init();

	assert(core_info != NULL);
	assert(app_info != NULL);
	assert(process_info != NULL);

	for(i=0 ; i<max_core_count ; i++)
	{
		memset(&core_info[i], 0x00, sizeof(CORE_INFO));
	}

	for(i=0 ; i<MAX_PROCESS_COUNT ; i++)
	{
		memset(&process_info[i], 0x00, sizeof(PROCESS_INFO));
	}

	for(i=0 ; i<MAX_APP_COUNT ; i++)
	{
		memset(&app_info[i], 0x00, sizeof(APP_INFO));
	}


	for(i=0 ; i<max_core_count ; i++)
	{
		core_info[i].core_id = i;
	}

	init_sensor();

#ifdef NO_CONSOLE
	printf("SKB-service Start\n");
#endif
}


/**
 * @breif   thread for system info
 *
 * @param   data         NULL
 *
 */
void *sys_info_collector(void *data)
{
	collect_core_static_info();
	sa_cpu_spec_set();

	while(1)
	{
		current_core_count = get_core_count();
		collect_core_dynamic_info();
		sa_cpu_state_update();
		collect_process_info();
		check_app();
		//sa_process_update();
		proc_update_pid_list();

		if(core_print_flag)
		{
			core_info_print();
		}

		if(process_print_flag)
		{
			process_info_print();
		}

		usleep(1000*1000*1);
	}


	pthread_exit(NULL);
}

/**
 * @breif   thread for console of SKB
 *
 * @param   data         NULL
 *
 */
#ifdef CONSOLE
void *skb_console(void *data)
{
	int select, ret;
	int pid;

	while(1)
	{
		printf("\n");
		printf("┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓\n");
		printf("┃  1. Process Info                     ┃\n");
		printf("┃  2. Core Info                        ┃\n");
		printf("┃  3. App Info                         ┃\n");
		printf("┃  4. Forced Registration App.         ┃\n");
		printf("┃  5. Forced Unregistration App.       ┃\n");
		printf("┃  6. Exit SKB                         ┃\n");
		printf("┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛\n");
		printf("   -  Choose > ");

		ret = scanf("%d", &select);
		__fpurge(stdin);
		printf("\n");

		if(ret)
		{
			switch(select)
			{
				case 1:
					process_info_print();
					break;
				case 2:
					core_info_print();
					break;
				case 3:
					hb_info_print();
					break;
				case 4:
					printf(" Input pid > ");

					ret = scanf("%d", &pid);
					__fpurge(stdin);
					printf("\n");
					force_registration_app(pid);
					break;
				case 5:
					printf(" Input pid > ");

					ret = scanf("%d", &pid);
					__fpurge(stdin);
					printf("\n");
					force_unregistration_app(pid);
					break;
				case 6:
					exit_skb(EXIT_SUCCESS);
				default:
					printf("Wrong Choice ? \n");
					break;
			}
		}
		else
		{
			printf("Wrong Choice ?? \n");
		}
	}

	pthread_exit(NULL);
}
#endif


/**
 * @breif   main, create 3 thread
 *
 * @param   data         NULL
 *
 */
int main(int argc, char *argv[])
{
	int ret;
	int status = 0;

	realpath(MSQID_FILE, msq_file);
#ifdef MOCO
	realpath(MSQID_FILE_EXT, msq_file_ext);

	/****************************************
	 * Predict Model : extern file( .vgn ) PATH set 	
	 * NOTE : user defined section II 
	 ******************************************/
	realpath(NeuralPATH, neural_path);
#endif
	init_signal_handler();

	skb_getopt(argc, argv);

	init_skb();

	ret = pthread_create(&tid_collector, NULL, sys_info_collector, core_info);
	if(ret < 0)
	{
		perror("pthread_create():tid_collector");
		exit(EXIT_FAILURE);
	}

	skb_server_start(&tid_socket);

	ret = pthread_create(&tid_mq, NULL, queue_listen, NULL);
	if(ret < 0)
	{
		perror("pthread_create():tid_collector");
		exit(EXIT_FAILURE);
	}

#ifdef CONSOLE
	ret = pthread_create(&tid_console, NULL, skb_console, NULL);
	if(ret < 0)
	{
		perror("pthead_create():tid_console");
		exit(EXIT_FAILURE);
	}

	pthread_join(tid_console, (void **)&status);
#endif

	pthread_join(tid_mq, (void **)&status);
	pthread_join(tid_socket, (void **)&status);
	pthread_join(tid_collector, (void **)&status);

	exit_skb(EXIT_SUCCESS);

	return 0;
}

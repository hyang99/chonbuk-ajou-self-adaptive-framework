#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "skb-types.h"
#include "sa_monitor_system.h"
#include "sa_monitor_process.h"
#include "skb-shm.h"

#define MAX_CORE    8

void main(int argc, char *argv[])
{
    int ret;
    int pid;
    int i, enabled;
    long freq;
    double usage, lf, qos_double;
    int process_count;
    unsigned long data;
    long freq_list[25];
    long qos_long;
    int list[1024];

    if(argc != 2)
    {
        printf("Usage : %s [pid]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    pid = atoi(argv[1]);

    

    //SA_CPU_SPEC_T *spec;
    SA_CPU_SPEC_T spec[MAX_CORE];
    //SA_CPU_STATE_T *state;
    SA_CPU_STATE_T state[MAX_CORE];
    SA_PROCESS_T process[50] = {0,};
    SA_MEM_INFO_T mem[50] = {0,};
    SA_NETWORK_INFO_T net[50] = {0,};
    SA_APP_STATIC_INFO_T static_i[50] = {0,};
    char tt_name[256], tt_path[256];

    while(1)
    {
        //sa_get_cpu_physical_core_count();
        //sa_get_cpu_current_core_count();
        for(i=0 ; i<8 ; i++)
        {
            /////sa_get_cpu_curr_freq(i);
            /////sa_get_core_usage(i);
            /////sa_get_cpu_linear_curr_freq(i);
            //sa_get_cpu_bigLITTLE_type(i);
            //sa_is_cpu_enabled(i);

            /////sa_get_cpu_spec_info(spec, i);
            //sa_get_cpu_state(state, i);
            /////sa_get_cpu_available_freq(freq_list, i);

        }
        /////sa_get_cpu_usage();

        /////sa_get_process_count();

        ret = sa_get_process_info_list(process);
        printf(" >>> ret : %d\n", ret);
        for(i=0 ; i<ret ; i++)
        {
            printf("[%s-%d] count : %d\n", __func__, __LINE__, process[i].count);
            printf("[%s-%d] pid : %d\n", __func__, __LINE__, process[i].static_info.pid);
        }

        //sa_get_process_info(&process[0], pid);
        //sa_get_process_usage(pid);
        //sa_get_process_state(pid);
        //sa_get_thread_count(pid);
        /////sa_get_mem_state(&mem[0], pid);
        /////sa_get_mem_vmpeak(pid);
        /////sa_get_mem_vmsize(pid);
        /////sa_get_mem_vmhwm(pid);
        /////sa_get_mem_vmrss(pid);
        /////sa_get_mem_usage(pid);
        /////sa_get_mem_total_usage();
        /////sa_get_network_total_rx_bytes(pid);
        /////sa_get_network_total_tx_bytes(pid);
        /////sa_get_network_delta_rx_bytes(pid);
        /////sa_get_network_delta_tx_bytes(pid);
        /////sa_get_network_rx_bandwinth(pid);
        /////sa_get_network_tx_bandwinth(pid);
        /////sa_get_network_backlogq_time(pid);
        /////sa_get_network_preq_time(pid);
        /////sa_get_network_ooo_time(pid);
        /////sa_get_network_recvq_time(pid);
        /////sa_get_network_rx_usage(pid);
        /////sa_get_network_tx_usage(pid);
        /////sa_get_core_by_pid(pid);
        /////sa_get_core_usage_by_pid(pid);
        /////sa_get_process_static_info(static_i, pid);
        /////sa_get_process_static_type(pid);
        /////sa_get_process_static_sampling_time(pid);
        /////sa_get_process_static_buffer_depth(pid);
        /////sa_get_process_static_max_qos(pid);
        /////sa_get_process_static_min_qos(pid);
        /////sa_get_process_static_target_qos(pid);
        /////sa_get_process_static_window_size(pid);
        /////sa_get_process_name(pid, tt_name);
        /////sa_get_process_path(pid, tt_path);
        /////sa_get_process_global_rate(pid);
        /////sa_get_process_window_rate(pid);
        /////sa_get_process_instant_rate(pid);
        /////sa_registration_application(pid);
        /////sa_unregistration_application(pid);
        /////sa_get_sys_network_total_rx_bytes();
        /////sa_get_sys_network_total_tx_bytes();
        /////sa_get_sys_network_delta_rx_bytes();
        /////sa_get_sys_network_delta_tx_bytes();
        printf("\n\n\n");

        /*
        ret = sa_get_cpu_physical_core_count();
        printf(" phy core : %d\n", ret);

        ret = sa_get_cpu_current_core_count();
        printf(" cur core : %d\n", ret);

        usage = sa_get_cpu_usage();
        printf(" total usage : %lf\n", usage);

        for(i=0 ; i<8 ; i++)
        {
            ret = sa_get_cpu_bigLITTLE_type(i);
            printf(" bL : %d\n", ret);
        }

//        ret = sa_get_cpu_available_freq(freq_list, 6);
//        for(i=0 ; i<25 ; i++)
//        {
//            printf(" [%d] freq : %ld\n", i, freq_list[i]);
//        }

        ret = sa_get_process_count();
        printf(" count : %d\n", ret);

        memset(process, 0x00, sizeof(SA_PROCESS_T)*50);
        ret = sa_get_process_info_list(process);

        printf(" -----------> count : %d\n", ret);
        for(i=0 ; i<50 ; i++)
        {
            if(process[i].process.pid > 0)
            {
                printf(" -> pid  : %d\n", process[i].process.pid);
                printf(" -> core : %d\n", process[i].process.core_id);
            }
        }

        usage = sa_get_process_usage(pid);
        printf(" process usage : %lf\n", usage);

        ret = sa_get_thread_count(pid);
        printf(" thread count : %d\n", ret);

        ret = sa_get_mem_vmpeak(pid);
        printf(" vmpeak : %d\n", ret);

        ret = sa_get_mem_vmsize(pid);
        printf(" vmsize : %d\n", ret);

        ret = sa_get_mem_vmhwm(pid);
        printf(" vmhwm : %d\n", ret);

        ret = sa_get_mem_vmrss(pid);
        printf(" vmrss : %d\n", ret);

        usage = sa_get_mem_total_usage();
        printf(" mem usage : %lf\n", usage);



        /*
        printf("---------------------------------------------\n");

        ret = sa_get_cpu_physical_core_count();
        printf(" physical core : %d\n", ret);

        ret = sa_get_cpu_current_core_count();
        printf(" current core : %d\n", ret);

        ret = sa_get_cpu_available_freq(freq_list, 6);

        for(i=0 ; i<25 ; i++)
        {
            printf("[%d] available_freq : %ld\n", ret, freq_list[i]);
        }
        printf("---------------------------------------------\n");

        ret = sa_get_cpu_bigLITTLE_type(2);
        printf(" [%d] bigLTTLE : %d\n", 2, ret);

        ret = sa_get_cpu_bigLITTLE_type(7);
        printf(" [%d] bigLTTLE : %d\n", 7, ret);

        ret = sa_get_process_count();
        printf("-----------------------------------> process_count : %d\n", ret);
        */

        //qos_long = sa_get_process_static_sampling_time(pid);
        //printf(" sampling_t : %ld\n", qos_long);

        /*
        qos_long = sa_get_process_static_buffer_depth(pid);
        printf(" buffer_d : %ld\n", qos_long);

        qos_double = sa_get_process_static_max_qos(pid);
        printf(" max_qos : %lf\n", qos_double);

        qos_double = sa_get_process_static_min_qos(pid);
        printf(" min_qos : %lf\n", qos_double);

        qos_double = sa_get_process_static_target_qos(pid);
        printf(" target_qos : %lf\n", qos_double);

        qos_double = sa_get_process_static_window_size(pid);
        printf(" window_s : %lf\n", qos_double);

        qos_double = sa_get_process_global_rate(pid);
        printf(" global : %lf\n", qos_double);

        qos_double = sa_get_process_window_rate(pid);
        printf(" window : %lf\n", qos_double);

        qos_double = sa_get_process_instant_rate(pid);
        printf(" instant : %lf\n", qos_double);
        */


        /*
        for(i=0 ; i<MAX_CORE ; i++)
        {
            freq = sa_get_cpu_curr_freq(i);
            printf("[%d] cur_freq : %ld\n", i, freq);
        }
        printf("---------------------------------------------\n");

        usage = sa_get_cpu_usage();
        printf(" sa_usage : %lf\n", usage);
        printf("---------------------------------------------\n");

        for(i=0 ; i<MAX_CORE ; i++)
        {
            enabled = sa_is_cpu_enabled(i);
            printf("[%d] enabled : %d\n", i, enabled);
        }
        printf("---------------------------------------------\n");

        for(i=0 ; i<MAX_CORE ; i++)
        {
            lf = sa_get_cpu_linear_curr_freq(i);
            printf("[%d] linear : %lf\n", i, lf);
        }
        printf("---------------------------------------------\n");

        ret = sa_get_cpu_spec_info(&spec[2], 2);

        printf(" ret : %d\n", ret);
        printf(" core_id : %d\n", spec[2].core_id);
        printf(" count : %d\n", spec[2].core_count);
        printf(" max : %ld\n", spec[2].max_freq);
        printf(" min : %ld\n", spec[2].min_freq);

        ret = sa_get_cpu_state(&state[5], 5);

        printf(" ret : %d\n", ret);
        printf(" core_id : %d\n", state[5].core_id);
        printf(" curr : %ld\n", state[5].curr_freq);
        printf(" enabled : %d\n", state[5].enabled);
        printf(" usage : %lf\n", state[5].usage);

        process_count = sa_get_process_count();
        printf(" count : %d\n", process_count);

        ret = sa_get_process_state(pid);
        printf(" state : %d\n", ret);

        ret = sa_get_process_info(&process[0], pid);
        printf(" [%d] pid : %d\n", ret, process[0].process.pid);
        printf(" [%d] tx : %ld\n", ret, process[0].net.total_tx);
        printf(" [%d] mem : %d\n", ret, process[0].mem.vm_peak);
        printf(" [%d] instant : %lf\n", ret, process[0].app.heartbeat_instant);

        usage = sa_get_process_usage(pid);
        printf(" usage : %lf\n", usage);

        ret = sa_get_thread_count(pid);
        printf(" thread : %d\n", ret);

        ret = sa_get_mem_state(&mem[0], pid);
        printf(" [%d] vmpeak : %d\n", ret, mem[0].vm_peak);
        printf(" [%d] vmsize : %d\n", ret, mem[0].vm_size);
        printf(" [%d] vmhwm : %d\n", ret, mem[0].vm_hwm);
        printf(" [%d] vmrss : %d\n", ret, mem[0].vm_rss);
        printf(" [%d] usage : %lf\n", ret, mem[0].vm_usage_percent);

        ret = sa_get_mem_vmpeak(pid);
        printf(" vmpeak : %d\n", ret);

        ret = sa_get_mem_vmsize(pid);
        printf(" vmsize : %d\n", ret);

        ret = sa_get_mem_vmhwm(pid);
        printf(" vmhwm : %d\n", ret);

        ret = sa_get_mem_vmrss(pid);
        printf(" vmrss : %d\n", ret);

        usage = sa_get_mem_usage(pid);
        printf(" usage : %lf\n", usage);

        data = sa_get_network_total_rx_bytes(pid);
        printf(" rx_bytes : %ld\n", data);

        data = sa_get_network_total_tx_bytes(pid);
        printf(" tx_bytes : %ld\n", data);

        data = sa_get_network_rx_bandwinth(pid);
        printf(" rx_bw : %ld\n", data);

        data = sa_get_network_tx_bandwinth(pid);
        printf(" tx_bw : %ld\n", data);

        data = sa_get_network_backlogq_time(pid);
        printf(" back : %ld\n", data);

        data = sa_get_network_preq_time(pid);
        printf(" preq : %ld\n", data);

        data = sa_get_network_ooo_time(pid);
        printf(" ooo : %ld\n", data);

        data = sa_get_network_recvq_time(pid);
        printf(" recvq : %ld\n", data);

        usage = sa_get_network_rx_usage(pid);
        printf(" rx_usage : %lf\n", usage);

        usage = sa_get_network_tx_usage(pid);
        printf(" tx_usage : %lf\n", usage);

        ret = sa_get_mem_vmsize(pid);
        printf(" vmsize : %d\n", ret);

        usage = sa_get_core_usage_by_pid(pid);
        printf(" core_usage_pid : %lf\n", usage);

        usage = sa_get_mem_total_usage();
        printf(" mem_total : %lf\n", usage);

        usage = sa_get_mem_usage(pid);
        printf(" mem_usage : %lf\n", usage);
        */

        ret = _get_thread_list_by_pid(pid, list, 1024);


        printf("---------------------------------------------\n");
        printf("=============================================\n");
        sleep(1);
    }
}

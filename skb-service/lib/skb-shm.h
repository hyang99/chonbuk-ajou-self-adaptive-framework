#ifndef __SKB_SHM_H
#define __SKB_SHM_H

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sched.h>

#include "skb-types.h"

/** LENGTH INDEX of receive data from socket                             */
/** Identifier for Shared Memory key_t - pathname                        */
#define PATH_CORE   "/tmp/core"
#define PATH_PROCESS    "/tmp/process"

/** Identifier for Shared memory ket_t - proj_id                         */
#define PROJ_CORE   97
#define PROJ_PROCESS    PROJ_CORE+1

key_t key_core, key_process;    /// key value of data structure

int shm_id_core;        /// shm id of core structure
int shm_id_process;     /// shm id of process structure

char *shm_core;         /// Shared memory Area of core structure
char *shm_process;      /// Shared memory Area of process structure



/**
 * @breif   Init function of shared memory
 *
 *
 * @return  int     On Success, return 0.
 *                  On Failure, return an integer less than Zero.
 *
 */
int skb_shm_init();


/**
 * @breif   Exit function of shared memory
 *
 *
 * @return  int     On Success, return 0.
 *                      On Failure, return an integer less than Zero.
 *
 */
int skb_shm_exit();


int sa_cpu_state_update(void);
int sa_cpu_spec_set(void);
int sa_process_update(void);



/**
 * @breif	The pinning of threads to specific core
 *
 * @param	int		process id
 * @param	int		start index of thread
 * @param	int 		end index of thread
 * @param	cpu_set_t	mask value for core
 *
 * @return	int		On Success, 0 is returned
 * 				On Failure, return an integer less than zero
 *
 */
int sa_set_affinity_threads(int, int, int, cpu_set_t);

/**
 * @breif	get thread count on core
 *
 * @param	int		core index
 *
 * @return	int		Return thread count
 *
 */
int sa_get_thread_count(int);

/**
 * @breif	Enable core
 *
 * @param	int		core index
 *
 * @return	int		On Success, 1 is returned
 				On Failure, return an integer less than zero
 *
 */
int sa_enable_core(int);

/**
 * @breif	Disable core
 *
 * @param	int		core index
 *
 * @return	int		On Success, 1 is returned
 				On Failure, return an integer less than zero
 *
 */
int sa_disable_core(int);

/**
 * @breif	Set frequency on core
 *
 * @param	int		core index
 * @param	int		frequency value for setting
 *
 * @return	int		On Success, 1 is returned
 				On Failure, return an integer less than zero
 *
 */
int sa_set_current_freq(int, int);





#endif  /* __SKB_SHM_H  */

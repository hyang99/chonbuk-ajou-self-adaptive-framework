#ifndef __SKB_PROC_H
#define __SKB_PROC_H

#include <dirent.h>
#include <sys/types.h>
#include <regex.h>
#include <limits.h>

#include <sys/stat.h>
#include <fcntl.h>

/**
 * @breif   collect static info of core
 *
 * @return  NULL
 *
 */
void collect_core_static_info();


/**
 * @breif   collect dynamic info of core
 *
 * @return  NULL
 *
 */
void collect_core_dynamic_info();


/**
 * @breif   get core count of system
 *
 * @return  int count of core
 *
 */
int get_core_count();


/**
 * @breif   collect process info
 *
 * @return  NULL
 *
 */
void collect_process_info();


int get_physical_core();
void get_process_info(int);
void proc_update_pid_list();

#endif  /*  __SKB_PROC_H    */

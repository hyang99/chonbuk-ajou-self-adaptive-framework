#ifndef __SKB_MQ_H
#define __SKB_MQ_H

#include <errno.h>
#include <sys/un.h>
#include <sys/socket.h>

/** msqid list - This MQs of content are delete                         */
#define MSQID_FILE  "./msqid.dat"

#ifdef MOCO
#define MSQID_FILE_EXT	"./msqid_ext.dat"
#endif


/** Maximum MQ count                                                    */
#define MQ_SIZE      50
#define BACK_LOG     5
#define INIT_BUFSIZE 15
#define COMM_BUFSIZE 300

#ifdef MOCO
/*added by Henry */
/** struct for mq message    */
struct msgbuf {
	long mtype;
	char mtext[300];
};
#endif

/** struct for mq                               */
typedef struct _mQstruct {
	int flag;       /// condition of MQ's message
	int pid;        /// pid for MQ  - This means key_t
#ifdef MOCO
	int mid[2];	    /// msqid for MQ: 0 s->a, 1 a->s
#else
	int mid;        /// msqid for MQ
#endif
} mQstruct;

mQstruct mQ[50];

/** present MQ count                                                    */
int mQ_cnt;
/** control flag for MQ thread                                          */
int mQ_flag;


/**
 * @breif   create MQ, using pid
 *
 * @param   pid     pid of app, Using a key_t
 *
 * @return  int     message queue id
 *
 */
#ifdef MOCO
int* mq_init(int);
#else
int mq_init(int);
#endif

/**
 * @breif   MQ message receive
 *
 * @param   buf     receive buffer
 *
 */
void mq_recv(char *);
/**
 * @breif   MQ message init for mq_recv
 *
 * @param   buf     receive buffer
 *
 */
#ifdef MOCO
void putmQ(int, int*);
#else
void putmQ(int, int);
#endif
/**
 * @breif   MQ clean in process init (init_skb)
 *
 */
void mq_clean();

/**
 * @breif   MQ thread Worker
 *
 */
void *queue_listen();

#endif

#ifndef __SKB_APP_H
#define __SKB_APP_H

typedef enum sa_type {
	SA_NONE = 0x00,
	SA_OK = 0x01
} SA_TYPE;

/**
 * @breif   collect static info of app from APP Register
 *
 * @param   buf         receive data from socket
 * @param   mq_id       MQ's id
 *
 */

#ifdef MOCO
void collect_app_static_info(char *, int*, int);
#else
void collect_app_static_info(char *, int, int);
#endif

/**
 * @breif   collect dynamic info of app from MQ's message
 *
 * @param   buf     message
 *
 */
void collect_app_dynamic_info(char *);
/**
 * @breif   for app data clear
 *
 * @param   pid     pid of app
 *
 */
void clear_app_info(int);


/**
 * @breif   Check lifetime of SA
 *
 * @param   NULL
 *
 */
void check_app();



/**
 * @breif   Register the NON-SA application by force.
 *
 * @param   pid     pid of app
 *
 */
void force_registration_app(int);

/**
 * @breif   Unregister the NON-SA application by force.
 *
 * @param   pid     pid of app
 *
 */
void force_unregistration_app(int);


#endif  /*  __SKB_APP_H */

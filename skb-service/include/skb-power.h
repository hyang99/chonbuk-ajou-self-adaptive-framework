#ifndef __SKB_POWER_H
#define __SKB_POWER_H

#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/ioctl.h>

#include "skb-types.h"

sensor_t sensor[SENSOR_MAX];

void open_sensor(const char *, sensor_t *);
void enable_sensor(sensor_t *, unsigned char);
void read_sensor_status(sensor_t *);
void read_sensor(sensor_t *);
void close_sensor(sensor_t *);
void init_sensor();
void exit_sensor();

#endif  /* __SKB_POWER_H    */

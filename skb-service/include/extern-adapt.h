#ifndef EXTERN-ADAPT_H_
#define EXTERN-ADAPT_H_


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>



#define POWER 0.084
#define POWER_core 40
#define CLOCK_NUM 10
#define THRESHOLD 1
#define CORE_NUM 8
#define ISSUE_WIDTH 3


char buffer[100];

//int core=1;
//int clock_count=0;

//int Frequency_table[30]={400000, 500000, 600000, 700000, 800000, 900000, 1000000, 1100000, 1200000, 1300000, 1400000, 1500000, 1600000, 1700000, 18000000, 1900000, 2000000};
 

int Controller(double time_m, uint64_t inst_m, int flag); 
int self_adaptation(double inst_hb,double target, double time_p, uint64_t inst_m);
void external_actuator( int clk_cnt );

#endif
